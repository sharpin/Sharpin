package com.sharpin.sharpin.card;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NormalCardTest {
    private NormalCard card;

    /**
     * Create a setup.
     */
    @BeforeEach
    public void setUp() {
        card = new NormalCard("pertanyaan", "jawaban", "Notes");
    }

    @Test
    public void testComponentType() {
        Assertions.assertEquals(card.componentType(),"Card");
    }

    @Test
    public void testCreateCard() {
        Card tst = new NormalCard("q", "a", "notes");
    }

    @Test
    public void whenCreateCardShouldHaveSrsToZero() {
        Assertions.assertEquals(0, card.getSrsStage());
    }

    @Test
    public void whenCreateCardReviewShouldBeTrue() {
        Assertions.assertTrue(card.isReadyForReview());
    }

    @Test
    public void whenCreateCardShouldHaveReadyForReviewToNow() {
        Date current = new Date();
        Assertions.assertTrue(current.getTime() - card.getNextReview().getTime() < 100);
    }

    @Test
    public void testIncreaseSrsStage() {
        card.increaseSrsStage();
        Assertions.assertTrue(card.getSrsStage() == 1);
    }

    @Test
    public void whenIncreaseSrsIncreaseShouldSetCardToNotBeReadyForReview() {
        card.increaseSrsStage();
        Assertions.assertFalse(card.isReadyForReview());
    }

    @Test
    public void testIncreaseSrsStageDoNothingWhenSrsStageIsEight() {
        card.setSrsStage(8);
        Assertions.assertTrue(card.getSrsStage() == 8);
    }

    @Test
    public void whenIncreaseSrsStageShouldSetNextReviewDate() {

        Date current = new Date();
        card.increaseSrsStage();
        Date nextReview = card.getNextReview();
        Assertions.assertTrue(nextReview.getTime() - current.getTime() >= 28800000);
    }

    @Test
    public void whenDecreaseStageShouldSetNextReviewDate() {

        Date current = new Date();
        card.setSrsStage(5);
        card.decreaseSrsStage();
        Date nextReview = card.getNextReview();
        Assertions.assertTrue(nextReview.getTime() - current.getTime() >= 604800000);
    }

    @Test
    public void whenDecreaseStageWhileZeroShouldSetNextReviewDate() {

        Date current = new Date();
        card.decreaseSrsStage();
        Date nextReview = card.getNextReview();
        Assertions.assertTrue(nextReview.getTime() - current.getTime() >= 14400000);
    }

    @Test
    public void tesDecreaseSrsStage() {
        card.increaseSrsStage();
        card.decreaseSrsStage();
        Assertions.assertTrue(card.getSrsStage() == 0);
    }

    @Test
    public void whenDecreaseSrsStageShouldSetReviewToFalse() {
        card.increaseSrsStage();
        card.decreaseSrsStage();
        Assertions.assertFalse(card.isReadyForReview());
    }


    @Test
    public void whenDecreaseSrsStageOnAlreadyZeroShouldSetReviewToFalse() {
        card.decreaseSrsStage();
        Assertions.assertFalse(card.isReadyForReview());
    }

    @Test
    public void tesDecreaseSrsStageDoNothingWhenSrsStageIsZero() {
        card.decreaseSrsStage();
        Assertions.assertTrue(card.getSrsStage() == 0);
    }

    @Test
    public void testValidateAnswer() {
        Assertions.assertTrue(card.validateAnswer("jawaban"));
    }

    @Test
    public void testUpdateFront() {
        card.updateFront("New Notes");
        Assertions.assertEquals(card.getFront(),"New Notes");
    }

    @Test
    public void testUpdateBack() {
        card.updateBack("New Answer");
        Assertions.assertEquals(card.getBack(),"New Answer");
    }

    @Test
    public void testUpdateNotes() {
        card.updateNotes("New Notes");
        Assertions.assertTrue(card.getNotes().equals("New Notes"));
    }

    @Test
    public void testUpdateWhenCallMethodSetNextReview() {

        Date update = new Date();
        card.setNextReview(update);
        card.update(update);
        Assertions.assertEquals(card.getNextReview(),update);
    }

    @Test
    public void testUpdateWhenDidntCallMethodSetNextReview() {

        Date update = new Date();
        card.setNextReview(update);

        Date update2 = new Date(update.getTime() + TimeUnit.HOURS.toMillis(1));
        card.update(update2);
        Assertions.assertNotEquals(card.getNextReview(),update2);

    }

    @Test
    public void testNotifyFalse() {

        Date update = new Date();
        card.setNextReview(update);

        Date update2 = new Date(update.getTime() - TimeUnit.HOURS.toMillis(1));
        card.update(update2);
        Assertions.assertFalse(card.isReadyForReview());

        Date update3 = new Date(update.getTime() + TimeUnit.HOURS.toMillis(1));
        card.update(update3);
        Assertions.assertTrue(card.isReadyForReview());
    }

    @Test
    public void testNotifyTrue() {

        Date update = new Date();
        card.setNextReview(update);

        Date update3 = new Date(update.getTime() + TimeUnit.HOURS.toMillis(1));
        card.update(update3);
        Assertions.assertTrue(card.isReadyForReview());
    }


}

package com.sharpin.sharpin.card;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserCardListTest {

    private UserCardList testSubject = new UserCardList();

    @Test
    public void whenSetIdShouldWork() {
        testSubject.setUserId("A123");
        Assertions.assertEquals("A123", testSubject.getUserId());
    }

    @Test
    public void whenCreateObjectShouldCreateIt() {

        testSubject = new UserCardList("A123");
    }
}

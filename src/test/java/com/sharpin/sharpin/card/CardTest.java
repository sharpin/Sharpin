package com.sharpin.sharpin.card;

import java.util.Date;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CardTest {

    private Card card;

    /**
     * Create a setup.
     */
    @BeforeEach
    public void setUp() {
        card = Mockito.mock(Card.class);
    }

    @Test
    public void testMethodGetBack() {
        Mockito.doCallRealMethod().when(card).getBack();
        String back = card.getBack();
        Assertions.assertEquals(null, back);
    }

    @Test
    public void testMethodGetFront() {
        Mockito.doCallRealMethod().when(card).getFront();
        String front = card.getFront();
        Assertions.assertEquals(null, front);
    }

    @Test
    public void testMethodGetNotes() {
        Mockito.doCallRealMethod().when(card).getNotes();
        String notes = card.getNotes();
        Assertions.assertEquals(null, notes);
    }

    @Test
    public void testMethodGetSrsStage() {
        Mockito.doCallRealMethod().when(card).getSrsStage();
        int srsStage = card.getSrsStage();
        Assertions.assertEquals(0, srsStage);
    }

    @Test
    public void testMethodGetNextReview() {
        Mockito.doCallRealMethod().when(card).getNextReview();
        Date nextReview = card.getNextReview();
        Assertions.assertEquals(null,  nextReview);
    }

    @Test
    public void testMethodIsReadyForReview() {
        Mockito.doCallRealMethod().when(card).isReadyForReview();
        Boolean readyForReview = card.isReadyForReview();
        Assertions.assertEquals(false, readyForReview);
    }

    @Test
    public void testGetRatioOfCorrectReviews() {
        card.numberOfCorrectAnswer = 3;
        card.numberOfIncorrectAnswer = 11;
        Mockito.doCallRealMethod().when(card).getRatioOfCorrectReviews();
        Double ratioOfCorrectReviews = card.getRatioOfCorrectReviews();
        Assertions.assertEquals(21.43, ratioOfCorrectReviews);

    }

    @Test
    public void getRatioOnZeroWillReturn100() {
        card.numberOfCorrectAnswer = 0;
        card.numberOfIncorrectAnswer = 0;
        Mockito.doCallRealMethod().when(card).getRatioOfCorrectReviews();
        Double ratioOfCorrectReviews = card.getRatioOfCorrectReviews();
        Assertions.assertEquals(100, ratioOfCorrectReviews);
    }
}

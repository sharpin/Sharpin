package com.sharpin.sharpin.card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import org.junit.jupiter.api.Test;

public class SrsWaitingTimeUpdateTest {

    private SrsWaitingTimeUpdate srsWaitingTimeUpdate = new SrsWaitingTimeUpdate();

    @Test
    public void whenGettingWaitingTimeShouldBeCorrect() {


        assertEquals(14400000, srsWaitingTimeUpdate.getSrsNewWaitingTime(0));
        assertEquals(28800000, srsWaitingTimeUpdate.getSrsNewWaitingTime(1));
        assertEquals(86400000, srsWaitingTimeUpdate.getSrsNewWaitingTime(2));
        assertEquals(172800000, srsWaitingTimeUpdate.getSrsNewWaitingTime(3));
        assertEquals(604800000, srsWaitingTimeUpdate.getSrsNewWaitingTime(4));
        assertEquals(1209600000, srsWaitingTimeUpdate.getSrsNewWaitingTime(5));
        assertEquals(2592000000L, srsWaitingTimeUpdate.getSrsNewWaitingTime(6));
        assertEquals(10368000000L, srsWaitingTimeUpdate.getSrsNewWaitingTime(7));
        assertTrue(srsWaitingTimeUpdate.getSrsNewWaitingTime(8) > 103680000000L);

    }

    @Test
    public void whenGettingWaitingTimeInvalidShouldThrowException() {

        assertThrows(IllegalArgumentException.class,
            () -> srsWaitingTimeUpdate.getSrsNewWaitingTime(-1));
        assertThrows(IllegalArgumentException.class,
            () -> srsWaitingTimeUpdate.getSrsNewWaitingTime(9));
    }

    @Test
    public void whenReturningNewTimeShouldBeCorrect() {

        Date date = new Date();
        Date newDate = srsWaitingTimeUpdate.getNewReviewDate(date, 5);
        assertEquals(1209600000, newDate.getTime() - date.getTime());
    }
}

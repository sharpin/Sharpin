package com.sharpin.sharpin.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataComponentRepositoryChildTest {

    @Autowired
    private DataComponentRepository repository;

    /**
     * Initializing mock data.
     */
    @Before
    public void setUp() {

        repository.deleteAll();

        Directory rootDir = new Directory("directory", "A123");
        Directory directory1 = new Directory("child1", "A123");
        rootDir.addChild(directory1);
        Card card1 = new NormalCard("f1", "b1", "n1");
        directory1.addChild(card1);
        Card card2 = new NormalCard("f2", "b2", "n2");
        directory1.addChild(card2);
        Card card3 = new NormalCard("f3", "b3", "n3");
        rootDir.addChild(card3);

        repository.save(rootDir);

    }

    @After
    public void clearDbAfter() {
        repository.deleteAll();
    }

    @Test
    public void directoryPersistedAll() {
        List<DataComponent> components = repository.findAll();
        assertEquals(5, components.size());
    }

    @Test
    public void directoryPersistChildFindById() {

        Directory rootDirFound = repository.findByUserIdAndDirectoryName("A123", "directory").get();
        long rootDirId = rootDirFound.getId();
        rootDirFound = (Directory) repository.findById(rootDirId).get();
        assertEquals(2, (rootDirFound).getChildrenSize());

    }

    @Test
    public void directoryPersistChildFindByName() {

        Directory rootDirFoundByName = repository
            .findByUserIdAndDirectoryName("A123", "directory").get();
        assertEquals(2, rootDirFoundByName.getChildrenSize());

    }

    @Test
    public void directoryCompositeWorks() {

        Directory rootDir = repository.findByUserIdAndDirectoryName("A123", "directory").get();
        List<Card> cards = listCard(rootDir);
        assertEquals(3, cards.size());
    }

    @Test
    public void testMovingChildToParent() {

        Directory child = repository
            .findByUserIdAndDirectoryName("A123", "child1").get();
        Card card1 = (Card) child.getChild(0);
        child.removeChild(card1);
        repository.save(child);

        Directory root = repository
                .findByUserIdAndDirectoryName("A123", "directory").get();
        root.addChild(card1);
        repository.save(root);

        child = repository
            .findByUserIdAndDirectoryName("A123", "child1").get();
        root = repository
            .findByUserIdAndDirectoryName("A123", "directory").get();
        assertEquals(3, root.getChildrenSize());
        assertEquals(1, child.getChildrenSize());


    }

    private List<Card> listCard(DataComponent cur) {

        List<Card> ret = new ArrayList<>();
        if (cur.componentType().equals("Card")) {
            ret.add((Card) cur);
            return ret;
        }
        Directory curd = (Directory) cur;
        for (int i = 0; i < curd.getChildrenSize(); i++) {
            List<Card> childCards = listCard(curd.getChild(i));
            ret.addAll(childCards);
        }
        return ret;

    }


}

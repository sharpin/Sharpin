package com.sharpin.sharpin.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataComponentRepositoryCardTest {

    @Autowired
    private DataComponentRepository repository;

    /**
     * Create a setup.
     */
    @Before
    public void setUp() {

        repository.deleteAll();

        Directory rootDir = new Directory("directory", "A123");
        Directory directory1 = new Directory("child1", "A123");
        rootDir.addChild(directory1);
        Card card1 = new NormalCard("f1", "b1", "n1");
        directory1.addChild(card1);
        Card card2 = new NormalCard("f2", "b2", "n2");
        directory1.addChild(card2);
        Card card3 = new NormalCard("f3", "b3", "n3");
        rootDir.addChild(card3);

        repository.save(rootDir);
    }

    @After
    public void clearDbAfter() {
        repository.deleteAll();
    }

    @Test
    public void listCardDisplayCard() {
        List<Card> cardList = repository.listAllCards();
        assertEquals(3,cardList.size());
    }

    @Test
    public void listCardDisplaySameCard() {
        List<Card> cardList = repository.listAllCards();
        ArrayList<String> frontCardList = new ArrayList<>();

        for (Card card : cardList) {
            frontCardList.add(card.getFront());
        }

        assertTrue(frontCardList.contains("f1"));
        assertTrue(frontCardList.contains("f2"));
        assertTrue(frontCardList.contains("f3"));
    }
}

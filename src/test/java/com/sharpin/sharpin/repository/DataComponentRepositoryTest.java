package com.sharpin.sharpin.repository;

import static org.junit.jupiter.api.Assertions.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataComponentRepositoryTest {

    @Autowired
    private DataComponentRepository repository;

    @Before
    public void clearDb() {
        repository.deleteAll();
    }

    @After
    public void clearDbAfter() {
        repository.deleteAll();
    }

    @Test
    public void testIdShouldNotBeNull() {
        DataComponent sample = new Directory("directory", "123456");
        repository.save(sample);
        assertNotNull(sample.getId());
    }

    @Test
    public void cardFoundAfterSaved() {

        Card card = new NormalCard("front", "back", "notes");
        repository.save(card);

        List<DataComponent> cards = repository.findAll();

        assertEquals(1, cards.size());

        Card sampleCard = (Card)cards.get(0);

        assertEquals("front", sampleCard.getFront());
        assertEquals("back", sampleCard.getBack());
        assertEquals("notes", sampleCard.getNotes());
        assertEquals(0, sampleCard.getSrsStage());
        assertTrue(sampleCard.isReadyForReview());

        repository.delete(sampleCard);
    }

    @Test
    public void cardCanBeDeletedAfterSaved() {
        Card card = new NormalCard("front", "back", "notes");
        repository.save(card);

        List<DataComponent> cards = repository.findAll();

        repository.delete(cards.get(0));

        cards = repository.findAll();
        assertEquals(0, cards.size());
    }

    @Test
    public void directoryFoundAfterSaved() {
        List<DataComponent> directories = repository.findAll();
        Directory directory = new Directory("directory", "123456");
        repository.save(directory);

        directories = repository.findAll();

        assertEquals(1, directories.size());

        repository.delete(directory);
    }

    @Test
    public void directoryCanBeDeletedAfterSaved() {
        Directory directory = new Directory("directory", "123456");
        repository.save(directory);

        List<DataComponent> directories = repository.findAll();

        repository.delete(directories.get(0));

        directories = repository.findAll();
        assertEquals(0, directories.size());
    }


    @Test
    public void directoryDoesNotSaveOnNullDname() {

        Directory directory = new Directory(null, "123456");
        assertThrows(DataIntegrityViolationException.class, () -> repository.save(directory));
    }

    @Test
    public void directoryDoesNotSaveOnNullUserId() {

        Directory directory = new Directory("directory name", null);
        assertThrows(DataIntegrityViolationException.class, () -> repository.save(directory));
    }

    @Test
    public void directoryNameUserIdPairShouldBeUnique() {

        DataComponent directory1 = new Directory("d1", "A123");
        repository.save(directory1);
        DataComponent directory2 = new Directory("d1", "A123");
        assertThrows(DataIntegrityViolationException.class, () -> repository.save(directory2));
    }

    @Test
    public void directoryNameUserIdPairCanBePartiallyDifferent() {

        DataComponent directory1 = new Directory("d1", "A123");
        repository.save(directory1);
        DataComponent directory2 = new Directory("b1", "A123");
        repository.save(directory2);
        DataComponent directory3 = new Directory("b1", "B123");
        repository.save(directory3);
    }

    @Test
    public void findingByDirectoryAndUserShouldWork() {

        DataComponent directory1 = new Directory("d1", "A123");
        repository.save(directory1);
        DataComponent directory2 = new Directory("b1", "A123");
        repository.save(directory2);
        DataComponent directory3 = new Directory("b1", "B123");
        repository.save(directory3);

        Optional<Directory> found = repository.findByUserIdAndDirectoryName("A123", "b1");
        Directory foundDir = found.get();
        assertEquals("b1", foundDir.getDirectoryName());
    }

}

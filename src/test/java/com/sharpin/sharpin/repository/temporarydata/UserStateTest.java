package com.sharpin.sharpin.repository.temporarydata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserStateTest {

    UserState testSubject;

    @BeforeEach
    public void setUp() {
        testSubject = new UserState();
    }

    @Test
    public void whenSetUserIdShouldSetIt() {
        testSubject.setUserId("A123");
        assertEquals("A123", testSubject.getUserId());
    }

    @Test
    public void whenSetStateShouldSetIt() {
        testSubject.setState("\\randomstate");
        assertEquals("\\randomstate", testSubject.getState());
    }
}

package com.sharpin.sharpin.repository.temporarydata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MoveCardStateTest {

    MoveCardState testSubject;

    @BeforeEach
    public void setUp() {
        testSubject = new MoveCardState();
    }

    @Test
    public void whenSetUserIdShouldSetIt() {
        testSubject.setUserId("A123");
        assertEquals("A123", testSubject.getUserId());
    }

    @Test
    public void whenSetStateShouldSetIt() {
        testSubject.setState("Next Handler");
        assertEquals("Next Handler", testSubject.getState());
    }

    @Test
    public void whenSetCardFrontShouldSetIt() {
        testSubject.setCardFront("The card front");
        assertEquals("The card front", testSubject.getCardFront());
    }

    @Test
    public void whenSetOldDirectoryShouldSetIt() {
        testSubject.setOldDirectory("The old directory");
        assertEquals("The old directory", testSubject.getOldDirectory());
    }

    @Test
    public void whenSetNewDirectoryShouldSetIt() {
        testSubject.setNewDirectory("The new directory");
        assertEquals("The new directory", testSubject.getNewDirectory());
    }
}

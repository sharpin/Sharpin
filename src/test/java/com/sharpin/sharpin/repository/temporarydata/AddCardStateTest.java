package com.sharpin.sharpin.repository.temporarydata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AddCardStateTest {

    AddCardState testSubject;

    @BeforeEach
    public void setUp() {
        testSubject = new AddCardState();
    }

    @Test
    public void whenSetUserIdShouldSetIt() {
        testSubject.setUserId("A1234");
        assertEquals("A1234", testSubject.getUserId());
    }

    @Test
    public void whenSetStateShouldSetIt() {
        testSubject.setState("Next Handler");
        assertEquals("Next Handler", testSubject.getState());
    }

    @Test
    public void whenSetCardFrontShouldSetIt() {
        testSubject.setCardFront("The card front");
        assertEquals("The card front", testSubject.getCardFront());
    }

    @Test
    public void whenSetCardBackShouldSetIt() {
        testSubject.setCardBack("The card back");
        assertEquals("The card back", testSubject.getCardBack());
    }

    @Test
    public void whenSetCardNotesShouldSetIt() {
        testSubject.setCardNotes("The card notes");
        assertEquals("The card notes", testSubject.getCardNotes());
    }

    @Test
    public void whenSetDirectoryShouldSetIt() {
        testSubject.setDirectory("The new directory");
        assertEquals("The new directory", testSubject.getDirectory());
    }
}

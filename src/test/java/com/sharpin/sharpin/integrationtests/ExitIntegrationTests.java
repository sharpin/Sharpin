package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.DeleteCardStateRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.service.handlers.deletecard.DeleteCardService;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ExitIntegrationTests {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    private String testUserId = "F123G";

    /**
     * Setting up tests by creating a mock directory and card.
     */
    @Before
    public void setUp() {


        Directory d = new Directory("Biology", testUserId);
        Card c = new NormalCard("Coconut", "milk", "is a plant");
        d.addChild(c);

        Card c2 = new NormalCard("Cabbage", "vegetable", "is a plant");
        d.addChild(c2);

        Directory root = new Directory("root", testUserId);
        root.addChild(d);
        dataComponentRepository.save(root);
    }

    @Test
    public void testExitIntegration() {

        // cycling the whole feature thrice to see if it is giving same results
        for (int times = 0; times < 3; times++) {

            assertEquals("Card front?", handler.handle(testUserId, "\\addcard").get(0));
            assertEquals("Card back?", handler.handle(testUserId, "Mitocondria").get(0));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));

            assertEquals("Directory name?", handler.handle(testUserId, "\\adddirectory").get(0));
            assertEquals("Parent directory?", handler.handle(testUserId, "Anatomy").get(0));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));

            assertEquals("Directory name?",  handler.handle(testUserId, "\\deletecard").get(0));
            assertEquals("Card front?", handler.handle(testUserId, "Biology").get(0));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));

            List<String> responses = handler.handle(testUserId, "\\quiz Biology");
            assertEquals(2, responses.size());
            assertEquals("Quiz initialized!", responses.get(0));
            assertEquals("Next question: Coconut", responses.get(1));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));

            assertEquals("Directory?", handler.handle(testUserId, "\\movecard").get(0));
            assertEquals("Card front?", handler.handle(testUserId, "Biology").get(0));
            assertEquals("New directory?", handler.handle(testUserId, "Coconut").get(0));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));

            responses = handler.handle(testUserId, "\\review 2");
            assertEquals(2, responses.size());
            assertEquals("Review initialized!", responses.get(0));
            assertEquals("Next question: Coconut", responses.get(1));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));

            assertEquals("Directory name?", handler.handle(testUserId, "\\editcard").get(0));
            assertEquals("Card front?", handler.handle(testUserId, "Biology").get(1));
            assertEquals("Process canceled", handler.handle(testUserId, "\\exit").get(0));


        }
    }
}

package com.sharpin.sharpin.integrationtests;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.AddDirectoryStateRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.service.handlers.adddirectory.AddDirectoryService;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AddDirectoryIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    AddDirectoryService addDirectoryService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    UserStateRepository userStateRepository;

    @Autowired
    AddDirectoryStateRepository addDirectoryStateRepository;

    /**
     * Create a setup.
     */
    @Before
    public void setUp() {
        userStateService.deleteUserData("A123");
        addDirectoryService.deleteUserData("A123");

        DataComponent testDir = new Directory("Biology","A123");
        dataComponentRepository.save(testDir);

    }

    @After
    public void clearDb() {

        dataComponentRepository.deleteAll();
    }

    @Test
    public void testAddDirectoryIsWorking() {

        // user: A123
        assertEquals("Directory name?", handler.handle("A123", "\\adddirectory").get(0));
        assertEquals("Parent directory?", handler.handle("A123", "Anatomy").get(0));
        assertEquals("Directory added!", handler.handle("A123", "Biology").get(0));

        assertFalse(userStateRepository.findById("A123").isPresent());
        assertFalse(addDirectoryStateRepository.findById("A123").isPresent());

        // masukin lagi
        assertEquals("Directory name?", handler.handle("A123", "\\adddirectory").get(0));
        assertEquals("Parent directory?", handler.handle("A123", "Microbiology").get(0));
        assertEquals("Directory added!", handler.handle("A123", "Biology").get(0));

        assertFalse(userStateRepository.findById("A123").isPresent());
        assertFalse(addDirectoryStateRepository.findById("A123").isPresent());

        // test nested
        assertEquals("Directory name?", handler.handle("A123", "\\adddirectory").get(0));
        assertEquals("Parent directory?", handler.handle("A123", "Head Anatomy").get(0));
        assertEquals("Directory added!", handler.handle("A123", "Anatomy").get(0));

        assertFalse(userStateRepository.findById("A123").isPresent());
        assertFalse(addDirectoryStateRepository.findById("A123").isPresent());

        Directory parentDir = dataComponentRepository
            .findByUserIdAndDirectoryName("A123", "Biology").get();
        assertEquals(2, parentDir.getChildrenSize());
        Directory child1 = (Directory) parentDir.getChild(0);
        Directory child2 = (Directory) parentDir.getChild(1);
        assertEquals("Anatomy", child1.getDirectoryName());
        assertEquals("Microbiology", child2.getDirectoryName());

        Directory anatomyDir = dataComponentRepository
            .findByUserIdAndDirectoryName("A123", "Anatomy").get();
        assertEquals(1, anatomyDir.getChildrenSize());
        child1 = (Directory) anatomyDir.getChild(0);
        assertEquals("Head Anatomy", child1.getDirectoryName());

    }

}

package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.DeleteCardStateRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.service.handlers.deletecard.DeleteCardService;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class DeleteCardIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    DeleteCardService deleteCardService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    UserStateRepository userStateRepository;

    @Autowired
    DeleteCardStateRepository deleteCardStateRepository;

    private String testUserId = "G321";

    /**
     * Setting up for the tests by creating a mock directory and card.
     */
    @Before
    public void setUp() {

        userStateService.deleteUserData(testUserId);
        deleteCardService.deleteUserData(testUserId);

        Directory targetDir = new Directory("Microbiology", testUserId);
        Card deletedCard = new NormalCard("Lactobacillus", "yougurt",
            "this is one of the most common bacteria");
        targetDir.addChild(deletedCard);

        Directory dumbDir = new Directory("Lactobacillus", testUserId);
        targetDir.addChild(dumbDir);
        dataComponentRepository.save(targetDir);
    }

    @Test
    public void testDeleteCardIntegration() {

        List<String> response = handler.handle(testUserId, "\\deletecard junk string");
        assertEquals(1, response.size());
        assertEquals("Directory name?", response.get(0));

        response = handler.handle(testUserId, "Aurelius");
        assertEquals(1, response.size());
        assertEquals("Directory does not exist, please give a valid directory name",
            response.get(0));

        response = handler.handle(testUserId, "Microbiology");
        assertEquals(1, response.size());
        assertEquals("Card front?", response.get(0));

        response = handler.handle(testUserId, "Mitocondria");
        assertEquals(1, response.size());
        assertEquals("The card front does not exist, please give a valid card front",
            response.get(0));

        response = handler.handle(testUserId, "Lactobacillus");
        assertEquals(1, response.size());
        assertEquals("Card with front \"Lactobacillus\" "
                + "has been deleted from directory Microbiology",
            response.get(0));

        Directory testDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "Microbiology").get();
        assertEquals(1, testDir.getChildrenSize());
        Directory onlyChild = (Directory) testDir.getChild(0);
        assertEquals("Lactobacillus", onlyChild.getDirectoryName());

        assertFalse(deleteCardStateRepository.findById(testUserId).isPresent());
        assertFalse(userStateRepository.findById(testUserId).isPresent());

        response = handler.handle(testUserId, "\\version");
        assertEquals("Version 0.0.1", response.get(0));

    }

}

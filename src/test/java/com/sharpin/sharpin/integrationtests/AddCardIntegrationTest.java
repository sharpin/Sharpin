package com.sharpin.sharpin.integrationtests;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.AddCardStateRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.service.handlers.addcard.AddCardService;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AddCardIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    AddCardService addCardService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    AddCardStateRepository addCardStateRepository;

    @Autowired
    UserStateRepository userStateRepository;

    /**
     * Create a setup.
     */
    @Before
    public void setUp() {
        userStateService.deleteUserData("A123");
        addCardService.deleteUserData("A123");
        DataComponent testDir = new Directory("Biochemistry","A123");
        DataComponent existCard = new NormalCard("Lisosom", "Organilitic Enzyme",
            "Lisosom is a cells component that contains organilitic enzyme.");
        ((Directory) testDir).addChild(existCard);

        dataComponentRepository.save(testDir);
    }

    @After
    public void clearDb() {

        dataComponentRepository.deleteAll();
    }

    @Test
    public void whenAddCardOnNewCardShouldWork() {

        // user: A123
        assertEquals("Card front?", handler.handle("A123", "\\addcard").get(0));
        assertEquals("Card back?", handler.handle("A123", "Mitocondria").get(0));
        assertEquals("Card notes?", handler.handle("A123", "Cells").get(0));
        assertEquals("Directory?", handler.handle("A123", "Notes").get(0));
        assertEquals("ERROR: Directory doesn't exist\nDirectory ?",
            handler.handle("A123", "Not Biochemistry").get(0));
        assertEquals("Card Mitocondria added to Biochemistry!",
                handler.handle("A123", "Biochemistry").get(0));

        Directory foundDir = dataComponentRepository
            .findByUserIdAndDirectoryName("A123", "Biochemistry").get();

        assertEquals(2, foundDir.getChildrenSize());
        Card child = (Card) foundDir.getChild(1);
        assertEquals("Mitocondria", child.getFront());
        assertEquals("Cells", child.getBack());
        assertEquals("Notes", child.getNotes());

        // checking if all temporary data is deleted
        assertFalse(userStateRepository.findById("A123").isPresent());
        assertFalse(addCardStateRepository.findById("A123").isPresent());

        // checking if app is working normally
        assertEquals("Version 0.0.1", handler.handle("A123", "\\version").get(0));
    }

    @Test
    public void whenAddCardOnExistingCardShouldFail() {
        // user: A123
        assertEquals("Card front?", handler.handle("A123", "\\addcard").get(0));
        assertEquals("Card back?", handler.handle("A123", "Lisosom").get(0));
        assertEquals("Card notes?", handler.handle("A123", "Cells").get(0));
        assertEquals("Directory?", handler.handle("A123", "Notes").get(0));
        assertEquals("ERROR: Directory doesn't exist\nDirectory ?",
            handler.handle("A123", "Not Biochemistry").get(0));

        String expectedResponse = "Card with front Lisosom already exist in Biochemistry. "
            + "Canceling addcard process, please try again";
        assertEquals(expectedResponse,
            handler.handle("A123", "Biochemistry").get(0));

        Directory foundDir = dataComponentRepository
            .findByUserIdAndDirectoryName("A123", "Biochemistry").get();

        assertEquals(1, foundDir.getChildrenSize());

        // checking if all temporary data is deleted
        assertFalse(userStateRepository.findById("A123").isPresent());
        assertFalse(addCardStateRepository.findById("A123").isPresent());

        // checking if app is working normally
        assertEquals("Version 0.0.1", handler.handle("A123", "\\version").get(0));
    }

}

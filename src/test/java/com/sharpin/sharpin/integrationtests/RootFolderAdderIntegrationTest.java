package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RootFolderAdderIntegrationTest {

    @Autowired
    private SharpinHandler handler;

    @Autowired
    private UserStateService userStateService;

    @Autowired
    private DataComponentRepository dataComponentRepository;

    private String testUserId = "A123Galang12";

    /**
     * Clearing the DB from temporary datas.
     */
    @Before
    public void setUp() {
        userStateService.deleteUserData(testUserId);
    }

    @Test
    public void whenFirstTimeShouldCreateRootDirectory() {

        Optional<Directory> userRootDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "root");
        assertFalse(userRootDir.isPresent());

        handler.handle(testUserId, "\\version");

        userRootDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "root");
        assertTrue(userRootDir.isPresent());
    }
}

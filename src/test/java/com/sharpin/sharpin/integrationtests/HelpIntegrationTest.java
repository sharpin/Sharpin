package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.sharpin.sharpin.service.handlers.SharpinHandler;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class HelpIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Test
    public void testHelpIntegration() {
        String expectedResponse = "Sharpin: A Chatbot based automatic flashcard system\n\n"
            + "The storage component consists of directory and cards. "
            + "Directory can be nested within another directory to create a tree-like hierarchy. "
            + "Each user by default will have a root directory called \"root\"\n\n"
            + "A card has a front, back, and notes:\n"
            + "Front: The part of the card that is asked in questions\n"
            + "Back: The part of the card that is considered the correct answer\n"
            + "Notes: additional info that is displayed after each answering attempt\n"
            + "Additionally, each card also has an SRS Level. "
            + "The higher the SRS level, the longer the next review will be. "
            + "Type \\help SRS for info on SRS.\n\n"
            + "List of commands:\n"
            + "To use a command, type and send it to the bot. "
            + "To get more info on each command, type \\help [COMMAND NAME], "
            + "for example, \\help adddirectory.\n"
            + "- \\exit: Interrupt an ongoing interaction\n"
            + "- \\addcard : add a card to your system\n"
            + "- \\adddirectory: add a directory to your system\n"
            + "- \\deletecard: delete a card from your system\n"
            + "- \\editcard: edit a card on your system\n"
            + "- \\movecard: move a card to a different directory\n"
            + "- \\quiz [directory name]: start a quiz with cards on the given directory\n"
            + "- \\review [number to review]: start a review with a "
            + "certain number of cards to review\n"
            + "- \\statistics [type] [directory name]: getting statistics on a directory\n"
            + "- \\list [type] [directory name]: listing cards/directory in a directory";

        assertEquals(expectedResponse, handler.handle("A123", "\\help").get(0));

        expectedResponse = "This command adds a directory to your system\n"
            + "Type \\adddirectory, the bot will start an interaction "
            + "asking the following in order:\n"
            + "- Directory name: Intended name of the newly added directory\n"
            + "- Parent directory: Intended place where the directory will be added\n"
            + "All your directory names must be unique, even if they don't are not ancestors.";

        assertEquals(expectedResponse,
            handler.handle("A123", "\\help \\adddirectory").get(0));

        expectedResponse = "Invalid parameter given for \\help. "
            + "Please enter \\help for the list of commands.";

        assertEquals(expectedResponse,
            handler.handle("A123", "\\help quiz random string").get(0));

    }
}

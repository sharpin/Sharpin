package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.MoveCardStateRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.service.handlers.movecard.MoveCardService;
import com.sharpin.sharpin.storage.Directory;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class MoveCardIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    MoveCardService moveCardService;

    @Autowired
    UserStateRepository userStateRepository;

    @Autowired
    MoveCardStateRepository moveCardStateRepository;

    @Autowired
    DataComponentRepository dataComponentRepository;

    private Directory mockOldDir;
    private Directory mockNewDir;

    /**
     * Function to set up a mock directory.
     */
    @Before
    public void setUp() {
        userStateService.deleteUserData("A123");
        moveCardService.deleteUserData("A123");
        dataComponentRepository.deleteAll();

        mockOldDir = new Directory("Plants", testUserId);
        Card movedCard = new NormalCard("Cockroach", "not the flying type", "eww");
        mockOldDir.addChild(movedCard);

        Card movedCardFail = new NormalCard("Coconut", "round and brown", "notes");
        mockOldDir.addChild(movedCardFail);

        mockNewDir = new Directory("Insects", testUserId);
        Card movedCardFailExist = new NormalCard("Coconut", "back", "notes");
        mockNewDir.addChild(movedCardFailExist);

    }
    
    private String testUserId = "F123";

    /**
     * Running movecard on different hierarchies.
     */
    public void testMoveCardIsWorking() {
        assertEquals("Directory?", handler.handle(testUserId, "\\movecard").get(0));
        assertEquals("Card front?", handler.handle(testUserId, "Plants").get(0));
        assertEquals("New directory?", handler.handle(testUserId, "Cockroach").get(0));
        assertEquals("Card Cockroach moved from Plants to Insects!",
            handler.handle(testUserId, "Insects").get(0));

        // checking if moved
        Directory oldDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "Plants").get();
        assertFalse(oldDir.isCardFrontExistAsDirectChild("Cockroach"));


        Directory newDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "Insects").get();
        assertTrue(newDir.isCardFrontExistAsDirectChild("Cockroach"));

        // checking if temporary data is deleted
        assertFalse(userStateRepository.findById(testUserId).isPresent());
        assertFalse(moveCardStateRepository.findById(testUserId).isPresent());

        // checking if app is working normally
        assertEquals("Version 0.0.1", handler.handle(testUserId, "\\version").get(0));
    }

    @Test
    public void testMoveCardIsWorkingNestedNewOld() {

        mockNewDir.addChild(mockOldDir);
        dataComponentRepository.save(mockNewDir);

        System.out.println(mockNewDir.getChildrenSize());
        testMoveCardIsWorking();



    }

    @Test
    public void testMoveCardIsWorkingNestedOldNew() {
        mockOldDir.addChild(mockNewDir);
        dataComponentRepository.save(mockOldDir);

        testMoveCardIsWorking();
    }

    @Test
    public void testMoveCardIsWorkingNotNested() {
        dataComponentRepository.save(mockOldDir);
        dataComponentRepository.save(mockNewDir);

        testMoveCardIsWorking();
    }

    @Test
    public void testMoveCardFail() {

        dataComponentRepository.save(mockNewDir);
        dataComponentRepository.save(mockOldDir);
        assertEquals("Directory?", handler.handle(testUserId, "\\movecard").get(0));

        assertEquals("ERROR: Directory Plantz does not exist.",
            handler.handle(testUserId, "Plantz").get(0));
        assertEquals("Card front?", handler.handle(testUserId, "Plants").get(0));

        assertEquals("ERROR: Card Snake does not exist.",
            handler.handle(testUserId, "Snake").get(0));
        assertEquals("New directory?", handler.handle(testUserId, "Coconut").get(0));

        assertEquals("ERROR: Directory Drink does not exist.",
            handler.handle(testUserId, "Drink").get(0));
        assertEquals("The card Coconut already exist in Insects. "
                + "Canceling movecard process, please try again",
            handler.handle(testUserId, "Insects").get(0));

        // checking if moved
        Directory oldDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "Plants").get();
        assertTrue(oldDir.isCardFrontExistAsDirectChild("Cockroach"));

        Directory newDir = dataComponentRepository
            .findByUserIdAndDirectoryName(testUserId, "Insects").get();
        assertFalse(newDir.isCardFrontExistAsDirectChild("Cockroach"));

        // checking if temporary data is deleted
        assertFalse(userStateRepository.findById(testUserId).isPresent());
        assertFalse(moveCardStateRepository.findById(testUserId).isPresent());

        // checking if app is working normally
        assertEquals("Version 0.0.1", handler.handle(testUserId, "\\version").get(0));

    }

}


package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.CardAnswerData;
import com.sharpin.sharpin.service.StatisticsService;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class StatisticIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    StatisticsService statisticsService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    UserStateRepository userStateRepository;

    /**
     * Create a setup.
     */
    @Before
    public void setUp() {
        userStateService.deleteUserData("A123");
        Directory testDir = new Directory("Biochemistry","A123");

        Card testCard1 = new NormalCard("Mitocondria","Cells", "Notes");
        testDir.addChild(testCard1);

        testCard1.increaseSrsStage();
        testCard1.increaseSrsStage();
        testCard1.increaseSrsStage();
        testCard1.increaseSrsStage();
        testCard1.increaseSrsStage();
        testCard1.decreaseSrsStage();
        testCard1.increaseSrsStage();
        testCard1.increaseSrsStage();

        Card testCard2 = new NormalCard("Nucleolus","Cells","Notes");
        testCard2.increaseSrsStage();
        testCard2.decreaseSrsStage();
        testCard2.decreaseSrsStage();
        testDir.addChild(testCard2);


        Card testCard3 = new NormalCard("Ribosom","Cells","Notes");
        testCard3.increaseSrsStage();
        testCard3.increaseSrsStage();
        testCard3.increaseSrsStage();
        testCard3.decreaseSrsStage();
        testCard3.decreaseSrsStage();
        testCard3.decreaseSrsStage();
        testCard3.increaseSrsStage();
        testDir.addChild(testCard3);

        Card testCard4 = new NormalCard("Peptide","Cells","Notes");
        testCard4.increaseSrsStage();
        testCard4.increaseSrsStage();
        testCard4.increaseSrsStage();
        testCard4.increaseSrsStage();
        testCard4.increaseSrsStage();
        testCard4.increaseSrsStage();
        testCard4.increaseSrsStage();
        testCard4.decreaseSrsStage();
        testCard4.decreaseSrsStage();
        testCard4.decreaseSrsStage();
        testDir.addChild(testCard4);

        Card testCard5 = new NormalCard("Mitosis","Cells","Notes");
        testCard5.increaseSrsStage();
        testCard5.decreaseSrsStage();
        testDir.addChild(testCard5);


        Card notDisplayedCard = new NormalCard("Lisosom", "Cells", "Notes");
        testDir.addChild(notDisplayedCard);
        dataComponentRepository.save(testDir);
    }

    @After
    public void clearDb() {

        dataComponentRepository.deleteAll();
    }

    @Test
    public void testStatisticsIsWorking() {

        // user: A123
        String expectedResponse = "Number of cards in each level on directory Biochemistry:\n"
            + "- Level 0: 3\n"
            + "- Level 1: 1\n"
            + "- Level 2: 0\n"
            + "- Level 3: 0\n"
            + "- Level 4: 1\n"
            + "- Level 5: 0\n"
            + "- Level 6: 1\n"
            + "- Level 7: 0\n"
            + "- Level 8: 0\n";
        assertEquals(expectedResponse,
                handler.handle("A123", "\\statistics statlevel Biochemistry").get(0));

        expectedResponse = "Cards in this directory that has the lowest correct answer rate:\n"
            + "- Nucleolus (33.33%)\n"
            + "- Mitosis (50.0%)\n"
            + "- Ribosom (57.14%)\n"
            + "- Peptide (70.0%)\n"
            + "- Mitocondria (87.5%)\n";
        assertEquals(expectedResponse,
                handler.handle("A123", "\\statistics statmiss Biochemistry").get(0));

        expectedResponse = "Cards in this directory that has the lowest correct answer rate:\n"
                + "- Nucleolus (33.33%)\n"
                + "- Mitosis (50.0%)\n"
                + "- Ribosom (57.14%)\n"
                + "- Peptide (70.0%)\n"
                + "- Mitocondria (87.5%)\n";
        assertEquals(expectedResponse,
                handler.handle("A123", "\\statistics statmiss Biochemistry").get(0));



        assertEquals("ERROR: Directory doesn't exist\nPlease try another directory",
                handler.handle("A123", "\\statistics statlevel Not Biochemistry").get(0));

        assertEquals("ERROR: Directory doesn't exist\nPlease try another directory",
                handler.handle("A123", "\\statistics statmiss Not Biochemistry").get(0));


        // checking if app is working normally
        assertEquals("Version 0.0.1", handler.handle("A123", "\\version").get(0));

    }



}

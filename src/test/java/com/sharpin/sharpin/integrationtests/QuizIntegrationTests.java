package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class QuizIntegrationTests {

    @Autowired
    private SharpinHandler handler;

    @Autowired
    private UserStateService userStateService;

    @Autowired
    private DataComponentRepository dataComponentRepository;

    @Autowired
    private UserStateRepository userStateRepository;


    private String testUserId = "A123Galang";

    /**
     * Inserting mock data to DB.
     */
    @Before
    public void setUp() {

        userStateService.deleteUserData(testUserId);

        DataComponent homeDir = new Directory("anime", testUserId);

        // adding cards to DB for testing
        Card tmp = new NormalCard(
            "Who is the seventh hokage?",
            "Uzumaki Naruto",
            "Uzumaki Naruto is the seventh hokage, after kakashi");
        ((Directory) homeDir).addChild(tmp);

        tmp = new NormalCard(
            "Who plays the drums at the U.A School Festival?",
            "Katsuki Bakugo",
            "IKUZOOO KOREEE\nYUUUEI ZEINNN OTO ZE YARUZE!\n...\n...\nYOROSHIKU ONEGAISHIMASU!!!"
        );
        ((Directory) homeDir).addChild(tmp);

        tmp = new NormalCard(
            "What is the best boss music in anime?",
            "grass skirt chase",
            "Ne ne ne ne ne ne not\n"
                + "Dung dung dung dung dungdungdungdungdung\n"
                + "Dung dung dung, dung dung, dung dung dung duuuunng\n"
                + "Dung dung dung dong dong deng dong dong dong doooong"
        );
        ((Directory) homeDir).addChild(tmp);

        dataComponentRepository.save(homeDir);

    }

    @After
    public void clearDb() {

        dataComponentRepository.deleteAll();
    }

    private void printResponses(List<String> response) {

        System.out.println("==== printing ===");
        for (String s : response) {
            System.out.println(s);
        }
        System.out.println("=== stop printing ====");
    }

    @Test
    public void testQuizIntegration() {

        List<String> response = handler.handle(testUserId, "\\quiz anime");
        printResponses(response);
        assertEquals(2, response.size());
        assertEquals("Quiz initialized!", response.get(0));
        assertEquals("Next question: Who is the seventh hokage?", response.get(1));

        response = handler.handle(testUserId, "Uzumaki Naruto");
        assertEquals(3, response.size());
        assertEquals("Correct answer!", response.get(0));
        assertEquals("Next question: Who plays the drums at the U.A School Festival?",
            response.get(2));

        response = handler.handle(testUserId, "Katsuki Bakugo");
        assertEquals(3, response.size());
        assertEquals("Correct answer!", response.get(0));
        assertEquals("Next question: What is the best boss music in anime?", response.get(2));

        response = handler.handle(testUserId, "grass skirt race");
        assertEquals(3, response.size());
        assertEquals("Incorrect answer, the correct answer is grass skirt chase.", response.get(0));
        assertEquals("Did you make a typo? (Y/N)", response.get(2));

        response = handler.handle(testUserId, "G");
        assertEquals(1, response.size());
        assertEquals("Please answer (Y/N)!", response.get(0));

        response = handler.handle(testUserId, "Y");
        assertEquals(2, response.size());
        assertEquals("Your answer is marked as correct!", response.get(0));
        assertEquals("All cards answered, your session has ended!", response.get(1));

        // checking if user state is deleted
        assertFalse(userStateRepository.findById(testUserId).isPresent());

        // checking if session exited gracefully
        response = handler.handle(testUserId, "\\version");
        assertEquals(1, response.size());
        assertEquals("Version 0.0.1", response.get(0));
    }

    @Test
    public void testQuizOnNonExistentDirectory() {
        List<String> responses = handler.handle("B123", "\\quiz spongebob");
        assertEquals(1, responses.size());
        assertEquals("Directory does not exist", responses.get(0));
    }

    @Test
    public void testQuizOnEmptyDirectory() {

        Directory emptyDir = new Directory("empty dir", testUserId);
        dataComponentRepository.save(emptyDir);

        List<String> responses = handler.handle(testUserId, "\\quiz empty dir");
        assertEquals(1, responses.size());
        assertEquals("No cards to quiz on this directory", responses.get(0));
    }
}

package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.UserState;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.NavigationService;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ReviewIntegrationTests {

    @Autowired
    private SharpinHandler handler;

    @Autowired
    private UserStateService userStateService;

    @Autowired
    private DataComponentRepository dataComponentRepository;

    @Autowired
    private UserStateRepository userStateRepository;

    @Autowired
    NavigationService navigationService;

    private String testUserId = "A123Galang";

    /**
     * Inserting mock data to DB.
     */
    @Before
    public void setUp() {

        userStateService.deleteUserData(testUserId);

        dataComponentRepository.deleteAll();

        Card card1 = new NormalCard("Review card number 1", "number 1", "notes of card 1");
        Card card2 = new NormalCard("Review card number 2", "number 2", "notes of card 2");
        Directory childDir = new Directory("children", testUserId);
        childDir.addChild(card1);
        childDir.addChild(card2);

        Card card3 = new NormalCard("Review card number 0", "number 0", "notes of card 0");
        Directory rootDir = new Directory("root", testUserId);
        rootDir.addChild(card3);
        rootDir.addChild(childDir);

        dataComponentRepository.save(rootDir);

    }


    private void printResponses(List<String> response) {

        System.out.println("==== printing ===");
        for (String s : response) {
            System.out.println(s);
        }
        System.out.println("=== stop printing ====");
    }

    @Test
    public void testReviewIntegration() {

        List<String> response = handler.handle(testUserId, "\\review 3");
        //printResponses(response);
        assertEquals(2, response.size());
        assertEquals("Review initialized!", response.get(0));
        assertEquals("Next question: Review card number 0", response.get(1));

        response = handler.handle(testUserId, "number 0");
        assertEquals(4, response.size());
        assertEquals("Correct answer!", response.get(0));
        assertEquals("Card with front \"Review card number 0\" SRS has been increased to 1",
            response.get(2));
        assertEquals("Next question: Review card number 1", response.get(3));

        response = handler.handle(testUserId, "number 70");
        assertEquals(3, response.size());
        assertEquals("Incorrect answer, the correct answer is number 1.", response.get(0));
        assertEquals("Did you make a typo? (Y/N)", response.get(2));

        response = handler.handle(testUserId, "G");
        assertEquals(1, response.size());
        assertEquals("Please answer (Y/N)!", response.get(0));

        response = handler.handle(testUserId, "N");
        assertEquals(3, response.size());
        assertEquals("Your answer is marked as incorrect!", response.get(0));
        assertEquals("Card with front \"Review card number 1\" SRS has been decreased to 0",
            response.get(1));
        assertEquals("Next question: Review card number 2", response.get(2));

        response = handler.handle(testUserId, "number 2");
        assertEquals(4, response.size());
        assertEquals("Correct answer!", response.get(0));
        assertEquals("Card with front \"Review card number 2\" SRS has been increased to 1",
            response.get(2));
        assertEquals("All cards answered, your session has ended!", response.get(3));

        // checking if user state is deleted
        assertFalse(userStateRepository.findById(testUserId).isPresent());

        // checking if session exited gracefully
        response = handler.handle(testUserId, "\\review");
        assertEquals(1, response.size());
        assertEquals("No cards to review", response.get(0));

    }

    @Test
    public void testReviewCardsDoesNotExistShouldReturnNoCardsToReview() {
        List<String> response = handler.handle("NotGalang", "\\review");
        assertEquals(1, response.size());
        assertEquals("No cards to review", response.get(0));
    }

}

package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.NavigationService;
import com.sharpin.sharpin.service.StatisticsService;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest

public class NavigationIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    NavigationService navigationService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    UserStateRepository userStateRepository;

    /**
     * Create a setup.
     */
    @Before
    public void setUp() {
        dataComponentRepository.deleteAll();
        userStateService.deleteUserData("A123");
        Directory testDir = new Directory("Biochemistry","A123");
        Directory childDir = new Directory("subdir Biochemistry", "A123");
        testDir.addChild(childDir);

        Directory childDir2 = new Directory("subdir Biochemistry2", "A123");
        testDir.addChild(childDir2);

        Card testCard = new NormalCard("Mitocondria","Cells", "Notes");
        testDir.addChild(testCard);

        DataComponent testCard2 = new NormalCard("Mitocondria2","Cells", "Notes");
        testDir.addChild(testCard2);

        DataComponent badCard = new NormalCard("Mitocondria3","Cells", "Notes");
        childDir2.addChild(badCard);

        dataComponentRepository.save(testDir);
    }

    @After
    public void clearDb() {

        dataComponentRepository.deleteAll();
    }

    @Test
    public void testNavigationIsWorking() {


        // user: A123
        String expectedResponse = "In directory Biochemistry. List of directories:\n"
            + "- subdir Biochemistry\n"
            + "- subdir Biochemistry2\n";
        assertEquals(expectedResponse,
            handler.handle("A123", "\\list dir Biochemistry").get(0));

        expectedResponse = "In directory Biochemistry. List of cards in this directory:\n"
            + "- Mitocondria\n"
            + "- Mitocondria2\n";
        assertEquals(expectedResponse,
                handler.handle("A123", "\\list card Biochemistry").get(0));

        assertEquals("ERROR: Directory doesn't exist\n"
                        + "Please try another directory",
                handler.handle("A123", "\\list dir Not Biochemistry").get(0));

        assertEquals("ERROR: Directory doesn't exist\n"
                        + "Please try another directory",
                handler.handle("A123", "\\list card Not Biochemistry").get(0));


        // checking if app is working normally
        assertEquals("Version 0.0.1", handler.handle("A123", "\\version").get(0));


    }
}

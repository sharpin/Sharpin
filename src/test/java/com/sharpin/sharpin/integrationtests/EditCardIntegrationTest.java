package com.sharpin.sharpin.integrationtests;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.EditCardStateRepository;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.service.handlers.editcard.EditCardService;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EditCardIntegrationTest {

    @Autowired
    SharpinHandler handler;

    @Autowired
    UserStateService userStateService;

    @Autowired
    EditCardService editCardService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    UserStateRepository userStateRepository;

    @Autowired
    EditCardStateRepository editCardStateRepository;

    /**
     * Create a setup.
     */
    @Before
    public void setUp() {
        userStateService.deleteUserData("A123");
        editCardService.deleteUserData("A123");

        Directory testDir = new Directory("Biology", "A123");
        Card testCard = new NormalCard("The powerhouse of the cell", "Nucleus",
                "The biochemical processes of the cell are known as cellular respiration.");
        testDir.addChild(testCard);
        Card moreCard = new NormalCard("Front", "Back", "Notes");
        testDir.addChild(moreCard);
        dataComponentRepository.save(testDir);
    }

    @After
    public void clearDb() {
        dataComponentRepository.deleteAll();
    }

    @Test
    public void testEditCardModifyWorking() {
        assertEquals("Directory name?", handler.handle("A123", "\\editcard").get(0));
        assertEquals("Card front?", handler.handle("A123", "Biology").get(1));

        List<String> newFront = handler.handle("A123", "The powerhouse of the cell");
        assertEquals("This card's current front is:", newFront.get(0));
        assertEquals("The powerhouse of the cell", newFront.get(1));
        assertEquals("New front?", newFront.get(2));

        List<String> newBack = handler.handle("A123", "keepsame");
        assertEquals("This card's current back is:", newBack.get(0));
        assertEquals("Nucleus", newBack.get(1));
        assertEquals("New back?", newBack.get(2));

        List<String> newNote = handler.handle("A123", "Mitochondria");
        assertEquals("This card's current note is:", newNote.get(0));
        assertEquals("The biochemical processes of the cell are known as cellular respiration.",
                newNote.get(1));
        assertEquals("New note?", newNote.get(2));

        assertEquals("Card edited!", handler.handle("A123", "keepsame").get(0));

        Optional<Directory> dir = dataComponentRepository.findByUserIdAndDirectoryName("A123",
                "Biology");
        Directory directory = dir.get();
        Card card = directory.findCardByName("The powerhouse of the cell");
        assertEquals("The powerhouse of the cell", card.getFront());
        assertEquals("Mitochondria", card.getBack());
        assertEquals("The biochemical processes of the cell are known as cellular respiration.",
                card.getNotes());

        assertEquals("Directory name?", handler.handle("A123", "\\editcard").get(0));
        assertEquals("Card front?", handler.handle("A123", "Biology").get(1));

        List<String> cardFront = handler.handle("A123", "Pythagoras Theorem");
        assertEquals("ERROR: Card with front Pythagoras Theorem does not exist", cardFront.get(0));
        assertEquals("Card front?", cardFront.get(1));

        assertEquals("New front?", handler.handle("A123", "The powerhouse of the cell").get(2));
        assertEquals("New back?", handler.handle("A123", "keepsame").get(2));
        assertEquals("New note?", handler.handle("A123", "Mitocondria").get(2));
        assertEquals("Card edited!", handler.handle("A123",
                "It generates most of the cell's supply of chemical energy.").get(0));

        dir = dataComponentRepository.findByUserIdAndDirectoryName("A123",
                "Biology");
        directory = dir.get();
        assertEquals(2, directory.getChildrenSize());

        card = directory.findCardByName("The powerhouse of the cell");
        assertEquals("It generates most of the cell's supply of chemical energy.",
                card.getNotes());
        assertEquals("The powerhouse of the cell", card.getFront());
        assertEquals("Mitocondria", card.getBack());


    }

    @Test
    public void testKeepSameIsWorking() {

        assertEquals("Directory name?", handler.handle("A123", "\\editcard").get(0));
        assertEquals("Card front?", handler.handle("A123", "Biology").get(1));

        handler.handle("A123", "The powerhouse of the cell");

        List<String> newFront = handler.handle("A123", "Front");
        assertEquals("ERROR: Card with front Front already exists.", newFront.get(0));
        assertEquals("New front?", newFront.get(1));

        handler.handle("A123", "keepsame");
        handler.handle("A123", "keepsame");
        handler.handle("A123", "keepsame");

        Optional<Directory> dir = dataComponentRepository.findByUserIdAndDirectoryName("A123",
            "Biology");
        Directory directory = dir.get();
        assertEquals(2, directory.getChildrenSize());

        Card card = directory.findCardByName("The powerhouse of the cell");
        assertEquals("The powerhouse of the cell", card.getFront());
        assertEquals("Nucleus", card.getBack());
        assertEquals("The biochemical processes of the cell are known as cellular respiration.",
            card.getNotes());

        assertFalse(userStateRepository.findById("A123").isPresent());
        assertFalse(editCardStateRepository.findById("A123").isPresent());
    }
}

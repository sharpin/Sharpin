package com.sharpin.sharpin.service;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JsonBuilderTest {

    JsonBuilder jsonBuilder;

    /**
     * Function method to set-up tests.
     */
    @BeforeEach
    public void setUp() {
        jsonBuilder = new JsonBuilder();
    }

    @Test
    public void testCreateEmptyJson() {

        String response = jsonBuilder.createJson(null);
        assertEquals("error_message:[no message for java.lang.NullPointerException]",
            response);
    }

}

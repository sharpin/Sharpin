package com.sharpin.sharpin.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DataServiceGetCardsFromTest {

    @Mock
    private DataComponentRepository dataComponentRepository;

    @Mock
    private NavigationService navigationService;

    @InjectMocks
    private DataService dataRepository = new DataServiceImpl();

    /**
     * Setting up a mock hierarchy.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        Card mock1 = mock(Card.class);
        Card mock2 = mock(Card.class);
        Card mock3 = mock(Card.class);
        Card mock4 = mock(Card.class);
        Card mock5 = mock(Card.class);
        List<Card> cards = new ArrayList<>();
        cards.add(mock1);
        cards.add(mock2);
        cards.add(mock3);
        cards.add(mock4);
        cards.add(mock5);
        Directory mockDir = mock(Directory.class);

        when(navigationService.listCardInHierarchy(mockDir)).thenReturn(cards);
        when(dataComponentRepository.findByUserIdAndDirectoryName("C123", "anime"))
            .thenReturn(java.util.Optional.ofNullable(mockDir));
    }

    @Test
    public void whenDirectoryDoesNotExistShouldThrowException() {
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "anime"))
            .thenThrow(new NoSuchElementException());

        assertThrows(NoSuchElementException.class,
            () -> dataRepository.getCardsFrom("B123", "anime", 10));
    }

    @Test
    public void whenDirectoryExistShouldReturnTheCards() {

        List<Card> returned = dataRepository.getCardsFrom("C123", "anime", 2);
        assertEquals(2, returned.size());
    }

    @Test
    public void whenDirectoryExistAndRequestsAreMoreShouldReturnAllCards() {

        List<Card> returned = dataRepository.getCardsFrom("C123", "anime", 8);
        assertEquals(5, returned.size());
    }

}

package com.sharpin.sharpin.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JsonProcessingHelperImpTest {

    private JsonProcessingHelper jsonProcessingHelper = new JsonProcessingHelperImpl();

    private String jsonStringTest;

    /**
     * Set up tests.
     */
    @BeforeEach
    public void setUp() {

        List<String> tmp = new ArrayList<>();
        tmp.add("Correct answer!");
        tmp.add("The next question");

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("session_ended", true);
        jsonBuilder.addList("responses", tmp);
        jsonBuilder.addEntry("exit_status", 0);
        jsonStringTest = jsonBuilder.createJson("returned_data");
    }

    @Test
    public void whenGetListFromJsonShouldReturnTheList() {

        List<String> ret = jsonProcessingHelper
            .getListFromJson(jsonStringTest, "returned_data", "responses");
        assertEquals(2, ret.size());
        assertEquals("Correct answer!", ret.get(0));
        assertEquals("The next question", ret.get(1));
    }

    @Test
    public void whenGetListFromJsonNotExistShouldThrowException() {
        List<String> ret = jsonProcessingHelper
            .getListFromJson("random string", "returned_data", "responses");

        assertEquals(1, ret.size());
        assertEquals("An error has occured!", ret.get(0));

    }

    @Test
    public void whenGetBooleanFromJsonTrueShouldReturnTheBool() {

        boolean returned = jsonProcessingHelper
            .getBooleanFromJson(jsonStringTest, "returned_data", "session_ended");

        assertTrue(returned);
    }

    @Test
    public void whenGetBooleanFromJsonFalseShouldReturnIt() {

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("session_ended", false);
        jsonBuilder.addEntry("exit_status", 0);
        jsonStringTest = jsonBuilder.createJson("returned_data");

        boolean returned = jsonProcessingHelper
            .getBooleanFromJson(jsonStringTest, "returned_data","session_ended");

        assertFalse(returned);
    }

    @Test
    public void whenGetBooleanErrorShouldReturnFalse() {

        boolean returned = jsonProcessingHelper
            .getBooleanFromJson("random string", "returned_data","session_ended");

        assertFalse(returned);
    }

    @Test
    public void whenGetIntErrorShouldReturnZero() {
        int returned = jsonProcessingHelper
            .getIntFromJson("random string", "returned_data","session_ended");

        assertEquals(0, returned);
    }

    @Test
    public void whenGetLongErrorShouldReturnZero() {
        long returned = jsonProcessingHelper
            .getLongFromJson("random string", "returned_data","session_ended");

        assertEquals(0, returned);
    }

    @Test
    public void whenGetStringFromJsonShouldReturnIt() {
        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("string data", "hello there");
        String jsonString = jsonBuilder.createJson("headerz");

        String returned = jsonProcessingHelper
            .getStringFromJson(jsonString, "headerz", "string data");

        assertEquals("hello there", returned);
    }

    @Test
    public void whenGetStringErrorShouldReturnZero() {
        String returned = jsonProcessingHelper
            .getStringFromJson("random string", "returned_data","session_ended");

        assertEquals(null, returned);
    }

    @Test
    public void whenIoErrorOnGetFieldShouldReturnFalse() {

        boolean returned = jsonProcessingHelper
            .isFieldInJson("random string", "returned_data","session_ended");

        assertFalse(returned);
    }

    @Test
    public void whenFieldExistShouldReturnTrue() {

        boolean returned = jsonProcessingHelper
            .isFieldInJson(jsonStringTest, "returned_data","session_ended");

        assertTrue(returned);
    }

    @Test
    public void whenFieldDoesNotExistShouldReturnFalse() {

        boolean returned = jsonProcessingHelper
            .isFieldInJson(jsonStringTest, "returned_data","doesnot_exist");

        assertFalse(returned);
    }


}

package com.sharpin.sharpin.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DataServiceGetReviewCardsTest {

    @Mock
    private DataComponentRepository dataComponentRepository;

    @Mock
    private NavigationService navigationService;

    @InjectMocks
    private DataService dataRepository = new DataServiceImpl();

    /**
     * Setting up a mock hierarchy.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        Card mock1 = mock(Card.class);
        when(mock1.isReadyForReview()).thenReturn(false);

        Card mock2 = mock(Card.class);
        when(mock2.isReadyForReview()).thenReturn(true);

        Card mock3 = mock(Card.class);
        when(mock3.isReadyForReview()).thenReturn(false);

        Card mock4 = mock(Card.class);
        when(mock4.isReadyForReview()).thenReturn(true);

        Card mock5 = mock(Card.class);
        when(mock5.isReadyForReview()).thenReturn(true);

        List<Card> cards = new ArrayList<>();
        cards.add(mock1);
        cards.add(mock2);
        cards.add(mock3);
        cards.add(mock4);
        cards.add(mock5);
        Directory mockDir = mock(Directory.class);

        when(navigationService.listCardInHierarchy(mockDir)).thenReturn(cards);
        when(dataComponentRepository.findByUserIdAndDirectoryName("C123", "root"))
            .thenReturn(java.util.Optional.ofNullable(mockDir));
    }

    @Test
    public void whenGetReviewCardsCountMeetShouldReturnIt() {

        List<Card> cards = dataRepository.getReviewCards("C123", 2);
        assertEquals(2, cards.size());
    }

    @Test
    public void whenGetReviewCardsOnInsufficientShouldReturnAll() {

        List<Card> cards = dataRepository.getReviewCards("C123", 5);
        assertEquals(3, cards.size());
    }
}

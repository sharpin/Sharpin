package com.sharpin.sharpin.service.handlers.movecard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.HandlerFactory;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class MoveCardHandlerTest {

    @Mock
    MoveCardService moveCardService;

    @Mock
    UserStateService userStateService;

    @Mock
    InitialHandler initialHandler;

    @Mock
    OldDirectoryHandler oldDirectoryHandler;

    @Mock
    CardFrontHandler cardFrontHandler;

    @Mock
    NewDirectoryHandler newDirectoryHandler;

    @InjectMocks
    Handler handler = new MoveCardHandler();

    private List<String> stringToList(String response) {
        List<String> messages = new ArrayList<>();
        messages.add(response);
        return messages;
    }

    /**
    Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        when(initialHandler.getName()).thenReturn("Initial Handler");
        when(oldDirectoryHandler.getName()).thenReturn("Old Directory Handler");
        when(newDirectoryHandler.getName()).thenReturn("New Directory Handler");
        when(cardFrontHandler.getName()).thenReturn("Card Front Handler");

    }


    @Test
    public void testGetNameShouldBeCorrect() {
        assertEquals("\\movecard", handler.getName());
    }

    @Test
    public void whenUserStateDoesNotExistShouldNotCreateIt() {

        when(moveCardService.getState("a123")).thenThrow(NoSuchElementException.class);

        handler.handle("a123", "random message");
        verify(moveCardService, times(0)).createUserData("a123");
    }

    @Test
    public void testHandleWhenNoStateShouldAskForDirectory() {

        when(initialHandler.handle("A123", null)).thenReturn(stringToList("Directory?"));

        when(moveCardService.getState("A123")).thenThrow(NoSuchElementException.class);
        assertEquals("Directory?", handler.handle("A123", null).get(0));
    }

    @Test
    public void testWhenInvalidStageReachedShouldSaySo() {
        when(moveCardService.getState("A123")).thenReturn("invalid state");
        assertEquals("Invalid stage reached!", handler.handle("A123", "random query").get(0));
    }

    @Test
    public void whenInvalidStageReachedShouldClearUserState() {
        when(moveCardService.getState("A123")).thenReturn("invalid state");
        handler.handle("A123", "random query");
        verify(moveCardService, times(1)).deleteUserData("A123");
        verify(userStateService, times(1)).deleteUserData("A123");
    }

    @Test
    public void testWhenUserInStateOldDirectoryShouldGoThere() {

        when(moveCardService.getState("A123")).thenReturn("Old Directory Handler");
        handler.handle("A123", "random message");

        verify(oldDirectoryHandler, times(1)).handle("A123", "random message");
    }

    @Test
    public void whenUserInStateCardFrontShouldGoThere() {
        when(moveCardService.getState("A123")).thenReturn("Card Front Handler");
        handler.handle("A123", "random message");

        verify(cardFrontHandler, times(1)).handle("A123", "random message");
    }

    @Test
    public void whenUserInStateNewDirectoryShoulGoThere() {
        when(moveCardService.getState("A123")).thenReturn("New Directory Handler");
        handler.handle("A123", "random message");

        verify(newDirectoryHandler, times(1)).handle("A123", "random message");
    }

    @Test
    public void whenUserGivesExitShouldEndInteraction() {

        handler.handle("A1234", "\\exit");
        verify(moveCardService).deleteUserData("A1234");
        verify(userStateService).deleteUserData("A1234");
    }

    @Test
    public void whenUserGivesExitShouldGiveMessage() {
        List<String> response = handler.handle("A1234", "\\exit");
        assertEquals("Process canceled", response.get(0));
    }
}

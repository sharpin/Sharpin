package com.sharpin.sharpin.service.handlers.editcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class EditCardInitHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    EditCardService editCardService;

    @InjectMocks
    Handler handler = new EditCardInitHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameShouldReturnCorrectName() {
        assertEquals("Initial Handler", handler.getName());
    }

    @Test
    public void handlerShouldSetUserStateToAddDirectory() {
        handler.handle("1234", null);
        verify(userStateService, times(1)).setUserState("1234", "\\editcard");
    }

    @Test
    public void handlerShouldSetAddDirectoryStateToDirectoryName() {
        handler.handle("1234", null);
        verify(editCardService, times(1)).setState("1234", "Directory Name Handler");
    }

    @Test
    public void whenHandleShouldAskForDirectoryName() {
        assertEquals("Directory name?", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenHandleIsCalledItShouldMoveAddDirectoryState() {
        handler.handle("1234", "message");
        verify(editCardService, times(1)).createUserData("1234");
    }

    @Test
    public void whenHandleIsCalledItShouldCreateUserData() {
        handler.handle("1234", "message");
        verify(userStateService, times(1)).createUserData("1234");
    }

}

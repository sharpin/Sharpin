package com.sharpin.sharpin.service.handlers.addcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AddCardHandlerTest {

    @Mock
    AddCardService addCardService;

    @Mock
    UserStateService userStateService;

    @Mock
    InitialAddCardHandler initialAddCardHandler;

    @Mock
    CardFrontAddCardHandler cardFrontAddCardHandler;

    @Mock
    CardBackAddCardHandler cardBackAddCardHandler;

    @Mock
    CardNotesAddCardHandler cardNotesAddCardHandler;

    @Mock
    DirectoryAddCardHandler directoryAddCardHandler;

    @InjectMocks
    Handler handler = new AddCardHandler();

    private List<String> stringToList(String response) {
        List<String> messages = new ArrayList<>();
        messages.add(response);
        return messages;
    }

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        when(initialAddCardHandler.getName()).thenReturn("Initial Handler");
        when(cardFrontAddCardHandler.getName()).thenReturn("Card Front Handler");
        when(cardBackAddCardHandler.getName()).thenReturn("Card Back Handler");
        when(cardNotesAddCardHandler.getName()).thenReturn("Card Notes Handler");
        when(directoryAddCardHandler.getName()).thenReturn("Directory Handler");
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\addcard", handler.getName());
    }

    @Test
    public void whenUserStateDoesNotExistShouldNotCreateIt() {

        when(addCardService.getState("a1234")).thenThrow(NoSuchElementException.class);

        handler.handle("a1234", "random message");
        verify(addCardService, times(0)).createUserData("a123");
    }

    @Test
    public void testHandleWhenNoStateShouldAskForCardFront() {

        when(initialAddCardHandler.handle("A1234", null)).thenReturn(stringToList("Card front?"));

        when(addCardService.getState("A1234")).thenThrow(NoSuchElementException.class);
        assertEquals("Card front?", handler.handle("A1234", null).get(0));
    }

    @Test
    public void testWhenInvalidStageReachedShouldSaySo() {
        when(addCardService.getState("A1234")).thenReturn("invalid state");
        assertEquals("Invalid stage reached!", handler.handle("A1234", "random query").get(0));
    }

    @Test
    public void whenInvalidStageReachedShouldClearUserState() {
        when(addCardService.getState("A1234")).thenReturn("invalid state");
        handler.handle("A1234", "random query");
        verify(addCardService, times(1)).deleteUserData("A1234");
        verify(userStateService, times(1)).deleteUserData("A1234");
    }

    @Test
    public void whenUserInStateCardFrontShouldGoThere() {
        when(addCardService.getState("A1234")).thenReturn("Card Front Handler");
        handler.handle("A1234", "random message");

        verify(cardFrontAddCardHandler, times(1)).handle("A1234", "random message");
    }

    @Test
    public void whenUserInStateCardBackShouldGoThere() {
        when(addCardService.getState("A1234")).thenReturn("Card Back Handler");
        handler.handle("A1234", "random message");

        verify(cardBackAddCardHandler, times(1)).handle("A1234", "random message");
    }

    @Test
    public void whenUserInStateCardNotesShouldGoThere() {
        when(addCardService.getState("A1234")).thenReturn("Card Notes Handler");
        handler.handle("A1234", "random message");

        verify(cardNotesAddCardHandler, times(1)).handle("A1234", "random message");
    }

    @Test
    public void whenUserInStateDirectoryShoulGoThere() {
        when(addCardService.getState("A1234")).thenReturn("Directory Handler");
        handler.handle("A1234", "random message");

        verify(directoryAddCardHandler, times(1)).handle("A1234", "random message");
    }

    @Test
    public void whenUserGivesExitShouldEndInteraction() {

        handler.handle("A1234", "\\exit");
        verify(addCardService).deleteUserData("A1234");
        verify(userStateService).deleteUserData("A1234");
    }

    @Test
    public void whenUserGivesExitShouldGiveMessage() {
        List<String> response = handler.handle("A1234", "\\exit");
        assertEquals("Process canceled", response.get(0));
    }

}

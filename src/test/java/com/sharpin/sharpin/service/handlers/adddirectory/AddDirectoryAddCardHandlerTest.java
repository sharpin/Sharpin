package com.sharpin.sharpin.service.handlers.adddirectory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AddDirectoryAddCardHandlerTest {

    @Mock
    AddDirectoryService addDirectoryService;

    @Mock
    UserStateService userStateService;

    @Mock
    InitHandler initHandler;

    @Mock
    DirectoryNameHandler directoryNameHandler;

    @Mock
    ParentDirectoryHandler parentDirectoryHandler;

    @InjectMocks
    Handler handler = new AddDirectoryHandler();

    private List<String> stringToList(String response) {
        List<String> messages = new ArrayList<>();
        messages.add(response);
        return messages;
    }

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(initHandler.getName()).thenReturn("Initial Handler");
        when(directoryNameHandler.getName()).thenReturn("Directory Name Handler");
        when(parentDirectoryHandler.getName()).thenReturn("Parent Directory Handler");
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {

        assertEquals("\\adddirectory", handler.getName());
    }

    @Test
    public void whenUserStateDoesNotExistShouldNotCreateIt() {
        when(addDirectoryService.getState("1234")).thenThrow(NoSuchElementException.class);

        handler.handle("1234", "message");
        verify(addDirectoryService, times(0)).createUserData("1234");
    }

    @Test
    public void whenNoStateHandlerShouldAskForDirectoryName() {
        when(initHandler.handle("1234", null)).thenReturn(stringToList("Directory name?"));
        when(addDirectoryService.getState("1234")).thenThrow(NoSuchElementException.class);
        assertEquals("Directory name?", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenInStateDirectoryNameShouldGoThere() {
        when(addDirectoryService.getState("1234")).thenReturn("Directory Name Handler");
        handler.handle("1234", "message");
        verify(directoryNameHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInStateParentDirectoryShouldGoThere() {
        when(addDirectoryService.getState("1234")).thenReturn("Parent Directory Handler");
        handler.handle("1234", "message");
        verify(parentDirectoryHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInvalidStageReachedShouldSaySo() {
        when(addDirectoryService.getState("1234")).thenReturn("invalid state");
        assertEquals("Invalid stage reached!", handler.handle("1234", "message").get(0));
    }

    @Test
    public void whenInvalidStageReachedShouldClearUserState() {
        when(addDirectoryService.getState("1234")).thenReturn("invalid state");
        handler.handle("1234", "message");
        verify(addDirectoryService, times(1)).deleteUserData("1234");
        verify(userStateService, times(1)).deleteUserData("1234");
    }
}

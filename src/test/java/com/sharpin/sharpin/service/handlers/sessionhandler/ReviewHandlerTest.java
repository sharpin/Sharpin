package com.sharpin.sharpin.service.handlers.sessionhandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.card.UserCardList;
import com.sharpin.sharpin.service.DataService;
import com.sharpin.sharpin.service.JsonProcessingHelper;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;

public class ReviewHandlerTest {

    @Mock
    JsonProcessingHelper helper;

    @Mock
    UserStateService userStateService;

    @Mock
    DataService dataService;

    @Mock
    Environment environment;

    @InjectMocks
    ReviewHandler handler = new ReviewHandler();

    List<Card> notEmptyList;

    /**
     * Function to set up tests.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        when(environment.getProperty("SESSION_URL")).thenReturn("http://localhost:8081");

        notEmptyList = new ArrayList<>();
        Card mockCard = mock(Card.class);
        notEmptyList.add(mockCard);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\review", handler.getName());
    }

    @Test
    public void whenCalledOnReviewExistShouldCreateUserData() {

        ArrayList<Card> notEmpty = new ArrayList<>();
        notEmpty.add(mock(Card.class));
        when(dataService.getReviewCards("A123", 5))
            .thenReturn(notEmpty);
        handler.handle("A123", "5");
        verify(userStateService).createUserData("A123");
    }

    @Test
    public void whenReviewOnNoReviewsShouldNotSetState() {

        ArrayList<Card> emptyList = new ArrayList<>();

        when(dataService.getReviewCards("D123", 5)).thenReturn(emptyList);

        handler.handle("D123", "5");
        verify(userStateService, times(0)).setUserState(eq("D123"), anyString());
    }

    @Test
    public void whenReviewOnNoReviewShouldReturnMessage() {
        ArrayList<Card> emptyList = new ArrayList<>();

        when(dataService.getReviewCards("D123", 5)).thenReturn(emptyList);

        List<String> response = handler.handle("D123", "5");
        assertEquals(1, response.size());
        assertEquals("No cards to review", response.get(0));
    }

    @Test
    public void whenCalledOnNullShouldSetUserState() {

        ArrayList<Card> notEmpty = new ArrayList<>();
        notEmpty.add(mock(Card.class));
        when(dataService.getReviewCards("A123", 5))
            .thenReturn(notEmpty);

        handler.handle("A123", null);
        verify(userStateService).setUserState("A123", "session");
    }

    @Test
    public void whenCalledOnNoNumberShouldCall5() {

        handler.handle("A123", null);
        verify(dataService).getReviewCards("A123", 5);

    }

    @Test
    public void whenCalledOnBadNumberShouldCall5() {

        handler.handle("A123", "bad");
        verify(dataService).getReviewCards("A123", 5);

    }

    @Test
    public void whenCalledWithNumberShouldCallIt() {
        handler.handle("A123", "7");
        verify(dataService).getReviewCards("A123", 7);
    }

    /**
     * Helper function to serialize cards.
     */
    public String serializeCards(UserCardList userCardList) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userCardList);
    }

    @Test
    public void whenPostReviewShouldPostCorrectJson() throws JsonProcessingException {

        Card card1 = new NormalCard("Card1", "Back of Card1", "notes of card1");
        Card card2 = new NormalCard("Card2", "Back of Card2", "notes of card2");
        card1.setCardId(1);
        card2.setCardId(2);
        List<Card> cardList = new ArrayList<>();
        cardList.add(card1);
        cardList.add(card2);

        UserCardList tst = new UserCardList("A123", cardList);
        String expectedJson = serializeCards(tst);
        handler.postReview("A123", cardList);
        verify(helper).postRequest(anyString(), eq(expectedJson));
    }

    @Test
    public void whenPostReviewShouldReturnCorrectString() throws JsonProcessingException {

        Card card1 = new NormalCard("Card1", "Back of Card1", "notes of card1");
        Card card2 = new NormalCard("Card2", "Back of Card2", "notes of card2");
        card1.setCardId(1);
        card2.setCardId(2);
        List<Card> cardList = new ArrayList<>();
        cardList.add(card1);
        cardList.add(card2);

        UserCardList tst = new UserCardList("A123", cardList);
        String expectedJson = serializeCards(tst);

        List<String> responses = new ArrayList<>();
        responses.add("Review initialized!");
        responses.add("Next question: Card1");

        when(helper.postRequest(anyString(), eq(expectedJson))).thenReturn("responseJson");
        when(helper.getListFromJson("responseJson", "returned_data","responses"))
            .thenReturn(responses);

        List<String> gotResponses = handler.postReview("A123", tst);
        assertEquals("Review initialized!", responses.get(0));
        assertEquals("Next question: Card1", responses.get(1));
    }

    @Test
    public void whenJsonProcessingErrorShouldGiveTheList() throws JsonProcessingException {

        when(dataService.getReviewCards("a123", 2))
            .thenReturn(notEmptyList);

        when(helper.postRequest(anyString(), anyString()))
            .thenThrow(new NullPointerException());

        List<String> responses = handler.handle("a123", "2");
        assertEquals(1, responses.size());
        assertEquals("Error when parsing, please try again!", responses.get(0));

    }

}

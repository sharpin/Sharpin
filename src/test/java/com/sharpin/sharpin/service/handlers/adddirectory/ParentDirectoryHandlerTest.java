package com.sharpin.sharpin.service.handlers.adddirectory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ParentDirectoryHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    AddDirectoryService addDirectoryService;

    @InjectMocks
    Handler handler = new ParentDirectoryHandler();

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameShouldReturnCorrectName() {
        assertEquals("Parent Directory Handler", handler.getName());
    }

    @Test
    public void whenNullMessageReceivedShouldReturnCantBeNull() {
        assertEquals("Directory name can't be empty!", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenNullMessageReceivedShouldNotChangeState() {
        handler.handle("1234", null);
        verify(addDirectoryService, times(0)).setState(eq("1234"), anyString());
    }

    @Test
    public void whenNullMessageReceivedShouldNotSaveName() {
        handler.handle("1234", null);
        verify(addDirectoryService, times(0)).setDirectoryName(eq("1234"), anyString());
    }

    @Test
    public void whenGivenMessageShouldSaveMessage() {
        when(addDirectoryService.setParentDirectory("1234","root")).thenReturn(true);
        handler.handle("1234", "root");
        verify(addDirectoryService, times(1)).setParentDirectory("1234", "root");
    }

    @Test
    public void whenGivenMessageShouldCallHandleChanges() {
        when(addDirectoryService.setParentDirectory("1234","root")).thenReturn(true);
        handler.handle("1234", "root");
        verify(addDirectoryService, times(1)).handleChanges("1234");
    }

    @Test
    public void whenGivenMessageShouldEraseUserData() {
        when(addDirectoryService.setParentDirectory("1234","root")).thenReturn(true);
        handler.handle("1234", "root");
        verify(addDirectoryService, times(1)).deleteUserData("1234");
        verify(userStateService, times(1)).deleteUserData("1234");
    }

    @Test
    public void whenGivenMessageShouldReturnCorrectResponse() {
        when(addDirectoryService.setParentDirectory("1234","root")).thenReturn(true);
        when(addDirectoryService.handleChanges("1234")).thenReturn("Directory added!");

        assertEquals("Directory added!",
                handler.handle("1234", "root").get(0));
    }

    @Test
    public void whenDirectoryDoesNotExistShouldGiveError() {
        when(addDirectoryService.setDirectoryName("1234", "random")).thenReturn(false);

        List<String> responses = handler.handle("1234", "random");
        assertEquals(2, responses.size());
        assertEquals("ERROR: Directory doesn't exist", responses.get(0));
        assertEquals("Parent directory?", responses.get(1));
    }

}

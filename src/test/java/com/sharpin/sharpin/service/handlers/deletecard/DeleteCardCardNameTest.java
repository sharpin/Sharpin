package com.sharpin.sharpin.service.handlers.deletecard;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DeleteCardCardNameTest {

    @Mock
    DeleteCardService deleteCardService;

    @Mock
    UserStateService userStateService;

    @InjectMocks
    private Handler handler = new DeleteCardCardNameHandler();

    /**
     * Setting up for the test cases.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("Card Name Handler", handler.getName());
    }

    @Test
    public void whenGivenNullShouldNotDeleteState() {
        handler.handle("A123", null);
        verify(deleteCardService, times(0))
            .deleteUserData(eq("A123"));
    }

    @Test
    public void whenGivenNullShouldReturnMessage() {
        List<String> response = handler.handle("B123", null);
        assertEquals(1, response.size());
        assertEquals("Card front cant be empty, please give a card front",
            response.get(0));
    }

    @Test
    public void whenGivenNonExistentCardShouldNotDeleteState() {
        when(deleteCardService.setCardFront("C123", "hokage"))
            .thenReturn(false);
        handler.handle("C123", "hokage");
        verify(deleteCardService, times(0))
            .deleteUserData("C123");
    }

    @Test
    public void whenGivenNonExistentCardShouldGiveMessage() {
        when(deleteCardService.setCardFront("C123", "hokage"))
            .thenReturn(false);
        List<String> response = handler.handle("C123", "hokage");
        assertEquals(1, response.size());
        assertEquals("The card front does not exist, please give a valid card front",
            response.get(0));
    }

    @Test
    public void whenGivenExistCardFrontShouldEndInteraction() {
        when(deleteCardService.setCardFront("A123", "mizukage"))
            .thenReturn(true);

        handler.handle("A123", "mizukage");
        verify(deleteCardService).deleteUserData("A123");
        verify(userStateService).deleteUserData("A123");
    }

    @Test
    public void whenGivenExistCardShouldHandleChanges() {
        when(deleteCardService.setCardFront("A123", "mizukage"))
            .thenReturn(true);

        handler.handle("A123", "mizukage");
        verify(deleteCardService).handleChanges("A123");
    }

    @Test
    public void whenGivenExistCardShouldReturnCorrectResponse() {
        when(deleteCardService.setCardFront("B123", "raikage"))
            .thenReturn(true);
        when(deleteCardService.handleChanges("B123"))
            .thenReturn("Card with front \"raikage\" has been deleted from directory kages");

        List<String> response = handler.handle("B123", "raikage");
        assertEquals(1, response.size());
        assertEquals("Card with front \"raikage\" has been deleted from directory kages",
            response.get(0));
    }
}

package com.sharpin.sharpin.service.handlers.listhandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.*;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ListHandlerTest {

    @Mock
    DataComponentRepository dataComponentRepository;

    @Mock
    NavigationService navigationService;

    @InjectMocks
    Handler handler = new ListHandler();

    @BeforeEach
    public void setUp() {
        navigationService = new NavigationServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\list", handler.getName());
    }

    @Test
    public void handlerListDirShouldReturnCorrectAnswer() {

        List<Directory> result = new ArrayList<>();

        Directory childDir = new Directory("Pigeon", "A123");
        result.add(childDir);
        Directory childDir2 = new Directory("Chicken", "A123");
        result.add(childDir2);

        Directory newDir = new Directory("Bird and Chicken", "A123");
        Optional<Directory> dir = Optional.of(newDir);

        when(dataComponentRepository.findByUserIdAndDirectoryName(
                "A123", "Bird and Chicken")).thenReturn(dir);
        when(navigationService.navigateDirectory(newDir)).thenReturn(result);

        List<String> response = handler.handle("A123", "dir Bird and Chicken");
        String expectedResult = "In directory Bird and Chicken. List of directories:\n"
            + "- Pigeon\n"
            + "- Chicken\n";
        assertEquals(expectedResult, response.get(0));
        assertEquals(1, response.size());
    }

    @Test
    public void handlerListDirNotFound() {
        List<String> response =  handler.handle("A123", "dir Bird and Chicken");
        assertEquals("ERROR: Directory doesn't exist\n"
                + "Please try another directory", response.get(0));
        assertEquals(1, response.size());

    }

    @Test
    public void handlerListDirEmpty() {
        Directory newDir = new Directory("Bird and Chicken", "A123");
        Optional<Directory> dir = Optional.of(newDir);
        List<Directory> result = new ArrayList<>();

        when(dataComponentRepository.findByUserIdAndDirectoryName(
                "A123", "Bird and Chicken")).thenReturn(dir);
        when(navigationService.navigateDirectory(newDir)).thenReturn(result);

        List<String> response = handler.handle("A123", "dir Bird and Chicken");
        assertEquals("In directory Bird and Chicken. This directory does not contain any directory",
                response.get(0));
        assertEquals(1, response.size());
    }

    @Test
    public void handlerListCardsShouldReturnCorrectAnswer() {
        Directory newDir = new Directory("Bird and Chicken", "A123");

        List<Card> cardsResult = new ArrayList<>();
        Card newCard = new NormalCard("PigeonCard", "Animal", "Notes");
        cardsResult.add(newCard);
        Card newCard2 = new NormalCard("Chicken", "Animal", "Notes");
        cardsResult.add(newCard2);

        Optional<Directory> dir = Optional.of(newDir);

        when(dataComponentRepository.findByUserIdAndDirectoryName(
                "A123", "Bird and Chicken")).thenReturn(dir);
        when(navigationService.listCardInDirectory(newDir)).thenReturn(cardsResult);

        List<String> response = handler.handle("A123", "card Bird and Chicken");
        String expectedResult = "In directory Bird and Chicken. List of cards in this directory:\n"
            + "- PigeonCard\n"
            + "- Chicken\n";
        assertEquals(expectedResult, response.get(0));
        assertEquals(1, response.size());
    }

    @Test
    public void handlerListCardEmpty() {
        Directory newDir = new Directory("Bird and Chicken", "A123");
        Optional<Directory> dir = Optional.of(newDir);
        List<Card> result = new ArrayList<>();

        when(dataComponentRepository.findByUserIdAndDirectoryName(
                "A123", "Bird and Chicken")).thenReturn(dir);
        when(navigationService.listCardInDirectory(newDir)).thenReturn(result);

        List<String> response = handler.handle("A123", "card Bird and Chicken");
        assertEquals("In directory Bird and Chicken. This directory does not contain any card",
                response.get(0));
        assertEquals(1, response.size());
    }

    @Test
    public void handlerListCardDirNotFound() {

        List<String> response = handler.handle("A123", "card Bird and Chicken");
        assertEquals("ERROR: Directory doesn't exist\n"
                + "Please try another directory", response.get(0));
        assertEquals(1, response.size());

    }

    @Test
    public void whenGivenInvalidCommandShouldSaySo() {

        String expectedResult = "ERROR: Invalid list command. "
            + "Please enter card or dir as the second command.\n"
            + "For example: \\list card root";
        List<String> response = handler.handle("A123", "random Bird and Chicken");
        assertEquals(expectedResult, response.get(0));
    }

    @Test
    public void whenGivenNoDirectoryShouldSaySo() {
        String expectedResult = "ERROR: Please give a directory name.\n"
            + "For example: \\list card root";
        List<String> response = handler.handle("A123", "card");
        assertEquals(expectedResult, response.get(0));
    }
}

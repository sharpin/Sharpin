package com.sharpin.sharpin.service.handlers.sessionhandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.JsonBuilder;
import com.sharpin.sharpin.service.JsonProcessingHelper;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;

public class SessionHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    JsonProcessingHelper jsonProcessingHelper;

    @Mock
    DataComponentRepository dataComponentRepository;

    @Mock
    Environment environment;

    @InjectMocks
    Handler handler = new SessionHandler();

    private String expectedAnswerJson;
    private String expectedResponseOnContinue;
    private String expectedResponseOnStop;
    private List<String> expectedResponses;
    private JsonBuilder jsonBuilder;

    /**
     * Function to set up tests.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("answer", "answer of the question");
        jsonBuilder.addEntry("userId", "A123");
        expectedAnswerJson = jsonBuilder.createJson("answer_data");

        expectedResponses = new ArrayList<>();
        expectedResponses.add("Correct answer!");
        expectedResponses.add("Your notes for this card:\n\nnotes for this card");
        expectedResponses.add("Next question: The next question");

        JsonBuilder jsonBuilder1 = new JsonBuilder();
        jsonBuilder1.addEntry("session_ended", false);
        jsonBuilder1.addList("responses", expectedResponses);
        jsonBuilder1.addEntry("exit_status", 0);
        expectedResponseOnContinue = jsonBuilder1.createJson("returned_data");

        JsonBuilder jsonBuilder2 = new JsonBuilder();
        jsonBuilder2.addEntry("session_ended", true);
        jsonBuilder2.addList("responses", expectedResponses);
        jsonBuilder2.addEntry("exit_status", 0);
        expectedResponseOnStop = jsonBuilder2.createJson("returned_data");

        when(environment.getProperty("SESSION_URL")).thenReturn("http://localhost:8081");
    }

    // karena list bisa di modified, setiap memanggil harus memanggil ini untuk fresh copy
    private List<String> getExpectedResponseList() {
        List<String> copyOfExpectedResponses = new ArrayList<>();
        copyOfExpectedResponses.addAll(expectedResponses);
        return copyOfExpectedResponses;
    }

    @Test
    public void whenGetNameShouldReturnName() {
        assertEquals("session", handler.getName());
    }

    @Test
    public void whenAnswerShouldPostTheRightJson() {
        handler.handle("A123", "answer of the question");

        verify(jsonProcessingHelper).postRequest(anyString(), eq(expectedAnswerJson));
    }

    @Test
    public void whenSessionNotEndedShouldReturnCorrectList() {

        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(expectedResponseOnContinue);
        when(jsonProcessingHelper.getListFromJson(expectedResponseOnContinue,
            "returned_data", "responses")).thenAnswer(i -> getExpectedResponseList());

        List<String> responses = handler.handle("A123", "answer of the question");
        assertEquals(3, responses.size());
        assertEquals("Correct answer!", responses.get(0));
        assertEquals("Your notes for this card:\n\nnotes for this card", responses.get(1));
        assertEquals("Next question: The next question", responses.get(2));

    }

    @Test
    public void whenSessionNotEndedShouldNotRemoveUserState() {

        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(expectedResponseOnContinue);
        when(jsonProcessingHelper
            .getBooleanFromJson(expectedResponseOnContinue, "returned_data", "session_ended"))
            .thenReturn(false);

        handler.handle("a123", "answer to the question");
        verify(userStateService, times(0)).deleteUserData("a123");
    }

    @Test
    public void whenSessionEndedShouldRemoveUserState() {

        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(expectedResponseOnStop);
        when(jsonProcessingHelper
            .getBooleanFromJson(expectedResponseOnStop, "returned_data", "session_ended"))
            .thenReturn(true);

        handler.handle("A123", "answer of the question");
        verify(userStateService).deleteUserData("A123");
    }

    private void addStubMethodSrsAndCardId(JsonProcessingHelper jsonProcessingHelper,
                                           String jsonString,
                                           int srsUpdate,
                                           long answeredCardId) {
        when(jsonProcessingHelper.getIntFromJson(jsonString, "returned_data", "srsUpdate"))
            .thenReturn(srsUpdate);

        when(jsonProcessingHelper.getLongFromJson(jsonString, "returned_data", "answeredCardId"))
            .thenReturn(answeredCardId);

        when(jsonProcessingHelper.isFieldInJson(jsonString, "returned_data", "srsUpdate"))
            .thenReturn(true);
    }

    private void addSrsAndCardId(JsonBuilder jsonBuilder, int srsUpdate, int answeredCardId) {
        jsonBuilder.addEntry("srsUpdate", srsUpdate);
        jsonBuilder.addEntry("answeredCardId", answeredCardId);
    }

    @Test
    public void whenJsonHasSrsUpdatePlusShouldIncreaseSrs() {

        Card mockCard = mock(Card.class);
        when(dataComponentRepository.findById((long) 12))
            .thenReturn(Optional.of(mockCard));

        addSrsAndCardId(jsonBuilder, 1, 12);

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, 1, 12);

        handler.handle("A123", "answer of the question");
        verify(mockCard).increaseSrsStage();
        verify(mockCard, times(0)).decreaseSrsStage();

    }

    @Test
    public void whenJsonHasSrsUpdatePlusShouldGiveResponse() {

        Card mockCard = mock(Card.class);
        when(dataComponentRepository.findById((long) 12))
            .thenReturn(Optional.of(mockCard));
        when(mockCard.getFront()).thenReturn("Front of Card");
        when(mockCard.getSrsStage()).thenReturn(5);

        addSrsAndCardId(jsonBuilder, 1, 12);

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);
        when(jsonProcessingHelper.getListFromJson(jsonString, "returned_data", "responses"))
            .thenAnswer(i -> getExpectedResponseList());
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, 1, 12);

        List<String> responses = handler.handle("A123", "answer of the question");
        assertEquals(4, responses.size());
        assertEquals("Card with front \"Front of Card\" SRS has been increased to 5",
            responses.get(2));
        assertEquals("Next question: The next question", responses.get(3));
    }

    @Test
    public void whenJsonHasSrsUpdateMinusShouldDecreaseSrs() {

        Card mockCard = mock(Card.class);
        when(dataComponentRepository.findById((long) 12))
            .thenReturn(Optional.of(mockCard));

        addSrsAndCardId(jsonBuilder, -1, 12);

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, -1, 12);

        handler.handle("A123", "answer of the question");
        verify(mockCard).decreaseSrsStage();
        verify(mockCard, times(0)).increaseSrsStage();
    }

    @Test
    public void whenJsonHasSrsUpdateMinusShouldGiveResponse() {

        Card mockCard = mock(Card.class);
        when(dataComponentRepository.findById((long) 12))
            .thenReturn(Optional.of(mockCard));
        when(mockCard.getFront()).thenReturn("Front of Card");
        when(mockCard.getSrsStage()).thenReturn(3);

        addSrsAndCardId(jsonBuilder, -1, 12);

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);

        when(jsonProcessingHelper.getListFromJson(jsonString, "returned_data", "responses"))
            .thenAnswer(i -> getExpectedResponseList());
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, -1, 12);

        List<String> responses = handler.handle("A123", "answer of the question");
        assertEquals(4, responses.size());
        assertEquals("Card with front \"Front of Card\" SRS has been decreased to 3",
            responses.get(2));
        assertEquals("Next question: The next question", responses.get(3));
    }

    @Test
    public void whenJsonHasSrsUpdateButErrorShouldGiveError() {

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);

        when(jsonProcessingHelper.getListFromJson(jsonString, "returned_data", "responses"))
            .thenAnswer(i -> getExpectedResponseList());
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, -1, 12);
        when(jsonProcessingHelper.getLongFromJson(jsonString, "returned_data", "answeredCardId"))
            .thenThrow(new NoSuchElementException("Card does not exist in database"));

        List<String> response = handler.handle("A123", "answer of the question");
        assertEquals(1, response.size());
        assertEquals("Card does not exist in database", response.get(0));
    }

    @Test
    public void whenJsonHasSrsUpdateZeroShouldIncreaseSrs() {

        Card mockCard = mock(Card.class);
        when(dataComponentRepository.findById((long) 12))
            .thenReturn(Optional.of(mockCard));

        addSrsAndCardId(jsonBuilder, 0, 12);

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, 0, 12);

        handler.handle("A123", "answer of the question");
        verify(mockCard, times(0)).decreaseSrsStage();
        verify(mockCard, times(0)).increaseSrsStage();
    }

    @Test
    public void whenJsonHasUpdateShouldSaveCard() {

        Card mockCard = mock(Card.class);
        when(dataComponentRepository.findById((long) 12))
            .thenReturn(Optional.of(mockCard));

        addSrsAndCardId(jsonBuilder, 1, 12);

        String jsonString = jsonBuilder.createJson("returned_data");
        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(jsonString);
        addStubMethodSrsAndCardId(jsonProcessingHelper, jsonString, 1, 12);

        handler.handle("A123", "answer of the question");
        ArgumentCaptor<Card> argumentCaptor = ArgumentCaptor.forClass(Card.class);
        verify(dataComponentRepository).save(argumentCaptor.capture());
        assertEquals(mockCard, argumentCaptor.getValue());
    }

    @Test
    public void whenPostingFailsShouldReturnAnErrorOccurred() {

        when(jsonProcessingHelper
            .getIntFromJson(expectedResponseOnContinue, "returned_data", "exit_status"))
            .thenReturn(1);

        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(expectedResponseOnContinue);

        List<String> responses = handler.handle("A123", "answer of the question");
        assertEquals(1, responses.size());
        assertEquals("An error occurred, please try again!", responses.get(0));
    }

    @Test
    public void whenPostingFailsShouldDeleteUserData() {

        when(jsonProcessingHelper
            .getIntFromJson(expectedResponseOnContinue, "returned_data", "exit_status"))
            .thenReturn(1);

        when(jsonProcessingHelper.postRequest(anyString(), eq(expectedAnswerJson)))
            .thenReturn(expectedResponseOnContinue);

        handler.handle("A123", "answer of the question");
        verify(userStateService).deleteUserData("A123");

    }

    @Test
    public void whenUserGivesExitShouldEndInteraction() {

        handler.handle("A1234", "\\exit");
        verify(userStateService).deleteUserData("A1234");
    }

    @Test
    public void whenUserGivesExitShouldGiveMessage() {
        List<String> response = handler.handle("A1234", "\\exit");
        assertEquals("Process canceled", response.get(0));
    }
}

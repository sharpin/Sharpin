package com.sharpin.sharpin.service.handlers.movecard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class InitialHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    MoveCardService moveCardService;

    @InjectMocks
    Handler handler = new InitialHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetNameShouldReturnCorrectName() {
        assertEquals("Initial Handler", handler.getName());
    }

    @Test
    public void testHandleShouldSetUserStateToMoveCard() {

        handler.handle("A123", null);
        verify(userStateService, times(1)).setUserState("A123", "\\movecard");
    }

    @Test
    public void testHandleShouldSetMoveCardStateToAddDirectoryName() {
        handler.handle("A123", null);
        verify(moveCardService, times(1)).setState("A123", "Old Directory Handler");
    }

    @Test
    public void testWhenHandleShouldAskForDirectory() {

        assertEquals("Directory?", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenHandleIsCalledItShouldMoveCardState() {

        handler.handle("A123", "lul");
        verify(moveCardService, times(1)).createUserData("A123");
    }

    @Test
    public void whenHandleIsCalledItShouldCreateUserState() {

        handler.handle("A123", "lul");
        verify(userStateService, times(1)).createUserData("A123");
    }
}

package com.sharpin.sharpin.service.handlers.statisticshandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.CardAnswerData;
import com.sharpin.sharpin.service.LevelData;
import com.sharpin.sharpin.service.StatisticsService;
import com.sharpin.sharpin.service.StatisticsServiceImpl;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class StatisticsHandlerTest {

    @Mock
    DataComponentRepository dataComponentRepository;

    @Mock
    StatisticsService statisticsService;

    @Mock
    UserStateService userStateService;

    @InjectMocks
    Handler handler = new StatisticsHandler();

    @BeforeEach
    public void setUp() {
        statisticsService = new StatisticsServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    private List<String> stringToList(String response) {
        List<String> messages = new ArrayList<>();
        messages.add(response);
        return messages;
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\statistics", handler.getName());
    }

    @Test
    public void handlerStatLevelReturnCorrectAnswer() {
        Directory newDir = new Directory("Bird","A123");
        Optional<Directory> dir = Optional.of(newDir);
        when(dataComponentRepository.findByUserIdAndDirectoryName("A123","Bird")).thenReturn(dir);

        List<LevelData> levelResult = new ArrayList<>();
        levelResult.add(new LevelData(1,1));
        levelResult.add(new LevelData(3, 5));
        levelResult.add(new LevelData(7, 2));
        when(statisticsService.getNumberOfCardsInEachLevel(newDir)).thenReturn(levelResult);

        List<String> response = handler.handle("A123", "statlevel Bird");
        String expectedResult = "Number of cards in each level on directory Bird:\n"
            + "- Level 1: 1\n"
            + "- Level 3: 5\n"
            + "- Level 7: 2\n";
        assertEquals(expectedResult, response.get(0));
        assertEquals(1, response.size());
    }


    @Test
    public void handlerStatMissReturnCorrectAnswer() {
        Directory newDir = new Directory("Bird","A123");
        Optional<Directory> dir = Optional.of(newDir);
        when(dataComponentRepository.findByUserIdAndDirectoryName("A123","Bird")).thenReturn(dir);

        List<CardAnswerData> ratioResult = new ArrayList<>();
        ratioResult.add(new CardAnswerData("Pigeon", 10.0));
        ratioResult.add(new CardAnswerData("Chicken", 20.0));
        ratioResult.add(new CardAnswerData("Hen", 50.0));
        when(statisticsService.getMostWrongAnsweredCard(newDir,5)).thenReturn(ratioResult);

        List<String> response = handler.handle("A123", "statmiss Bird");
        String expectedResult = "Cards in this directory that has the lowest correct answer rate:\n"
            + "- Pigeon (10.0%)\n"
            + "- Chicken (20.0%)\n"
            + "- Hen (50.0%)\n";
        assertEquals(expectedResult, response.get(0));
        assertEquals(1, response.size());

    }

    @Test
    public void whenGivenInvalidStatisticsCommandShouldGiveError() {

        List<String> response = handler.handle("A123", "badcommand Bird");
        String expectedResult = "ERROR: invalid statistics command, "
            + "please enter statmiss or statlevel as the second command\n"
            + "For example: \\statistics statmiss root";
        assertEquals(expectedResult, response.get(0));
    }

    @Test
    public void whenGivenNoDirectoryShouldGiveError() {
        List<String> response = handler.handle("A123", "statlevel");
        String expectedResult = "ERROR: please give a directory name\n"
            + "For example: \\statistics statmiss root";
        assertEquals(expectedResult, response.get(0));
    }
}

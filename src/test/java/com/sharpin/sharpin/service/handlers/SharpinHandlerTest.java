package com.sharpin.sharpin.service.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.handlers.addcard.AddCardHandler;
import com.sharpin.sharpin.service.handlers.adddirectory.AddDirectoryHandler;
import com.sharpin.sharpin.service.handlers.deletecard.DeleteCardHandler;
import com.sharpin.sharpin.service.handlers.editcard.EditCardHandler;
import com.sharpin.sharpin.service.handlers.movecard.MoveCardHandler;
import com.sharpin.sharpin.service.handlers.sessionhandler.SessionHandler;
import com.sharpin.sharpin.storage.Directory;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class SharpinHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    MoveCardHandler moveCard;

    @Mock
    SessionHandler sessionHandler;

    @Mock
    AddCardHandler addCardHandler;

    @Mock
    EditCardHandler editCardHandler;

    @Mock
    NoStateHandler noStateHandler;

    @Mock
    AddDirectoryHandler addDirectoryHandler;

    @Mock
    DataComponentRepository dataComponentRepository;

    @Mock
    DeleteCardHandler deleteCardHandler;

    @InjectMocks
    Handler handler = new SharpinHandler();

    /**
     * Function to set up test.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        //when(startingHandlers.create(anyString())).thenReturn(moveCard);
        when(moveCard.getName()).thenReturn("\\movecard");
        when(addCardHandler.getName()).thenReturn("\\addcard");
        when(editCardHandler.getName()).thenReturn("\\editcard");
        when(addDirectoryHandler.getName()).thenReturn("\\adddirectory");
        when(noStateHandler.getName()).thenReturn("No State Handler");
        when(sessionHandler.getName()).thenReturn("session");
        when(deleteCardHandler.getName()).thenReturn("Delete Card Handler");
    }

    @Test
    public void testGetNameShouldReturnCorrectName() {

        assertEquals("Sharpin Handler", handler.getName());
    }

    @Test
    public void whenUserStateDoesNotExistShouldNotCreateIt() {
        when(userStateService.getUserState("a123")).thenThrow(NoSuchElementException.class);
        handler.handle("a123", "\\random command");
        verify(userStateService, times(0)).createUserData("a123");
    }

    @Test
    public void testUserDoesNotExistShouldCallNoStateHandler() {

        when(userStateService.getUserState("a123")).thenThrow(NoSuchElementException.class);
        handler.handle("a123", "\\version");
        verify(noStateHandler, times(1)).handle("a123", "\\version");
    }

    @Test
    public void testInvalidCheckPointShouldReturnIt() {
        when(userStateService.getUserState("a123")).thenReturn("Invalid Checkpoint");
        assertEquals("Invalid checkpoint reached", handler.handle("a123", "\\version").get(0));
    }

    @Test
    public void whenInvalidStateShouldEraseUserData() {
        when(userStateService.getUserState("a123")).thenReturn("invalid checkpoint");
        handler.handle("a123", "\\version");
        verify(userStateService, times(1)).deleteUserData("a123");
    }

    @Test
    public void whenMoveCardStateExistShouldGoToThat() {

        when(userStateService.getUserState("a123")).thenReturn("\\movecard");

        handler.handle("a123", "continue movecard from before");
        verify(moveCard, times(1)).handle("a123", "continue movecard from before");

    }

    @Test
    public void whenUserInSessionShouldGoThere() {

        when(userStateService.getUserState("a123")).thenReturn("session");

        handler.handle("a123", "answer to the last question");
        verify(sessionHandler).handle("a123", "answer to the last question");
    }

    @Test
    public void whenUserDoesNotHaveRootDirShouldCreate() {

        when(userStateService.getUserState("F123")).thenReturn("session");

        when(dataComponentRepository.findByUserIdAndDirectoryName("F123", "root"))
            .thenReturn(Optional.empty());

        handler.handle("F123", "\\quiz blablabla");
        ArgumentCaptor<Directory> argumentCapture = ArgumentCaptor.forClass(Directory.class);
        verify(dataComponentRepository).save(argumentCapture.capture());
        assertEquals("F123", argumentCapture.getValue().getUserId());
        assertEquals("root", argumentCapture.getValue().getDirectoryName());
    }

    @Test
    public void whenUserHasRootDirShouldNotCreate() {

        when(userStateService.getUserState("F123")).thenReturn("session");

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("F123", "root"))
            .thenReturn(Optional.of(mockDir));

        handler.handle("F123", "\\quiz blablabla");
        verify(dataComponentRepository, times(0)).save(any());
    }
}

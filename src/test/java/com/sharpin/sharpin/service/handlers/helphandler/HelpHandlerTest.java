package com.sharpin.sharpin.service.handlers.helphandler;

import static org.junit.Assert.assertEquals;

import com.sharpin.sharpin.service.handlers.Handler;
import org.junit.Test;

public class HelpHandlerTest {

    private Handler handler = new HelpHandler();

    @Test
    public void testGeneralHelp() {

        String expectedResponse = "Sharpin: A Chatbot based automatic flashcard system\n\n"
            + "The storage component consists of directory and cards. "
            + "Directory can be nested within another directory to create a tree-like hierarchy. "
            + "Each user by default will have a root directory called \"root\"\n\n"
            + "A card has a front, back, and notes:\n"
            + "Front: The part of the card that is asked in questions\n"
            + "Back: The part of the card that is considered the correct answer\n"
            + "Notes: additional info that is displayed after each answering attempt\n"
            + "Additionally, each card also has an SRS Level. "
            + "The higher the SRS level, the longer the next review will be. "
            + "Type \\help SRS for info on SRS.\n\n"
            + "List of commands:\n"
            + "To use a command, type and send it to the bot. "
            + "To get more info on each command, type \\help [COMMAND NAME], "
            + "for example, \\help adddirectory.\n"
            + "- \\exit: Interrupt an ongoing interaction\n"
            + "- \\addcard : add a card to your system\n"
            + "- \\adddirectory: add a directory to your system\n"
            + "- \\deletecard: delete a card from your system\n"
            + "- \\editcard: edit a card on your system\n"
            + "- \\movecard: move a card to a different directory\n"
            + "- \\quiz [directory name]: start a quiz with cards on the given directory\n"
            + "- \\review [number to review]: start a review with a "
            + "certain number of cards to review\n"
            + "- \\statistics [type] [directory name]: getting statistics on a directory\n"
            + "- \\list [type] [directory name]: listing cards/directory in a directory";

        String response = handler.handle("A123", "").get(0);
        System.out.println(response);
        assertEquals(response, expectedResponse);
    }

    @Test
    public void testNotFoundHelp() {

        String expectedResponse = "Invalid parameter given for \\help. "
            + "Please enter \\help for the list of commands.";

        String response = handler.handle("A123", "bad command").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testSrsHelp() {

        String expectedResponse = "The SRS level is ranged from 0 to 8. "
            + "Cards in higher levels is available for review after a longer period of time. "
            + "Details on the wait time for each SRS level:\n"
            + "- Level 0: 4 hours\n"
            + "- Level 1: 8 hours\n"
            + "- Level 2: 1 day\n"
            + "- Level 3: 2 days\n"
            + "- Level 4: 1 week\n"
            + "- Level 5: 2 weeks\n"
            + "- Level 6: 1 month\n"
            + "- Level 7: 4 month\n"
            + "- Level 8: Forever (will never be reviewed)\n\n"
            + "Credits to Wanikani (wanikani.com) for coming up with this SRS period scheme.";

        String response = handler.handle("A123", "SRS").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testAddCardHelp() {

        String expectedResponse = "This command adds a card to your system\n"
            + "Type \\addcard, the bot will start an interaction asking the following in order:\n"
            + "- Card front: Intended front of the newly inserted card\n"
            + "- Card back: Intended back of the newly inserted card\n"
            + "- Card notes: Intended notes of the newly inserted card\n"
            + "- Directory: Intended directory of the newly inserted card\n"
            + "Note that the card front must be unique for the given directory.";

        String response = handler.handle("A123", "\\addcard").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testAddDirectory() {
        String expectedResponse = "This command adds a directory to your system\n"
            + "Type \\adddirectory, the bot will start an interaction "
            + "asking the following in order:\n"
            + "- Directory name: Intended name of the newly added directory\n"
            + "- Parent directory: Intended place where the directory will be added\n"
            + "All your directory names must be unique, even if they don't are not ancestors.";

        String response = handler.handle("A123", "\\adddirectory").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testDeleteCard() {
        String expectedResponse = "This command deletes a card from your system\n"
            + "Type \\deletecard, the bot will start an interaction "
            + "asking the following in order:\n"
            + "- Directory name: The name of the directory where the card is located\n"
            + "- Card front: The card front of the card you want to delete";

        String response = handler.handle("A123", "\\deletecard").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testExitHelp() {
        String expectedResponse = "In the middle of an interaction "
            + "(e.g: deletecard, quiz, adddirectory), "
            + "you can enter this command to interrupt the interaction.";

        String response = handler.handle("A123", "\\exit").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);

    }

    @Test
    public void testMoveCardHelp() {
        String expectedResponse = "This command moves a card to a different directory\n"
            + "Type \\movecard, the bot will start an interaction "
            + "asking the following in order:\n"
            + "- Directory name: The name of the directory where the card is located\n"
            + "- Card front: The card front of the card you want to move\n"
            + "- New Directory: THe new directory location where you want this card to be\n"
            + "Note that it still applies that for each directory, "
            + "the card front must be unique.\n";

        String response = handler.handle("A123", "\\movecard").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testEditCardHelp() {
        String expectedResponse = "This command edits a card from your system\n"
            + "Type \\editcard, the bot will start an interaction "
            + "asking the following in order:\n"
            + "- Directory name: The name of the directory where the card is located\n"
            + "- Card front: The card front of the card you want to edit\n"
            + "- New front: The new front of the card you want to edit\n"
            + "- New back: The new back of the card you want to edit\n"
            + "- New notes: The new notes of the card you want to edit\n"
            + "To not change a value, you can use type \"keepsame\" instead.";

        String response = handler.handle("A123", "\\editcard").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testQuizHelp() {
        String expectedResponse = "This command starts a quiz using cards from [directory name]\n"
            + "Type \\quiz [directory name], the bot will start "
            + "asking you a maximum of 10 cards random from that directory.\n\n"
            + "Note that the bot can ask cards which is not a direct child of the directory. "
            + "For example, lets say you have a directory \"Biology\" with cards Dog, Cat, "
            + "and a nested directory called \"Microbiology\" containing card Mitocondria. "
            + "If you enter \\quiz Biology, the bot will ask you Dog, Cat, and Mitocondria.";

        String response = handler.handle("A123", "\\quiz").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testReviewHelp() {
        String expectedResponse = "This command starts a review of a maximum of [number of cards]\n"
            + "Type \\review [number of cards], the bot will start "
            + "Asking cards that can be reviewed with a maximum of [number of cards] card.\n\n"
            + "Note that this will ask for cards from your whole system. You cannot choose which "
            + "directory to review from.";

        String response = handler.handle("A123", "\\review").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testStatisticsHelp() {
        String expectedResponse =
                "This command will show statistics from the directory you chose.\n"
                + "There are 2 options:\n"
                + "Type \\statistics statlevel [directory name]"
                + " to see the number of cards in each level from the directory you typed\n"
                + "type \\statistics statmiss [directory name]"
                + " to see 5 most wrong answered card in that directory";

        String response = handler.handle("A123", "\\statistics").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testListHelp() {
        String expectedResponse = "This command will show lists from the directory you chose.\n"
                + "There are 2 options:\n"
                + "Type \\list dir [directory name]"
                + " to see the list of subdirectories under the directory you typed\n"
                + "Type \\list card [directory name]"
                + "to see the list of cards in that directory";

        String response = handler.handle("A123", "\\list").get(0);
        System.out.println(response);
        assertEquals(expectedResponse, response);
    }
}

package com.sharpin.sharpin.service.handlers.movecard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class OldDirectoryHandlerTest {


    @Mock
    MoveCardService moveCardService;

    @InjectMocks
    Handler handler = new OldDirectoryHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("Old Directory Handler", handler.getName());
    }

    @Test
    public void whenNullMessageShouldReturnCantBeNull() {
        assertEquals("Directory name can't be empty!", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenNullMessageShouldNotChangeState() {
        handler.handle("A123", null);
        verify(moveCardService, times(0)).setState(eq("A123"), anyString());
    }

    @Test
    public void whenNullMessageShouldNotSaveName() {
        handler.handle("A123", null);
        verify(moveCardService, times(0)).setOldDirectory(eq("A123"), anyString());
    }


    @Test
    public void whenGivenMessageOnValidShouldAskCardFront() {
        when(moveCardService.setOldDirectory("A123", "random directory name"))
            .thenReturn(true);
        assertEquals("Card front?",
            handler.handle("A123", "random directory name").get(0));
    }

    @Test
    public void whenGivenMessageShouldChangeStateToCardFront() {
        when(moveCardService.setOldDirectory("A123", "random directory name"))
            .thenReturn(true);
        handler.handle("A123", "random directory name");
        verify(moveCardService).setState("A123", "Card Front Handler");
    }

    @Test
    public void whenGivenMessageOnInvalidShouldGiveError() {
        when(moveCardService.setOldDirectory("B123", "random directory name"))
            .thenReturn(false);
        String expectedResponse = "ERROR: Directory random directory name does not exist.";
        List<String> response = handler.handle("B123", "random directory name");
        assertEquals(expectedResponse, response.get(0));
        assertEquals("Directory?", response.get(1));
    }

    @Test
    public void whenGivenMessageOnInvalidShouldNotSetState() {
        when(moveCardService.setOldDirectory("B123", "random directory name"))
            .thenReturn(false);
        handler.handle("B123", "random directory name");
        verify(moveCardService, times(0)).setState(eq("B123"), anyString());
    }


}

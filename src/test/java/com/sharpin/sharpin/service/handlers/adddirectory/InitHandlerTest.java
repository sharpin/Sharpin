package com.sharpin.sharpin.service.handlers.adddirectory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class InitHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    AddDirectoryService addDirectoryService;

    @InjectMocks
    Handler handler = new InitHandler();

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameShouldReturnCorrectName() {
        assertEquals("Initial Handler", handler.getName());
    }

    @Test
    public void handlerShouldSetUserStateToAddDirectory() {
        handler.handle("1234", null);
        verify(userStateService, times(1)).setUserState("1234", "\\adddirectory");
    }

    @Test
    public void handlerShouldSetAddDirectoryStateToDirectoryName() {
        handler.handle("1234", null);
        verify(addDirectoryService, times(1)).setState("1234", "Directory Name Handler");
    }

    @Test
    public void whenHandleShouldAskForDirectoryName() {
        assertEquals("Directory name?", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenHandleIsCalledItShouldMoveAddDirectoryState() {
        handler.handle("1234", "message");
        verify(addDirectoryService, times(1)).createUserData("1234");
    }

    @Test
    public void whenHandleIsCalledItShouldCreateUserData() {
        handler.handle("1234", "message");
        verify(userStateService, times(1)).createUserData("1234");
    }

}

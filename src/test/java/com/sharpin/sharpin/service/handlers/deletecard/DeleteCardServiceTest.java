package com.sharpin.sharpin.service.handlers.deletecard;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.DeleteCardState;
import com.sharpin.sharpin.repository.temporarydata.DeleteCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

public class DeleteCardServiceTest {

    @Mock
    DataComponentRepository dataComponentRepository;

    @Mock
    DeleteCardStateRepository deleteCardStateRepository;

    @InjectMocks
    private DeleteCardService deleteCardService = new DeleteCardServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCreateUserShouldSaveNewInstance() {

        deleteCardService.createUserData("A123");
        ArgumentCaptor<DeleteCardState> captor = ArgumentCaptor.forClass(DeleteCardState.class);
        verify(deleteCardStateRepository).save(captor.capture());
        assertEquals("A123", captor.getValue().getUserId());
    }

    @Test
    public void whenDeleteUserShouldCallDeleteUser() {

        deleteCardService.deleteUserData("A123");
        verify(deleteCardStateRepository).deleteById("A123");
    }

    @Test
    public void whenDeleteDoesNotExistShouldDoNothing() {

        doThrow(new EmptyResultDataAccessException(1))
            .when(deleteCardStateRepository).deleteById("B123");
        deleteCardService.deleteUserData("B123");
    }

    @Test
    public void whenSetStateShouldSetState() {

        DeleteCardState deleteCardState = new DeleteCardState();
        deleteCardState.setUserId("C123");
        when(deleteCardStateRepository.findById("C123"))
            .thenReturn(Optional.of(deleteCardState));
        deleteCardService.setState("C123", "Card Name Handler");

        ArgumentCaptor<DeleteCardState> captor = ArgumentCaptor.forClass(DeleteCardState.class);
        verify(deleteCardStateRepository).save(captor.capture());
        assertEquals("Card Name Handler", captor.getValue().getState());
    }

    @Test
    public void whenGetStateShouldGiveState() {

        DeleteCardState state = new DeleteCardState();
        state.setUserId("C123");
        state.setState("Directory Name Handler");
        when(deleteCardStateRepository.findById("C123"))
            .thenReturn(Optional.of(state));

        assertEquals("Directory Name Handler", deleteCardService.getState("C123"));
    }

    @Test
    public void whenSetCardFrontOnNonExistentCardShouldNotSetIt() {


        DeleteCardState state = new DeleteCardState();
        state.setUserId("C123");
        state.setDirectory("kages");
        when(deleteCardStateRepository.findById("C123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("C123", "kages"))
            .thenReturn(Optional.ofNullable(mockDir));
        when(mockDir.isCardFrontExistAsDirectChild("mizukage"))
            .thenReturn(false);

        deleteCardService.setCardFront("C123", "mizukage");
        verify(deleteCardStateRepository, times(0)).save(any());

    }

    @Test
    public void whenSetCardFrontOnNonExistentCardShouldReturnFalse() {


        DeleteCardState state = new DeleteCardState();
        state.setUserId("C123");
        state.setDirectory("kages");
        when(deleteCardStateRepository.findById("C123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("C123", "kages"))
            .thenReturn(Optional.ofNullable(mockDir));
        when(mockDir.isCardFrontExistAsDirectChild("mizukage"))
            .thenReturn(false);

        boolean response = deleteCardService.setCardFront("C123", "mizukage");
        assertFalse(response);

    }


    @Test
    public void whenSetCardFrontOnExistCardShouldDeleteCardFromDirectory() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("D123");
        state.setDirectory("kages");
        when(deleteCardStateRepository.findById("D123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "kages"))
            .thenReturn(Optional.ofNullable(mockDir));
        when(mockDir.isCardFrontExistAsDirectChild("mizukage"))
            .thenReturn(true);

        deleteCardService.setCardFront("D123", "mizukage");

        ArgumentCaptor<DeleteCardState> captor = ArgumentCaptor.forClass(DeleteCardState.class);
        verify(deleteCardStateRepository).save(captor.capture());
        assertEquals("mizukage", captor.getValue().getCardFront());
    }

    @Test
    public void whenSetCardFrontOnExistCardShouldReturnTrue() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("D123");
        state.setDirectory("kages");
        when(deleteCardStateRepository.findById("D123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "kages"))
            .thenReturn(Optional.ofNullable(mockDir));
        when(mockDir.isCardFrontExistAsDirectChild("mizukage"))
            .thenReturn(true);

        boolean response = deleteCardService.setCardFront("D123", "mizukage");
        assertTrue(response);
    }

    @Test
    public void whenSetDirectoryOnNonExistentDirectoryShouldReturnFalse() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("D123");
        when(deleteCardStateRepository.findById("D123"))
            .thenReturn(Optional.of(state));

        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "hiya"))
            .thenReturn(Optional.empty());

        boolean response = deleteCardService.setDirectoryName("D123", "hiya");
        assertFalse(response);
    }

    @Test
    public void whenSetDirectoryOnNonExistentDirectoryShouldNotSetName() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("D123");
        when(deleteCardStateRepository.findById("D123"))
            .thenReturn(Optional.of(state));

        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "hiya"))
            .thenReturn(Optional.empty());

        deleteCardService.setDirectoryName("D123", "hiya");
        verify(deleteCardStateRepository, times(0))
            .save(any());
    }

    @Test
    public void whenSetDirectoryOnExistentDirectoryShouldSetIt() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("D123");
        when(deleteCardStateRepository.findById("D123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);

        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "hiya"))
            .thenReturn(Optional.ofNullable(mockDir));

        deleteCardService.setDirectoryName("D123", "hiya");

        ArgumentCaptor<DeleteCardState> captor = ArgumentCaptor.forClass(DeleteCardState.class);
        verify(deleteCardStateRepository).save(captor.capture());
        assertEquals("hiya", captor.getValue().getDirectory());

    }

    @Test
    public void whenSetDirectoryOnExistentDirectoryShouldReturnTrue() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("D123");
        when(deleteCardStateRepository.findById("D123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);

        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "hiya"))
            .thenReturn(Optional.ofNullable(mockDir));

        boolean response = deleteCardService.setDirectoryName("D123", "hiya");
        assertTrue(response);
    }

    @Test
    public void whenHandleChangesShouldReturnCorrectString() {
        DeleteCardState state = new DeleteCardState();
        state.setUserId("E123");
        state.setDirectory("kages");
        state.setCardFront("mizukage");
        when(deleteCardStateRepository.findById("E123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);

        when(dataComponentRepository.findByUserIdAndDirectoryName("E123", "kages"))
            .thenReturn(Optional.of(mockDir));

        String response = deleteCardService.handleChanges("E123");
        assertEquals("Card with front \"mizukage\" has been deleted from directory kages",
            response);
    }

    @Test
    public void whenHandleChangesShouldDeleteFromDirectory() {

        DeleteCardState state = new DeleteCardState();
        state.setUserId("F123");
        state.setDirectory("kages");
        state.setCardFront("mizukage");
        when(deleteCardStateRepository.findById("F123"))
            .thenReturn(Optional.of(state));

        Directory mockDir = mock(Directory.class);

        when(dataComponentRepository.findByUserIdAndDirectoryName("F123", "kages"))
            .thenReturn(Optional.of(mockDir));

        deleteCardService.handleChanges("F123");
        verify(mockDir).removeChildByCardFront("mizukage");
        verify(dataComponentRepository).save(mockDir);
    }



}

package com.sharpin.sharpin.service.handlers.addcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DirectoryAddCardHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    AddCardService addCardService;

    @InjectMocks
    Handler handler = new DirectoryAddCardHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("Directory Handler", handler.getName());
    }

    @Test
    public void whenNullMessageShouldReturnCantBeNull() {
        assertEquals("Directory name can't be empty!", handler.handle("A1234", null).get(0));
    }

    @Test
    public void whenNullMessageShouldNotChangeState() {
        handler.handle("A1234", null);
        verify(addCardService, times(0)).setState(eq("A1234"), anyString());
    }

    @Test
    public void whenNullMessageShouldNotSaveName() {
        handler.handle("A1234", null);
        verify(addCardService, times(0)).setDirectory(eq("A1234"), anyString());
    }

    @Test
    public void whenGivenMessageShouldSaveMessage() {
        handler.handle("A1234", "random directory name");
        verify(addCardService, times(1)).setDirectory("A1234", "random directory name");
    }

    @Test
    public void whenGivenMessageShouldCallHandleChanges() {
        when(addCardService.setDirectory("A1234","directory")).thenReturn(true);
        handler.handle("A1234", "directory");
        verify(addCardService, times(1)).handleChanges("A1234");
    }

    @Test
    public void whenGivenMessageShouldEraseUserData() {
        when(addCardService.setDirectory("A1234","directory")).thenReturn(true);
        handler.handle("A1234", "directory");
        verify(addCardService, times(1)).deleteUserData("A1234");
        verify(userStateService, times(1)).deleteUserData("A1234");
    }

    @Test
    public void whenGivenMessageNonexistDirectoryShouldReturnCorrectResponseOnu() {
        when(addCardService.handleChanges("A1234"))
                .thenReturn("ERROR: Directory doesn't exist\n"
                        + "Directory ?");

        assertEquals("ERROR: Directory doesn't exist\n"
                        + "Directory ?",
                handler.handle("A1234", "random card name").get(0));
    }

    @Test
    public void whenGivenMessageExistDirectoryShouldReturnCorrectResponse() {

        when(addCardService.setDirectory("A1234","directory")).thenReturn(true);
        when(addCardService.handleChanges("A1234"))
                .thenReturn("Card randomCard added to "
                        + "directory!");

        assertEquals("Card randomCard added to directory!",
                handler.handle("A1234", "directory").get(0));
    }
}

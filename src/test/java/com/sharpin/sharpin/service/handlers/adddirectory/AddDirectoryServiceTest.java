package com.sharpin.sharpin.service.handlers.adddirectory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.AddDirectoryState;
import com.sharpin.sharpin.repository.temporarydata.AddDirectoryStateRepository;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

public class AddDirectoryServiceTest {

    @Mock
    AddDirectoryStateRepository addDirectoryStateRepository;

    @Mock
    DataComponentRepository dataComponentRepository;

    @InjectMocks
    AddDirectoryService addDirectoryService;

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {
        addDirectoryService = new AddDirectoryServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateUserCallsDateSave() {
        ArgumentCaptor<AddDirectoryState> argumentCaptor = ArgumentCaptor.forClass(
                AddDirectoryState.class);
        addDirectoryService.createUserData("1234");
        verify(addDirectoryStateRepository).save(argumentCaptor.capture());
        assertEquals("1234", argumentCaptor.getValue().getUserId());
    }

    @Test
    public void whenGetUserStateShouldReturnCorrectOne() {
        AddDirectoryState subject = new AddDirectoryState();
        subject.setState("current state");
        Optional<AddDirectoryState> returned = Optional.of(subject);
        when(addDirectoryStateRepository.findById("1234")).thenReturn(returned);

        assertEquals("current state", addDirectoryService.getState("1234"));
    }

    @Test
    public void whenGetUserStateDoesNotExistShouldThrowException() {
        assertThrows(NoSuchElementException.class, () -> addDirectoryService.getState("1234"));
    }

    @Test
    public void getDirectoryNameShouldReturnCorrectName() {
        AddDirectoryState sample = new AddDirectoryState();
        sample.setDirectoryName("directory");
        Optional<AddDirectoryState> returned = Optional.of(sample);
        when(addDirectoryStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("directory", addDirectoryService.getDirectoryName("1234"));
    }

    @Test
    public void getParentDirectoryShouldReturnCorrectDirectory() {
        AddDirectoryState sample = new AddDirectoryState();
        sample.setParentDirectory("parent");
        Optional<AddDirectoryState> returned = Optional.of(sample);
        when(addDirectoryStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("parent", addDirectoryService.getParentDirectory("1234"));
    }

    @Test
    public void whenSetStateShouldSetIt() {

        AddDirectoryState mockState = new AddDirectoryState();
        mockState.setState("old state");
        Optional<AddDirectoryState> returned = Optional.of(mockState);
        when(addDirectoryStateRepository.findById(("1234"))).thenReturn(returned);

        ArgumentCaptor<AddDirectoryState> argumentCaptor = ArgumentCaptor.forClass(
                AddDirectoryState.class);
        addDirectoryService.setState("1234", "new state");
        verify(addDirectoryStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("new state", argumentCaptor.getValue().getState());
    }

    @Test
    public void whenSetDirectoryNameShouldSetIt() {

        AddDirectoryState mockState = new AddDirectoryState();
        mockState.setDirectoryName("directory 0");
        Optional<AddDirectoryState> returned = Optional.of(mockState);
        when(addDirectoryStateRepository.findById(("1234"))).thenReturn(returned);

        ArgumentCaptor<AddDirectoryState> argumentCaptor = ArgumentCaptor.forClass(
                AddDirectoryState.class);
        addDirectoryService.setDirectoryName("1234", "directory 1");
        verify(addDirectoryStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("directory 1", argumentCaptor.getValue().getDirectoryName());
    }

    @Test
    public void whenSetDirectoryOnExistShouldNotSetIt() {

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("1234", "directory 0"))
            .thenReturn(Optional.of(mockDir));

        assertFalse(addDirectoryService.setDirectoryName("1234", "directory 0"));
        verify(addDirectoryStateRepository, times(0)).save(any());
    }

    @Test
    public void whenSetParentDirectoryShouldSetIt() {

        AddDirectoryState mockState = new AddDirectoryState();
        mockState.setParentDirectory("parent 0");
        Optional<AddDirectoryState> returned = Optional.of(mockState);
        when(addDirectoryStateRepository.findById(("1234"))).thenReturn(returned);
        Directory dir = new Directory("parent 1", "1234");
        Optional<Directory> odir = Optional.of(dir);
        when(dataComponentRepository.findByUserIdAndDirectoryName("1234", "parent 1"))
                .thenReturn(odir);

        ArgumentCaptor<AddDirectoryState> argumentCaptor = ArgumentCaptor.forClass(
                AddDirectoryState.class);
        addDirectoryService.setParentDirectory("1234", "parent 1");
        verify(addDirectoryStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("parent 1", argumentCaptor.getValue().getParentDirectory());

    }

    @Test
    public void whenSetParentDirectoryOnNotExistShouldNotSetIt() {

        when(dataComponentRepository.findByUserIdAndDirectoryName("1234", "parent 0"))
            .thenReturn(Optional.empty());

        assertFalse(addDirectoryService.setParentDirectory("1234", "parent 0"));
        verify(addDirectoryStateRepository, times(0)).save(any());
    }

    @Test
    public void whenDeleteDataShouldCallDelete() {

        addDirectoryService.deleteUserData("1234");
        verify(addDirectoryStateRepository, times(1)).deleteById("1234");
    }

    @Test
    public void whenDeleteDataOnNoDataShouldDoNothing() {
        doThrow(new EmptyResultDataAccessException(1))
            .when(addDirectoryStateRepository).deleteById("A123");
        addDirectoryService.deleteUserData("A123");
    }

    @Test
    public void whenHandleChangesShouldReturnMessage() {

        AddDirectoryState mockState = new AddDirectoryState();
        mockState.setDirectoryName("directory name");
        mockState.setParentDirectory("root");
        Optional<AddDirectoryState> returned = Optional.of(mockState);
        when(addDirectoryStateRepository.findById(("1234"))).thenReturn(returned);

        assertEquals("Directory added!",
                addDirectoryService.handleChanges("1234"));

    }

    @Test
    public void whenHandleChangesShouldSaveToParentDir() {

        AddDirectoryState mockState = new AddDirectoryState();
        mockState.setDirectoryName("directory name");
        mockState.setParentDirectory("father");
        mockState.setUserId("1234");
        Optional<AddDirectoryState> returned = Optional.of(mockState);
        when(addDirectoryStateRepository.findById(("1234"))).thenReturn(returned);

        Directory father = mock(Directory.class);
        when(father.getDirectoryName()).thenReturn("father");
        when(dataComponentRepository.findByUserIdAndDirectoryName("1234", "father"))
            .thenReturn(Optional.ofNullable(father));

        // did the program add the directory as child?
        ArgumentCaptor<DataComponent> capture = ArgumentCaptor.forClass(DataComponent.class);
        addDirectoryService.handleChanges("1234");
        verify(father).addChild(capture.capture());
        assertEquals("directory name", ((Directory) capture.getValue()).getDirectoryName());

        verify(dataComponentRepository).save(capture.capture());
        assertEquals("father", ((Directory) capture.getValue()).getDirectoryName());

    }
}

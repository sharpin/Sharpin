package com.sharpin.sharpin.service.handlers.deletecard;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DeleteCardHandlerTest {

    @Mock
    DeleteCardService deleteCardService;

    @Mock
    UserStateService userStateService;

    @Mock
    DeleteCardDirectoryNameHandler directoryNameHandler;

    @Mock
    DeleteCardCardNameHandler cardNameHandler;


    @InjectMocks
    private Handler handler = new DeleteCardHandler();

    /**
     * Setting up for test cases.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(directoryNameHandler.getName()).thenReturn("Directory Name Handler");
        when(cardNameHandler.getName()).thenReturn("Card Name Handler");
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("Delete Card Handler", handler.getName());
    }

    @Test
    public void whenStateIsDirectoryNameHandlerShouldGoToThat() {
        when(deleteCardService.getState("A123")).thenReturn("Directory Name Handler");
        handler.handle("A123", "Biology");
        verify(directoryNameHandler).handle("A123", "Biology");
    }

    @Test
    public void whenStateIsCardNameHandlerShouldGoToThat() {
        when(deleteCardService.getState("B123")).thenReturn("Card Name Handler");
        handler.handle("B123", "Mitocondria");
        verify(cardNameHandler).handle("B123", "Mitocondria");
    }

    @Test
    public void whenStateIsUnknownShouldDeleteAllUserData() {
        when(deleteCardService.getState("C123")).thenReturn("funny handler");
        handler.handle("C123", "Lisosom");
        verify(userStateService).deleteUserData("C123");
        verify(deleteCardService).deleteUserData("C123");
    }

    @Test
    public void whenInvalidStateReachedShouldReturnTheMessage() {
        when(deleteCardService.getState("A123")).thenReturn("funny handler");
        List<String> returned = handler.handle("A123", "Lisosom");
        assertEquals(1, returned.size());
        assertEquals("Invalid state reached, please try again!", returned.get(0));

    }

    @Test
    public void whenUserGivesExitShouldEndInteraction() {

        handler.handle("A1234", "\\exit");
        verify(deleteCardService).deleteUserData("A1234");
        verify(userStateService).deleteUserData("A1234");
    }

    @Test
    public void whenUserGivesExitShouldGiveMessage() {
        List<String> response = handler.handle("A1234", "\\exit");
        assertEquals("Process canceled", response.get(0));
    }
}

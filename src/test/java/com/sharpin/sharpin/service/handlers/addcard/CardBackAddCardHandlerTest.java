package com.sharpin.sharpin.service.handlers.addcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.sharpin.sharpin.service.handlers.Handler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CardBackAddCardHandlerTest {
    @Mock
    AddCardService addCardService;

    @InjectMocks
    Handler handler = new CardBackAddCardHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        Assertions.assertEquals("Card Back Handler", handler.getName());
    }

    @Test
    public void whenNullMessageShouldReturnCantBeNull() {
        assertEquals("Card back can't be empty!", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenNullMessageShouldNotChangeState() {
        handler.handle("A123", null);
        verify(addCardService, times(0)).setState(eq("A123"), anyString());
    }

    @Test
    public void whenNullMessageShouldNotSaveName() {
        handler.handle("A123", null);
        verify(addCardService, times(0)).setCardBack(eq("A123"), anyString());
    }

    @Test
    public void whenGivenCardBackShouldSaveIt() {
        handler.handle("A123", "random card back");
        verify(addCardService, times(1)).setCardBack("A123", "random card back");
    }

    @Test
    public void whenGivenCardBackShouldChangeState() {
        handler.handle("A123", "random card back");
        verify(addCardService, times(1)).setState("A123", "Card Notes Handler");
    }
}

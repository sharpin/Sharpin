package com.sharpin.sharpin.service.handlers.addcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.sharpin.sharpin.service.handlers.Handler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CardFrontAddCardHandlerTest {

    @Mock
    AddCardService addCardService;

    @InjectMocks
    Handler handler = new CardFrontAddCardHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        Assertions.assertEquals("Card Front Handler", handler.getName());
    }

    @Test
    public void whenNullMessageShouldReturnCantBeNull() {
        assertEquals("Card front can't be empty!", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenNullMessageShouldNotChangeState() {
        handler.handle("A123", null);
        verify(addCardService, times(0)).setState(eq("A123"), anyString());
    }

    @Test
    public void whenNullMessageShouldNotSaveName() {
        handler.handle("A123", null);
        verify(addCardService, times(0)).setCardFront(eq("A123"), anyString());
    }

    @Test
    public void whenGivenCardFrontShouldSaveIt() {
        handler.handle("A123", "random card front");
        verify(addCardService, times(1)).setCardFront("A123", "random card front");
    }

    @Test
    public void whenGivenCardFrontShouldChangeState() {
        handler.handle("A123", "random card front");
        verify(addCardService, times(1)).setState("A123", "Card Back Handler");
    }
}

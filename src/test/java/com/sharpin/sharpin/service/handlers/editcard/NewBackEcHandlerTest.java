package com.sharpin.sharpin.service.handlers.editcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.sharpin.sharpin.service.handlers.Handler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class NewBackEcHandlerTest {

    @Mock
    EditCardService editCardService;

    @InjectMocks
    Handler handler = new NewBackEcHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameShouldReturnCorrectName() {
        assertEquals("New Back Handler", handler.getName());
    }

    @Test
    public void whenNullMessageReceivedShouldReturnCantBeNull() {
        assertEquals("New card back name can't be empty!", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenNullMessageReceivedShouldNotChangeState() {
        handler.handle("1234", null);
        verify(editCardService, times(0)).setState(eq("1234"), anyString());
    }

    @Test
    public void whenNullMessageReceivedShouldNotSaveName() {
        handler.handle("1234", null);
        verify(editCardService, times(0)).setNewBack(eq("1234"), anyString());
    }

    @Test
    public void whenGivenMessageShouldSaveMessage() {
        handler.handle("1234", "new back");
        verify(editCardService, times(1)).setNewBack("1234", "new back");
    }

    @Test
    public void whenGivenMessageShouldChangeStateToNewNote() {
        handler.handle("1234", "new back");
        verify(editCardService, times(1)).setState("1234", "New Note Handler");
    }

}

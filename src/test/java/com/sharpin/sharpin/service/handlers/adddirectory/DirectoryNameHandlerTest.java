package com.sharpin.sharpin.service.handlers.adddirectory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DirectoryNameHandlerTest {

    @Mock
    AddDirectoryService addDirectoryService;

    @InjectMocks
    Handler handler = new DirectoryNameHandler();

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameShouldReturnCorrectName() {
        assertEquals("Directory Name Handler", handler.getName());
    }

    @Test
    public void whenNullMessageReceivedShouldReturnCantBeNull() {
        assertEquals("Directory name can't be empty!", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenNullMessageReceivedShouldNotChangeState() {
        handler.handle("1234", null);
        verify(addDirectoryService, times(0)).setState(eq("1234"), anyString());
    }

    @Test
    public void whenNullMessageReceivedShouldNotSaveName() {
        handler.handle("1234", null);
        verify(addDirectoryService, times(0)).setDirectoryName(eq("1234"), anyString());
    }

    @Test
    public void whenGivenMessageShouldSaveMessage() {
        handler.handle("1234", "directory name");
        verify(addDirectoryService, times(1)).setDirectoryName("1234", "directory name");
    }

    @Test
    public void whenGivenMessageShouldChangeStateParentDirectory() {
        when(addDirectoryService.setDirectoryName("1234","some random name")).thenReturn(true);
        handler.handle("1234", "some random name");
        verify(addDirectoryService, times(1)).setState("1234", "Parent Directory Handler");
    }

}

package com.sharpin.sharpin.service.handlers.movecard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class NewDirectoryHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    MoveCardService moveCardService;

    @InjectMocks
    Handler handler = new NewDirectoryHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("New Directory Handler", handler.getName());
    }

    @Test
    public void whenNullMessageShouldReturnCantBeNull() {
        assertEquals("Directory name can't be empty!", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenNullMessageShouldNotChangeState() {
        handler.handle("A123", null);
        verify(moveCardService, times(0)).setState(eq("A123"), anyString());
    }

    @Test
    public void whenNullMessageShouldNotSaveName() {
        handler.handle("A123", null);
        verify(moveCardService, times(0)).setNewDirectory(eq("A123"), anyString());
    }

    @Test
    public void whenGivenMessageShouldSaveMessage() {
        handler.handle("A123", "random directory name");
        verify(moveCardService, times(1)).setNewDirectory("A123", "random directory name");
    }

    @Test
    public void whenGivenMessageShouldCallHandleChanges() {
        when(moveCardService.setNewDirectory("A123", "random directory name"))
            .thenReturn(true);
        handler.handle("A123", "random directory name");
        verify(moveCardService, times(1)).handleChanges("A123");
    }

    @Test
    public void whenGivenMessageShouldEraseUserData() {
        when(moveCardService.setNewDirectory("A123", "random directory name"))
            .thenReturn(true);
        handler.handle("A123", "random directory name");
        verify(moveCardService, times(1)).deleteUserData("A123");
        verify(moveCardService, times(1)).deleteUserData("A123");
    }

    @Test
    public void whenGivenMessageShouldReturnCorrectResponse() {
        when(moveCardService.setNewDirectory("A123", "random card name"))
            .thenReturn(true);
        when(moveCardService.handleChanges("A123"))
                .thenReturn("Card randomCard has been moved "
                        + "from directory directory1 to directory2!");

        assertEquals("Card randomCard has been moved from directory directory1 to directory2!",
                handler.handle("A123", "random card name").get(0));
    }

}

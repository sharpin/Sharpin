package com.sharpin.sharpin.service.handlers.deletecard;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DeleteCardInitialHandlerTest {

    @Mock
    DeleteCardService deleteCardService;

    @Mock
    UserStateService userStateService;

    @InjectMocks
    private Handler handler = new DeleteCardInitialHandler();

    /**
     * Setting up for test cases.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\deletecard", handler.getName());
    }

    @Test
    public void whenOnCallShouldCreateUserState() {

        handler.handle("C123", null);
        verify(userStateService).createUserData("C123");
    }

    @Test
    public void whenOnCallShouldCreateDeleteCardState() {

        handler.handle("D123", null);
        verify(deleteCardService).createUserData("D123");
    }

    @Test
    public void whenOnCallShouldSetUserStateToDeleteCard() {
        handler.handle("C123", null);
        verify(userStateService).setUserState("C123", "Delete Card Handler");
    }

    @Test
    public void whenOnCallShouldSetStateDeleteCardStateToDirectoryState() {
        handler.handle("C123", null);
        verify(deleteCardService).setState("C123", "Directory Name Handler");
    }

    @Test
    public void whenOnCallShouldAskForDirectoryName() {

        List<String> returned = handler.handle("C123", null);
        assertEquals(1, returned.size());
        assertEquals("Directory name?", returned.get(0));
    }
}

package com.sharpin.sharpin.service.handlers.editcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class EditCardHandlerTest {

    @Mock
    EditCardService editCardService;

    @Mock
    UserStateService userStateService;

    @Mock
    EditCardInitHandler editCardInitHandler;

    @Mock
    DirectoryNameEcHandler directoryNameEcHandler;

    @Mock
    CardFrontEcHandler cardFrontEcHandler;

    @Mock
    NewFrontEcHandler newFrontEcHandler;

    @Mock
    NewBackEcHandler newBackEcHandler;

    @Mock
    NewNoteEcHandler newNoteEcHandler;

    @InjectMocks
    Handler handler = new EditCardHandler();

    private List<String> stringToList(String response) {
        List<String> messages = new ArrayList<>();
        messages.add(response);
        return messages;
    }

    /**
     Function to set-up the test.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(editCardInitHandler.getName()).thenReturn("Initial Handler");
        when(directoryNameEcHandler.getName()).thenReturn("Directory Name Handler");
        when(cardFrontEcHandler.getName()).thenReturn("Card Front Handler");
        when(newFrontEcHandler.getName()).thenReturn("New Front Handler");
        when(newBackEcHandler.getName()).thenReturn("New Back Handler");
        when(newNoteEcHandler.getName()).thenReturn("New Note Handler");
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\editcard", handler.getName());
    }

    @Test
    public void whenUserStateDoesNotExistShouldNotCreateIt() {
        when(editCardService.getState("1234")).thenThrow(NoSuchElementException.class);

        handler.handle("1234", "message");
        verify(editCardService, times(0)).createUserData("1234");
    }

    @Test
    public void whenNoStateHandlerShouldAskForDirectoryName() {
        when(editCardInitHandler.handle("1234", null)).thenReturn(stringToList("Directory name?"));
        when(editCardService.getState("1234")).thenThrow(NoSuchElementException.class);
        assertEquals("Directory name?", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenInStateDirectoryNameShouldGoThere() {
        when(editCardService.getState("1234")).thenReturn("Directory Name Handler");
        handler.handle("1234", "message");
        verify(directoryNameEcHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInStateCardFrontShouldGoThere() {
        when(editCardService.getState("1234")).thenReturn("Card Front Handler");
        handler.handle("1234", "message");
        verify(cardFrontEcHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInStateNewFrontShouldGoThere() {
        when(editCardService.getState("1234")).thenReturn("New Front Handler");
        handler.handle("1234", "message");
        verify(newFrontEcHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInStateNewBackShouldGoThere() {
        when(editCardService.getState("1234")).thenReturn("New Back Handler");
        handler.handle("1234", "message");
        verify(newBackEcHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInStateNewNoteShouldGoThere() {
        when(editCardService.getState("1234")).thenReturn("New Note Handler");
        handler.handle("1234", "message");
        verify(newNoteEcHandler, times(1)).handle("1234", "message");
    }

    @Test
    public void whenInvalidStageReachedShouldSaySo() {
        when(editCardService.getState("1234")).thenReturn("invalid state");
        assertEquals("Invalid stage reached!", handler.handle("1234", "message").get(0));
    }

    @Test
    public void whenInvalidStageReachedShouldClearUserState() {
        when(editCardService.getState("1234")).thenReturn("invalid state");
        handler.handle("1234", "message");
        verify(editCardService, times(1)).deleteUserData("1234");
        verify(userStateService, times(1)).deleteUserData("1234");
    }

    @Test
    public void whenUserGivesExitShouldEndInteraction() {

        handler.handle("A1234", "\\exit");
        verify(editCardService).deleteUserData("A1234");
        verify(userStateService).deleteUserData("A1234");
    }

    @Test
    public void whenUserGivesExitShouldGiveMessage() {
        List<String> response = handler.handle("A1234", "\\exit");
        assertEquals("Process canceled", response.get(0));
    }

}

package com.sharpin.sharpin.service.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class VersionHandlerTest {

    Handler handler = new VersionHandler();

    @Test
    public void testGetNameShouldReturnCorrectName() {
        assertEquals("\\version", handler.getName());
    }

    @Test
    public void testHandleShouldReturnCurrentVersionOnNull() {
        assertEquals("Version 0.0.1", handler.handle("a123", null).get(0));
    }

    @Test
    public void testHandlerShouldReturnCurrentVersionOnNotNull() {
        assertEquals("Version 0.0.1", handler.handle("a123", "test parameter").get(0));
    }
}

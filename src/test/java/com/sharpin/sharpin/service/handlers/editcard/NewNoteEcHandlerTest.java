package com.sharpin.sharpin.service.handlers.editcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class NewNoteEcHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    EditCardService editCardService;

    @InjectMocks
    Handler handler = new NewNoteEcHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNameShouldReturnCorrectName() {
        assertEquals("New Note Handler", handler.getName());
    }

    @Test
    public void whenNullMessageReceivedShouldReturnCantBeNull() {
        assertEquals("New card note can't be empty!", handler.handle("1234", null).get(0));
    }

    @Test
    public void whenNullMessageReceivedShouldNotChangeState() {
        handler.handle("1234", null);
        verify(editCardService, times(0)).setState(eq("1234"), anyString());
    }

    @Test
    public void whenNullMessageReceivedShouldNotSaveName() {
        handler.handle("1234", null);
        verify(editCardService, times(0)).setNewNote(eq("1234"), anyString());
    }

    @Test
    public void whenGivenMessageShouldSaveMessage() {
        handler.handle("1234", "directory name");
        verify(editCardService, times(1)).setNewNote("1234", "directory name");
    }

    @Test
    public void whenGivenMessageShouldCallHandleChanges() {
        handler.handle("1234", "directory name");
        verify(editCardService, times(1)).handleChanges("1234");
    }

    @Test
    public void whenGivenMessageShouldEraseUserData() {
        handler.handle("1234", "directory name");
        verify(editCardService, times(1)).deleteUserData("1234");
        verify(userStateService, times(1)).deleteUserData("1234");
    }

    @Test
    public void whenGivenMessageShouldReturnCorrectResponse() {
        when(editCardService.handleChanges("1234")).thenReturn("Card edited!");

        assertEquals("Card edited!",
                handler.handle("1234", "new note").get(0));
    }

}

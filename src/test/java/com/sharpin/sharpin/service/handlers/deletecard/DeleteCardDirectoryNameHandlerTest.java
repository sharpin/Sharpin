package com.sharpin.sharpin.service.handlers.deletecard;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DeleteCardDirectoryNameHandlerTest {

    @Mock
    DeleteCardService deleteCardService;

    @InjectMocks
    private Handler handler = new DeleteCardDirectoryNameHandler();

    /**
     * Function to set up the tests.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("Directory Name Handler", handler.getName());
    }

    @Test
    public void whenGivenNullShouldNotSetState() {
        handler.handle("A123", null);
        verify(deleteCardService, times(0))
            .setState(eq("A123"), anyString());
    }

    @Test
    public void whenGivenNullShouldReturnMessage() {
        List<String> response = handler.handle("A123", null);
        assertEquals(1, response.size());
        assertEquals("Directory name cant be empty, please give a directory name",
            response.get(0));
    }

    @Test
    public void whenDirectoryDoesNotExistShouldNotSetState() {

        when(deleteCardService.setDirectoryName("B123", "dirname"))
            .thenReturn(false);
        verify(deleteCardService, times(0))
            .setState(eq("B123"), anyString());
    }

    @Test
    public void whenDirectoryDoesNotExistShouldGiveMessage() {

        when(deleteCardService.setDirectoryName("B123", "dirname"))
            .thenReturn(false);

        List<String> response = handler.handle("A123", "dirname");
        assertEquals(1, response.size());
        assertEquals("Directory does not exist, please give a valid directory name",
            response.get(0));

    }

    @Test
    public void whenGivenValidDirectoryShouldSetStateToCardName() {
        when(deleteCardService.setDirectoryName("B123", "dirname"))
            .thenReturn(true);
        handler.handle("B123", "dirname");
        verify(deleteCardService).setState("B123", "Card Name Handler");
    }

    @Test
    public void whenGivenValidDirectoryShouldAskForCardName() {
        when(deleteCardService.setDirectoryName("B123", "dirname"))
            .thenReturn(true);
        List<String> response = handler.handle("B123", "dirname");
        assertEquals(1, response.size());
        assertEquals("Card front?", response.get(0));
    }
}

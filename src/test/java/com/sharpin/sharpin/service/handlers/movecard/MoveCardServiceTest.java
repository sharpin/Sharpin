package com.sharpin.sharpin.service.handlers.movecard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.MoveCardState;
import com.sharpin.sharpin.repository.temporarydata.MoveCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

public class MoveCardServiceTest {

    @Mock
    MoveCardStateRepository moveCardStateRepository;

    @Mock
    DataComponentRepository dataComponentRepository;

    @InjectMocks
    MoveCardService moveCardService;

    /**
     * setting up function.
     */
    @BeforeEach
    public void setUp() {
        moveCardService = new MoveCardServiceImpl();
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testCreateUserCallsDateSave() {

        ArgumentCaptor<MoveCardState> argumentCaptor = ArgumentCaptor.forClass(MoveCardState.class);
        moveCardService.createUserData("A123");
        verify(moveCardStateRepository).save(argumentCaptor.capture());
        assertEquals("A123", argumentCaptor.getValue().getUserId());

    }

    @Test
    public void whenGetUserStateShouldReturnCorrectOne() {

        MoveCardState subject = new MoveCardState();
        subject.setState("current state");
        Optional<MoveCardState> returned = Optional.of(subject);
        when(moveCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("current state", moveCardService.getState("A123"));
    }

    @Test
    public void whenGetUserStateDoesNotExistShouldThrowException() {

        assertThrows(NoSuchElementException.class, () -> moveCardService.getState("A123"));
    }

    @Test
    public void whenGetFrontCardShouldReturnCorrectOne() {

        MoveCardState subject = new MoveCardState();
        subject.setCardFront("the card front");
        Optional<MoveCardState> returned = Optional.of(subject);
        when(moveCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the card front", moveCardService.getCardFront("A123"));

    }

    @Test
    public void whenGetOldDirectoryShouldReturnCorrectOne() {

        MoveCardState subject = new MoveCardState();
        subject.setOldDirectory("the old directory");
        Optional<MoveCardState> returned = Optional.of(subject);
        when(moveCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the old directory", moveCardService.getOldDirectory("A123"));
    }

    @Test
    public void whenGetNewDirectoryShouldReturnCorrectOne() {

        MoveCardState subject = new MoveCardState();
        subject.setNewDirectory("the new directory");
        Optional<MoveCardState> returned = Optional.of(subject);
        when(moveCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the new directory", moveCardService.getNewDirectory("A123"));
    }

    @Test
    public void whenSetStateShouldSetIt() {

        MoveCardState mockState = new MoveCardState();
        mockState.setState("the old state");
        Optional<MoveCardState> returned = Optional.of(mockState);
        when(moveCardStateRepository.findById(("A123"))).thenReturn(returned);

        ArgumentCaptor<MoveCardState> argumentCaptor = ArgumentCaptor.forClass(MoveCardState.class);
        moveCardService.setState("A123", "the new state");
        verify(moveCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("the new state", argumentCaptor.getValue().getState());
    }

    /**
     * Preparing for testing setolddirectory by creating a mock.
     * @param shouldItExist should the old directory exist
     */
    private void prepareOldDirectoryTest(boolean shouldItExist) {
        MoveCardState mockState = new MoveCardState();
        mockState.setOldDirectory("old dir1");
        Optional<MoveCardState> returned = Optional.of(mockState);
        when(moveCardStateRepository.findById(("A123"))).thenReturn(returned);

        if (shouldItExist) {
            Directory mockOldDir = mock(Directory.class);
            when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "old dir2"))
                .thenReturn(Optional.of(mockOldDir));
        } else {
            when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "old dir2"))
                .thenReturn(Optional.empty());
        }
    }

    @Test
    public void whenSetOldDirectoryShouldExistSetIt() {

        prepareOldDirectoryTest(true);

        ArgumentCaptor<MoveCardState> argumentCaptor = ArgumentCaptor.forClass(MoveCardState.class);
        moveCardService.setOldDirectory("A123", "old dir2");
        verify(moveCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("old dir2", argumentCaptor.getValue().getOldDirectory());
    }

    @Test
    public void whenSetOldDirectoryExistShouldReturnTrue() {

        prepareOldDirectoryTest(true);
        assertTrue(moveCardService.setOldDirectory("A123", "old dir2"));
    }

    @Test
    public void whenSetOldDirectoryNotFoundShouldNotSetIt() {

        prepareOldDirectoryTest(false);
        moveCardService.setOldDirectory("A123", "old dir2");
        verify(moveCardStateRepository, times(0)).save(any());
    }

    @Test
    public void whenSetOldDirectoryNotFoundShouldReturnFalse() {
        prepareOldDirectoryTest(false);
        assertFalse(moveCardService.setOldDirectory("A123", "old dir2"));
    }

    /**
     * setting up the directory for new directory tests below.
     * @param shouldItExist should the new directory exist?
     */
    private void setUpNewDirectoryTests(boolean shouldItExist) {
        MoveCardState mockState = new MoveCardState();
        mockState.setNewDirectory("new dir1");
        Optional<MoveCardState> returned = Optional.of(mockState);
        when(moveCardStateRepository.findById(("A123"))).thenReturn(returned);

        if (shouldItExist) {
            Directory mockOldDir = mock(Directory.class);
            when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "new dir2"))
                .thenReturn(Optional.of(mockOldDir));
        } else {
            when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "new dir2"))
                .thenReturn(Optional.empty());
        }
    }

    @Test
    public void whenSetNewDirectoryExistShouldSetIt() {

        setUpNewDirectoryTests(true);

        ArgumentCaptor<MoveCardState> argumentCaptor = ArgumentCaptor.forClass(MoveCardState.class);
        moveCardService.setNewDirectory("A123", "new dir2");
        verify(moveCardStateRepository).save(argumentCaptor.capture());
        assertEquals("new dir2", argumentCaptor.getValue().getNewDirectory());
    }

    @Test
    public void whenSetNewDirectoryExistShouldReturnTrue() {
        setUpNewDirectoryTests(true);
        assertTrue(moveCardService.setNewDirectory("A123", "new dir2"));
    }

    @Test
    public void whenSetNewDirectoryNotExistShouldNotSetIt() {
        setUpNewDirectoryTests(false);

        moveCardService.setNewDirectory("A123", "new dir2");
        verify(moveCardStateRepository, times(0)).save(any());
    }

    @Test
    public void whenSetNewDirectoryNotExistShouldReturnFalse() {

        setUpNewDirectoryTests(false);

        assertFalse(moveCardService.setNewDirectory("A123", "new dir2"));
    }

    private void setUpDirectoryForCardFront(boolean shouldCardFrontExist) {
        MoveCardState mockState = new MoveCardState();
        mockState.setCardFront("card front1");
        mockState.setOldDirectory("biology");
        Optional<MoveCardState> returned = Optional.of(mockState);
        when(moveCardStateRepository.findById(("A123"))).thenReturn(returned);

        Directory mockOld = mock(Directory.class);
        when(mockOld.isCardFrontExistAsDirectChild("card front2"))
            .thenReturn(shouldCardFrontExist);
        when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "biology"))
            .thenReturn(Optional.of(mockOld));
    }

    @Test
    public void whenSetCardValidFrontShouldSetIt() {

        setUpDirectoryForCardFront(true);

        ArgumentCaptor<MoveCardState> argumentCaptor = ArgumentCaptor.forClass(MoveCardState.class);
        moveCardService.setCardFront("A123", "card front2");
        verify(moveCardStateRepository).save(argumentCaptor.capture());
        assertEquals("card front2", argumentCaptor.getValue().getCardFront());
    }

    @Test
    public void whenSetCardValidFrontShouldReturnTrue() {

        setUpDirectoryForCardFront(true);

        assertTrue(moveCardService.setCardFront("A123", "card front2"));
    }

    @Test
    public void whenInvalidCardFrontGivenShouldNotSave() {

        setUpDirectoryForCardFront(false);

        moveCardService.setCardFront("A123", "card front2");
        verify(moveCardStateRepository, times(0)).save(any());
    }

    @Test
    public void whenInvalidCardFrontGivenShouldReturnFalse() {

        setUpDirectoryForCardFront(false);

        assertFalse(moveCardService.setCardFront("A123", "card front2"));
    }

    @Test
    public void whenDeleteDataShouldCallDelete() {

        moveCardService.deleteUserData("A123");
        verify(moveCardStateRepository, times(1)).deleteById("A123");
    }


    private Directory mockOldDir;
    private Directory mockNewDir;
    private Card mockCard;

    /**
     * Setting up for handlechanges tests.
     * @param shouldNewDirectoryAlreadyContainCard should the new directory already contain card.
     */
    private void setUpHandleChangesMocking(boolean shouldNewDirectoryAlreadyContainCard) {
        MoveCardState mockState = new MoveCardState();
        mockState.setCardFront("mitocondria cells");
        mockState.setNewDirectory("Biochemistry");
        mockState.setOldDirectory("cells");
        Optional<MoveCardState> returned = Optional.of(mockState);
        when(moveCardStateRepository.findById(("A123"))).thenReturn(returned);

        mockOldDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "cells"))
            .thenReturn(Optional.of(mockOldDir));

        mockCard = mock(Card.class);
        when(mockOldDir.findCardByName("mitocondria cells")).thenReturn(mockCard);

        mockNewDir = mock(Directory.class);
        when(mockNewDir.isCardFrontExistAsDirectChild("mitocondria cells"))
            .thenReturn(shouldNewDirectoryAlreadyContainCard);
        when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "Biochemistry"))
            .thenReturn(Optional.of(mockNewDir));
    }

    @Test
    public void whenHandleChangesShouldOnValidReturnMessage() {

        setUpHandleChangesMocking(false);

        assertEquals("Card mitocondria cells moved from cells to Biochemistry!",
            moveCardService.handleChanges("A123"));

    }

    @Test
    public void whenHandleChangesOnValidShouldDeleteOldAndSaveNew() {
        setUpHandleChangesMocking(false);

        moveCardService.handleChanges("A123");
        verify(mockOldDir).removeChildByCardFront("mitocondria cells");
        verify(dataComponentRepository).save(mockOldDir);

        verify(mockNewDir).addChild(mockCard);
        verify(dataComponentRepository).save(mockNewDir);


    }

    @Test
    public void whenHandleChangesOnInValidShouldReturnMessage() {

        setUpHandleChangesMocking(true);

        String expectedResponse = "The card mitocondria cells already exist in Biochemistry. "
            + "Canceling movecard process, please try again";
        assertEquals(expectedResponse, moveCardService.handleChanges("A123"));
    }

    @Test
    public void whenHandleChangesOnInvalidShouldNotSave() {

        setUpHandleChangesMocking(true);
        moveCardService.handleChanges("A123");
        verify(dataComponentRepository, times(0)).save(any());
    }

    @Test
    public void whenDeleteUserGivesErrorShouldDoNothing() {

        doThrow(new EmptyResultDataAccessException(1))
            .when(moveCardStateRepository).deleteById("A123");
        moveCardService.deleteUserData("A123");
    }
}

package com.sharpin.sharpin.service.handlers.sessionhandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.card.UserCardList;
import com.sharpin.sharpin.service.DataService;
import com.sharpin.sharpin.service.JsonProcessingHelper;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;



public class QuizHandlerTest {

    @Mock
    JsonProcessingHelper helper;

    @Mock
    UserStateService userStateService;

    @Mock
    DataService dataService;

    @Mock
    Environment environment;

    @InjectMocks
    QuizHandler handler = new QuizHandler();

    List<Card> notEmptyList;

    /**
     * Setting up tests.
     */
    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        when(environment.getProperty("SESSION_URL")).thenReturn("http://localhost:8081");

        notEmptyList = new ArrayList<>();
        Card mockCard = mock(Card.class);
        notEmptyList.add(mockCard);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        assertEquals("\\quiz", handler.getName());
    }

    @Test
    public void whenCalledShouldCreateUserData() {

        when(dataService.getCardsFrom("A123", "Naruto", 5))
            .thenReturn(notEmptyList);
        handler.handle("A123", "Naruto");
        verify(userStateService).createUserData("A123");
    }

    @Test
    public void whenCalledShouldSetUserState() {

        when(dataService.getCardsFrom("A123", "Naruto", 5))
            .thenReturn(notEmptyList);
        handler.handle("A123", "Naruto");
        verify(userStateService).setUserState("A123", "session");
    }

    @Test
    public void whenCalledOnNoMessageShouldReturnErrorMessage() {
        List<String> response = handler.handle("A123", null);
        assertEquals(1, response.size());
        assertEquals("Please enter a topic!", response.get(0));
    }

    @Test
    public void whenCalledOnNoMessageShouldNotCreateUser() {
        handler.handle("A123", null);
        verify(userStateService, times(0)).createUserData("A123");
    }

    @Test
    public void whenCalledOnNoMessageShouldNotSetUserState() {
        handler.handle("A123", null);
        verify(userStateService, times(0)).setUserState("A123", "session");
    }

    @Test
    public void whenCalledOnMessageShouldGetCards() {
        handler.handle("A123", "Naruto");
        verify(dataService).getCardsFrom("A123", "Naruto", 5);
    }

    /**
     * Helper function to serialize cards.
     */
    public String serializeCards(UserCardList userCardList) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userCardList);
    }

    @Test
    public void whenPostQuizShouldPostCorrectJson() throws JsonProcessingException {

        Card card1 = new NormalCard("Card1", "Back of Card1", "notes of card1");
        Card card2 = new NormalCard("Card2", "Back of Card2", "notes of card2");
        card1.setCardId(1);
        card2.setCardId(2);
        List<Card> cardList = new ArrayList<>();
        cardList.add(card1);
        cardList.add(card2);

        UserCardList tst = new UserCardList("A123", cardList);
        String expectedJson = serializeCards(tst);
        handler.postQuiz("A123", cardList);
        verify(helper).postRequest(anyString(), eq(expectedJson));
    }

    @Test
    public void whenPostQuizShouldReturnCorrectString() throws JsonProcessingException {

        Card card1 = new NormalCard("Card1", "Back of Card1", "notes of card1");
        Card card2 = new NormalCard("Card2", "Back of Card2", "notes of card2");
        card1.setCardId(1);
        card2.setCardId(2);
        List<Card> cardList = new ArrayList<>();
        cardList.add(card1);
        cardList.add(card2);

        UserCardList tst = new UserCardList("A123", cardList);
        String expectedJson = serializeCards(tst);

        List<String> responses = new ArrayList<>();
        responses.add("Quiz initialized!");
        responses.add("Next question: Card1");

        when(helper.postRequest(anyString(), eq(expectedJson))).thenReturn("responseJson");
        when(helper.getListFromJson("responseJson", "returned_data", "responses"))
            .thenReturn(responses);

        List<String> gotResponses = handler.postQuiz("A123", tst);
        assertEquals("Quiz initialized!", responses.get(0));
        assertEquals("Next question: Card1", responses.get(1));
    }

    @Test
    public void whenDirectoryDoesNotExistShouldShouldReturnTheError() {

        when(dataService.getCardsFrom("E123", "anime", 5))
            .thenThrow(new NoSuchElementException("Directory does not exist!"));

        List<String> responses = handler.handle("E123", "anime");
        assertEquals(1, responses.size());
        assertEquals("Directory does not exist!", responses.get(0));

    }

    @Test
    public void whenDirectoryHasNoCardsShouldReturnTheError() {

        List<Card> emptyList = new ArrayList<>();
        when(dataService.getCardsFrom("E123", "Anime", 5))
            .thenReturn(emptyList);

        List<String> responses = handler.handle("E123", "anime");
        assertEquals(1, responses.size());
        assertEquals("No cards to quiz on this directory", responses.get(0));
    }


    @Test
    public void whenJsonProcessingErrorShouldGiveTheList() throws JsonProcessingException {

        when(dataService.getCardsFrom("a123", "test message", 5))
            .thenReturn(notEmptyList);

        when(helper.postRequest(anyString(), anyString()))
            .thenThrow(new NullPointerException());

        List<String> responses = handler.handle("a123", "test message");
        assertEquals(1, responses.size());
        assertEquals("Error when parsing, please try again!", responses.get(0));

    }

}

package com.sharpin.sharpin.service.handlers.movecard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;



public class CardFrontHandlerTest {

    @Mock
    MoveCardService moveCardService;

    @InjectMocks
    Handler handler = new CardFrontHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenGetNameShouldReturnCorrectName() {
        Assertions.assertEquals("Card Front Handler", handler.getName());
    }

    @Test
    public void whenNullMessageShouldReturnCantBeNull() {
        assertEquals("Card front name can't be empty!", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenNullMessageShouldNotChangeState() {
        handler.handle("A123", null);
        verify(moveCardService, times(0)).setState(eq("A123"), anyString());
    }

    @Test
    public void whenNullMessageShouldNotSaveName() {
        handler.handle("A123", null);
        verify(moveCardService, times(0)).setCardFront(eq("A123"), anyString());
    }

    @Test
    public void whenGivenCardFrontValidShouldSaveIt() {
        when(moveCardService.setCardFront("B123", "random card front"))
            .thenReturn(true);
        assertEquals("New directory?",
            handler.handle("B123", "random card front").get(0));

    }

    @Test
    public void whenGivenCardFrontShouldChangeState() {
        when(moveCardService.setCardFront("B123","random card front"))
            .thenReturn(true);
        handler.handle("B123", "random card front");
        verify(moveCardService).setState("B123", "New Directory Handler");
    }

    @Test
    public void whenGivenCardFrontInvalidShouldGiveError() {
        when(moveCardService.setCardFront("C123", "random card front"))
            .thenReturn(false);
        List<String> response = handler.handle("C123", "random card front");
        assertEquals("ERROR: Card random card front does not exist.", response.get(0));
        assertEquals("Card front?", response.get(1));
    }

    @Test
    public void whenGivenCardFrontInvalidShouldNotSave() {
        when(moveCardService.setCardFront("C123", "random card front"))
            .thenReturn(false);
        handler.handle("C123", "random card front");
        verify(moveCardService, times(0)).setState(eq("C123"), anyString());
    }
}

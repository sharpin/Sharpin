package com.sharpin.sharpin.service.handlers.addcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.AddCardState;
import com.sharpin.sharpin.repository.temporarydata.AddCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

public class AddCardServiceTest {

    @Mock
    AddCardStateRepository addCardStateRepository;

    @Mock
    DataComponentRepository dataComponentRepository;

    @InjectMocks
    AddCardService addCardService;

    /**
     * setting up function.
     */
    @BeforeEach
    public void setUp() {
        addCardService = new AddCardServiceImpl();
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testCreateUserCallsDateSave() {

        ArgumentCaptor<AddCardState> argumentCaptor = ArgumentCaptor.forClass(AddCardState.class);
        addCardService.createUserData("A123");
        verify(addCardStateRepository).save(argumentCaptor.capture());
        assertEquals("A123", argumentCaptor.getValue().getUserId());

    }

    @Test
    public void whenGetUserStateShouldReturnCorrectOne() {

        AddCardState subject = new AddCardState();
        subject.setState("current state");
        Optional<AddCardState> returned = Optional.of(subject);
        when(addCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("current state", addCardService.getState("A123"));
    }

    @Test
    public void whenGetUserStateDoesNotExistShouldThrowException() {

        assertThrows(NoSuchElementException.class, () -> addCardService.getState("A123"));
    }

    @Test
    public void whenGetFrontCardShouldReturnCorrectOne() {

        AddCardState subject = new AddCardState();
        subject.setCardFront("the card front");
        Optional<AddCardState> returned = Optional.of(subject);
        when(addCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the card front", addCardService.getCardFront("A123"));

    }

    @Test
    public void whenGetBackCardShouldReturnCorrectOne() {

        AddCardState subject = new AddCardState();
        subject.setCardBack("the card back");
        Optional<AddCardState> returned = Optional.of(subject);
        when(addCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the card back", addCardService.getCardBack("A123"));

    }

    @Test
    public void whenGetNotesCardShouldReturnCorrectOne() {

        AddCardState subject = new AddCardState();
        subject.setCardNotes("the card notes");
        Optional<AddCardState> returned = Optional.of(subject);
        when(addCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the card notes", addCardService.getCardNotes("A123"));

    }

    @Test
    public void whenGetDirectoryShouldReturnCorrectOne() {

        AddCardState subject = new AddCardState();
        subject.setDirectory("the new directory");
        Optional<AddCardState> returned = Optional.of(subject);
        when(addCardStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("the new directory", addCardService.getDirectory("A123"));
    }

    @Test
    public void whenSetStateShouldSetIt() {

        AddCardState mockState = new AddCardState();
        mockState.setState("the old state");
        Optional<AddCardState> returned = Optional.of(mockState);
        when(addCardStateRepository.findById(("A123"))).thenReturn(returned);

        ArgumentCaptor<AddCardState> argumentCaptor = ArgumentCaptor.forClass(AddCardState.class);
        addCardService.setState("A123", "the new state");
        verify(addCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("the new state", argumentCaptor.getValue().getState());
    }

    @Test
    public void whenSetCardFrontShouldSetIt() {

        AddCardState mockState = new AddCardState();
        mockState.setCardFront("card front1");
        Optional<AddCardState> returned = Optional.of(mockState);
        when(addCardStateRepository.findById(("A123"))).thenReturn(returned);

        ArgumentCaptor<AddCardState> argumentCaptor = ArgumentCaptor.forClass(AddCardState.class);
        addCardService.setCardFront("A123", "card front2");
        verify(addCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("card front2", argumentCaptor.getValue().getCardFront());
    }

    @Test
    public void whenSetCardBackShouldSetIt() {

        AddCardState mockState = new AddCardState();
        mockState.setCardBack("card back1");
        Optional<AddCardState> returned = Optional.of(mockState);
        when(addCardStateRepository.findById(("A123"))).thenReturn(returned);

        ArgumentCaptor<AddCardState> argumentCaptor = ArgumentCaptor.forClass(AddCardState.class);
        addCardService.setCardBack("A123", "card back2");
        verify(addCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("card back2", argumentCaptor.getValue().getCardBack());
    }

    @Test
    public void whenSetCardNotesShouldSetIt() {

        AddCardState mockState = new AddCardState();
        mockState.setCardNotes("card note1");
        Optional<AddCardState> returned = Optional.of(mockState);
        when(addCardStateRepository.findById(("A123"))).thenReturn(returned);

        ArgumentCaptor<AddCardState> argumentCaptor = ArgumentCaptor.forClass(AddCardState.class);
        addCardService.setCardNotes("A123", "card note2");
        verify(addCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("card note2", argumentCaptor.getValue().getCardNotes());
    }

    @Test
    public void whenSetDirectoryShouldSetIt() {

        AddCardState mockState = new AddCardState();
        mockState.setDirectory("old dir1");
        Optional<AddCardState> returned = Optional.of(mockState);
        when(addCardStateRepository.findById(("A123"))).thenReturn(returned);
        Directory dirw = new Directory("old dir2","A123");
        Optional<Directory> dir = Optional.of(dirw);
        when(dataComponentRepository.findByUserIdAndDirectoryName("A123", "old dir2"))
                .thenReturn(dir);

        ArgumentCaptor<AddCardState> argumentCaptor = ArgumentCaptor.forClass(AddCardState.class);
        addCardService.setDirectory("A123", "old dir2");
        verify(addCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("old dir2", argumentCaptor.getValue().getDirectory());
    }

    @Test
    public void whenSetNonExistedDirectoryShouldSetReturnFalse() {

        AddCardState mockState = new AddCardState();
        mockState.setDirectory("old dir1");
        Optional<AddCardState> returned = Optional.of(mockState);
        when(addCardStateRepository.findById(("A123"))).thenReturn(returned);

        assertEquals(false, addCardService.setDirectory("A123", "old dir2"));
    }

    @Test
    public void whenDeleteDataShouldCallDelete() {

        addCardService.deleteUserData("A123");
        verify(addCardStateRepository, times(1)).deleteById("A123");
    }

    @Test
    public void whenDeleteDataOnNoDataShouldDoNothing() {
        doThrow(new EmptyResultDataAccessException(1))
            .when(addCardStateRepository).deleteById("A123");
        addCardService.deleteUserData("A123");
    }

    @Test
    public void whenHandleChangesOnAlreadyExistDirectoryShouldReturnMessage() {
        AddCardState mockState = new AddCardState();
        mockState.setCardFront("mitocondria cells");
        mockState.setCardBack("Biochemistry cels");
        mockState.setCardNotes("this is notes");
        mockState.setDirectory("cells");
        mockState.setUserId("B123");
        when(addCardStateRepository.findById("B123"))
            .thenReturn(Optional.of(mockState));

        Directory mockDir = mock(Directory.class);
        when(mockDir.isCardFrontExistAsDirectChild("mitocondria cells"))
            .thenReturn(true);
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "cells"))
            .thenReturn(Optional.of(mockDir));

        assertEquals("Card with front mitocondria cells already exist in cells"
                +  ". Canceling addcard process, please try again",
            addCardService.handleChanges("B123"));
    }

    @Test
    public void whenHandleChangesOnAlreadyExistDirectoryShouldNotSave() {

        AddCardState mockState = new AddCardState();
        mockState.setCardFront("mitocondria cells");
        mockState.setCardBack("Biochemistry cels");
        mockState.setCardNotes("this is notes");
        mockState.setDirectory("cells");
        mockState.setUserId("B123");
        when(addCardStateRepository.findById("B123"))
            .thenReturn(Optional.of(mockState));

        Directory mockDir = mock(Directory.class);
        when(mockDir.isCardFrontExistAsDirectChild("mitocondria cells"))
            .thenReturn(true);
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "cells"))
            .thenReturn(Optional.of(mockDir));

        verify(dataComponentRepository, times(0)).save(any());

    }

    @Test
    public void whenHandleChangesOnValidShouldReturnMessage() {
        AddCardState mockState = new AddCardState();
        mockState.setCardFront("mitocondria cells");
        mockState.setCardBack("Biochemistry cels");
        mockState.setCardNotes("this is notes");
        mockState.setDirectory("cells");
        mockState.setUserId("B123");
        when(addCardStateRepository.findById("B123"))
            .thenReturn(Optional.of(mockState));

        Directory mockDir = mock(Directory.class);
        when(mockDir.isCardFrontExistAsDirectChild("mitocondria cells"))
            .thenReturn(false);
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "cells"))
            .thenReturn(Optional.of(mockDir));

        assertEquals("Card mitocondria cells added to cells!",
            addCardService.handleChanges("B123"));
    }

    @Test
    public void whenHandleChangesOnValidShouldSaveCard() {
        AddCardState mockState = new AddCardState();
        mockState.setCardFront("mitocondria cells");
        mockState.setCardBack("Biochemistry cels");
        mockState.setCardNotes("this is notes");
        mockState.setDirectory("cells");
        mockState.setUserId("B123");
        when(addCardStateRepository.findById("B123"))
            .thenReturn(Optional.of(mockState));

        Directory mockDir = mock(Directory.class);
        when(mockDir.isCardFrontExistAsDirectChild("mitocondria cells"))
            .thenReturn(false);
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "cells"))
            .thenReturn(Optional.of(mockDir));

        addCardService.handleChanges("B123");

        ArgumentCaptor<Card> capture = ArgumentCaptor.forClass(Card.class);
        verify(mockDir).addChild(capture.capture());
        Card savedCard = capture.getValue();
        assertEquals("mitocondria cells", savedCard.getFront());
        assertEquals("Biochemistry cels", savedCard.getBack());
        assertEquals("this is notes", savedCard.getNotes());

        verify(dataComponentRepository).save(mockDir);
    }
}

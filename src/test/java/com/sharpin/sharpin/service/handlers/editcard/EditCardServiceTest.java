package com.sharpin.sharpin.service.handlers.editcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.EditCardState;
import com.sharpin.sharpin.repository.temporarydata.EditCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

public class EditCardServiceTest {
    
    @Mock
    EditCardStateRepository editCardStateRepository;

    @Mock
    DataComponentRepository dataComponentRepository;
    
    @InjectMocks
    EditCardService editCardService;
    
    @BeforeEach
    public void setUp() {
        editCardService = new EditCardServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateUserCallsDateSave() {
        ArgumentCaptor<EditCardState> argumentCaptor = ArgumentCaptor.forClass(EditCardState.class);
        editCardService.createUserData("1234");
        verify(editCardStateRepository).save(argumentCaptor.capture());
        assertEquals("1234", argumentCaptor.getValue().getUserId());
    }

    @Test
    public void whenGetUserStateShouldReturnCorrectOne() {
        EditCardState subject = new EditCardState();
        subject.setState("current state");
        Optional<EditCardState> returned = Optional.of(subject);
        when(editCardStateRepository.findById("1234")).thenReturn(returned);

        assertEquals("current state", editCardService.getState("1234"));
    }

    @Test
    public void whenGetUserStateDoesNotExistShouldThrowException() {
        assertThrows(NoSuchElementException.class, () -> editCardService.getState("1234"));
    }

    @Test
    public void getDirectoryNameShouldReturnCorrectName() {
        EditCardState sample = new EditCardState();
        sample.setDirectoryName("directory");
        Optional<EditCardState> returned = Optional.of(sample);
        when(editCardStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("directory", editCardService.getDirectoryName("1234"));
    }

    @Test
    public void getCardFrontShouldReturnCorrectDirectory() {
        EditCardState sample = new EditCardState();
        sample.setCardFront("card");
        Optional<EditCardState> returned = Optional.of(sample);
        when(editCardStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("card", editCardService.getCardFront("1234"));
    }

    @Test
    public void getNewFrontShouldReturnCorrectDirectory() {
        EditCardState sample = new EditCardState();
        sample.setNewFront("front");
        Optional<EditCardState> returned = Optional.of(sample);
        when(editCardStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("front", editCardService.getNewFront("1234"));
    }

    @Test
    public void getNewBackShouldReturnCorrectDirectory() {
        EditCardState sample = new EditCardState();
        sample.setNewBack("back");
        Optional<EditCardState> returned = Optional.of(sample);
        when(editCardStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("back", editCardService.getNewBack("1234"));
    }

    @Test
    public void getNewNoteShouldReturnCorrectDirectory() {
        EditCardState sample = new EditCardState();
        sample.setNewNote("note");
        Optional<EditCardState> returned = Optional.of(sample);
        when(editCardStateRepository.findById("1234")).thenReturn(returned);
        assertEquals("note", editCardService.getNewNote("1234"));
    }

    @Test
    public void whenSetStateShouldSetIt() {

        EditCardState mockState = new EditCardState();
        mockState.setState("old state");
        Optional<EditCardState> returned = Optional.of(mockState);
        when(editCardStateRepository.findById(("1234"))).thenReturn(returned);

        ArgumentCaptor<EditCardState> argumentCaptor = ArgumentCaptor.forClass(EditCardState.class);
        editCardService.setState("1234", "new state");
        verify(editCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("new state", argumentCaptor.getValue().getState());
    }

    private void setUpSetDirectoryMocking(boolean shouldDirectoryExist) {
        EditCardState mockState = new EditCardState();
        mockState.setDirectoryName("directory 0");
        Optional<EditCardState> returned = Optional.of(mockState);
        when(editCardStateRepository.findById(("1234"))).thenReturn(returned);

        if (shouldDirectoryExist) {
            Directory mockDir = mock(Directory.class);
            Optional<Directory> odir = Optional.of(mockDir);
            when(dataComponentRepository.findByUserIdAndDirectoryName("1234", "directory 1"))
                .thenReturn(odir);
        } else {
            when(dataComponentRepository.findByUserIdAndDirectoryName("1234", "directory 1"))
                .thenReturn(Optional.empty());
        }
    }

    @Test
    public void whenSetDirectoryNameShouldSetIt() {

        setUpSetDirectoryMocking(true);

        ArgumentCaptor<EditCardState> argumentCaptor = ArgumentCaptor.forClass(EditCardState.class);
        editCardService.setDirectoryName("1234", "directory 1");
        verify(editCardStateRepository).save(argumentCaptor.capture());
        assertEquals("directory 1", argumentCaptor.getValue().getDirectoryName());
    }

    @Test
    public void whenSetDirectoryNameShouldReturnTrue() {
        setUpSetDirectoryMocking(true);

        assertTrue(editCardService.setDirectoryName("1234", "directory 1"));
    }

    @Test
    public void whenSetDirectoryNameDoesNotExistShouldNotSave() {
        setUpSetDirectoryMocking(false);
        editCardService.setDirectoryName("1234", "directory 1");
        verify(editCardStateRepository, times(0)).save(any());
    }

    @Test
    public void whenSetDirectoryNameDoesNotExistShouldReturnFalse() {
        setUpSetDirectoryMocking(false);
        assertFalse(editCardService.setDirectoryName("1234", "directory 1"));
    }

    @Test
    public void whenSetDirectoryNameOnInvalidShouldNotSetIt() {

    }

    @Test
    public void whenSetNewBackShouldSetIt() {

        EditCardState mockState = new EditCardState();
        mockState.setNewBack("back 0");
        Optional<EditCardState> returned = Optional.of(mockState);
        when(editCardStateRepository.findById(("1234"))).thenReturn(returned);

        ArgumentCaptor<EditCardState> argumentCaptor = ArgumentCaptor.forClass(EditCardState.class);
        editCardService.setNewBack("1234", "back 1");
        verify(editCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("back 1", argumentCaptor.getValue().getNewBack());
    }

    @Test
    public void whenSetNewNoteShouldSetIt() {

        EditCardState mockState = new EditCardState();
        mockState.setNewNote("note 0");
        Optional<EditCardState> returned = Optional.of(mockState);
        when(editCardStateRepository.findById(("1234"))).thenReturn(returned);

        ArgumentCaptor<EditCardState> argumentCaptor = ArgumentCaptor.forClass(EditCardState.class);
        editCardService.setNewNote("1234", "note 1");
        verify(editCardStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("note 1", argumentCaptor.getValue().getNewNote());
    }


    @Test
    public void whenDeleteDataShouldCallDelete() {

        editCardService.deleteUserData("1234");
        verify(editCardStateRepository, times(1)).deleteById("1234");
    }

    @Test
    public void whenDeleteGiveErrorShouldDoNothing() {
        doThrow(new EmptyResultDataAccessException(1))
            .when(editCardStateRepository).deleteById("A123");
        editCardService.deleteUserData("A123");
    }

    private void setUpSetCardFrontMocking(boolean shouldCardExist) {

        EditCardState mockState = new EditCardState();
        mockState.setDirectoryName("Diseases");
        when(editCardStateRepository.findById("B123")).thenReturn(Optional.of(mockState));

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "Diseases"))
            .thenReturn(Optional.of(mockDir));

        if (shouldCardExist) {
            Card mockCard = new NormalCard("SARS", "A widespread diseases", "Causes coffing");
            when(mockDir.findCardByName("SARS")).thenReturn(mockCard);
        } else {
            when(mockDir.findCardByName(("SARS"))).thenReturn(null);
        }

    }

    @Test
    public void whenSetCardFrontOnCardShouldReturnTrue() {

        setUpSetCardFrontMocking(true);

        assertTrue(editCardService.setCardFront("B123", "SARS"));
    }

    @Test
    public void whenSetCardFrontOnValidShouldSave() {

        setUpSetCardFrontMocking(true);

        editCardService.setCardFront("B123", "SARS");
        ArgumentCaptor<EditCardState> capture = ArgumentCaptor.forClass(EditCardState.class);
        verify(editCardStateRepository).save(capture.capture());
        EditCardState saved = capture.getValue();
        assertEquals("SARS", saved.getOldFront());
        assertEquals("A widespread diseases", saved.getOldBack());
        assertEquals("Causes coffing", saved.getOldNote());

    }

    @Test
    public void whenSetCardFrontOnInvalidShouldReturnFalse() {

        setUpSetCardFrontMocking(false);

        assertFalse(editCardService.setCardFront("B123", "SARS"));
    }

    @Test
    public void whenSetCardFrontOnInvalidShouldNotSave() {

        setUpSetCardFrontMocking(false);

        editCardService.setCardFront("B123", "SARS");
        verify(editCardStateRepository, times(0)).save(any());

    }

    private void setUpNewCardFrontMocking(boolean shouldNewCardFrontExist) {

        EditCardState mockState = new EditCardState();
        mockState.setDirectoryName("Drinks");
        when(editCardStateRepository.findById("B123")).thenReturn(Optional.of(mockState));

        Directory mockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("B123", "Drinks"))
            .thenReturn(Optional.of(mockDir));

        when(mockDir.isCardFrontExistAsDirectChild("Water"))
            .thenReturn(shouldNewCardFrontExist);

    }

    @Test
    public void whenKeepSameShouldSave() {

        setUpNewCardFrontMocking(true);

        editCardService.setNewFront("B123", "keepsame");
        ArgumentCaptor<EditCardState> capture = ArgumentCaptor.forClass(EditCardState.class);
        verify(editCardStateRepository).save(capture.capture());
        assertEquals("keepsame", capture.getValue().getNewFront());

    }

    @Test
    public void whenNewCardNameDoesNotExistShouldSave() {

        setUpNewCardFrontMocking(false);

        editCardService.setNewFront("B123", "Water");
        ArgumentCaptor<EditCardState> capture = ArgumentCaptor.forClass(EditCardState.class);
        verify(editCardStateRepository).save(capture.capture());
        assertEquals("Water", capture.getValue().getNewFront());

    }

    @Test
    public void whenSetNewCardNameDoesNotExistShouldReturnTrue() {
        setUpNewCardFrontMocking(false);

        assertTrue(editCardService.setNewFront("B123", "Water"));
    }

    @Test
    public void whenSetNewCardNameDoesExistShouldNotSave() {

        setUpNewCardFrontMocking(true);

        editCardService.setNewFront("B123", "Water");
        verify(editCardStateRepository, times(0)).save(any());
    }

    @Test
    public void whenSetNewCardNameDoesExistShouldReturnFalse() {

        setUpNewCardFrontMocking(true);

        assertFalse(editCardService.setNewFront("B123", "Water"));

    }

    @Test
    public void whenGetOldDataShouldGetIt() {

        EditCardState mockState = new EditCardState();
        mockState.setOldBack("x");
        mockState.setOldFront("y");
        mockState.setOldNote("z");
        when(editCardStateRepository.findById("C123")).thenReturn(Optional.of(mockState));

        assertEquals("y", editCardService.getOldFront("C123"));
        assertEquals("x", editCardService.getOldBack("C123"));
        assertEquals("z", editCardService.getOldNote("C123"));

    }

    private Directory handleChangesMockDir;
    private Card handleChangesMockCard;

    /**
     * Function to set up handle changes mocking.
     */
    private void setUpHandleChangesMocking(boolean isNotesKeepsame) {
        EditCardState mockState = new EditCardState();
        mockState.setNewFront("The question of everything");
        mockState.setNewBack("Philosophy");

        if (isNotesKeepsame) {
            mockState.setNewNote("keepsame");
        } else {
            mockState.setNewNote("just a random notes");
        }
        mockState.setUserId("D123");
        mockState.setCardFront("A subject in university");
        when(editCardStateRepository.findById("D123")).thenReturn(Optional.of(mockState));

        mockState.setDirectoryName("Problems");
        handleChangesMockDir = mock(Directory.class);
        when(dataComponentRepository.findByUserIdAndDirectoryName("D123", "Problems"))
            .thenReturn(Optional.of(handleChangesMockDir));

        handleChangesMockCard = mock(Card.class);
        when(handleChangesMockDir.findCardByName("A subject in university"))
            .thenReturn(handleChangesMockCard);
    }

    @Test
    public void whenHandleChangesShouldReturnCardEdited() {

        setUpHandleChangesMocking(true);

        assertEquals("Card edited!", editCardService.handleChanges("D123"));
    }

    @Test
    public void whenHandleChangesShouldUpdateCard() {

        setUpHandleChangesMocking(true);

        editCardService.handleChanges("D123");
        verify(handleChangesMockCard).updateFront("The question of everything");
        verify(handleChangesMockCard).updateBack("Philosophy");
        verify(handleChangesMockCard, times(0)).updateNotes(anyString());
        verify(dataComponentRepository).save(handleChangesMockDir);
    }

    @Test
    public void whenHandleChangesNotesNotKeepSameShouldUpdate() {
        setUpHandleChangesMocking(false);

        editCardService.handleChanges("D123");
        verify(handleChangesMockCard).updateFront("The question of everything");
        verify(handleChangesMockCard).updateBack("Philosophy");
        verify(handleChangesMockCard).updateNotes("just a random notes");
        verify(dataComponentRepository).save(handleChangesMockDir);
    }
}

package com.sharpin.sharpin.service.handlers.addcard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class InitialAddCardHandlerTest {

    @Mock
    UserStateService userStateService;

    @Mock
    AddCardService addCardService;

    @InjectMocks
    Handler handler = new com.sharpin.sharpin.service.handlers.addcard.InitialAddCardHandler();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetNameShouldReturnCorrectName() {
        assertEquals("Initial Handler", handler.getName());
    }

    @Test
    public void testHandleShouldSetUserStateToAddCard() {

        handler.handle("A123", null);
        verify(userStateService, times(1)).setUserState("A123", "\\addcard");
    }

    @Test
    public void testHandleShouldSetAddCardStateToAddCardFront() {
        handler.handle("A123", null);
        verify(addCardService, times(1)).setState("A123", "Card Front Handler");
    }

    @Test
    public void testWhenHandleShouldAskForDirectory() {

        assertEquals("Card front?", handler.handle("A123", null).get(0));
    }

    @Test
    public void whenHandleIsCalledItShouldAddCardState() {

        handler.handle("A123", "lul");
        verify(addCardService, times(1)).createUserData("A123");
    }

    @Test
    public void whenHandleIsCalledItShouldCreateUserState() {

        handler.handle("A123", "lul");
        verify(userStateService, times(1)).createUserData("A123");
    }
}

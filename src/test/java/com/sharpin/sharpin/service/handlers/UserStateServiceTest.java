package com.sharpin.sharpin.service.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.repository.temporarydata.UserState;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.dao.EmptyResultDataAccessException;


public class UserStateServiceTest {


    @Mock
    UserStateRepository userStateRepository;

    @InjectMocks
    private UserStateService userStateService;

    /**
     * set up method for tests.
     */
    @BeforeEach
    public void setUp() {

        userStateService = new UserStateServiceImpl();
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testCreateUserCallsDateSave() {

        ArgumentCaptor<UserState> argumentCaptor = ArgumentCaptor.forClass(UserState.class);
        userStateService.createUserData("A123");
        verify(userStateRepository).save(argumentCaptor.capture());
        assertEquals("A123", argumentCaptor.getValue().getUserId());

    }

    @Test
    public void whenGetUserStateShouldReturnCorrectOne() {

        UserState mockState = mock(UserState.class);
        when(mockState.getState()).thenReturn("testing state");
        Optional<UserState> returned = Optional.of(mockState);

        when(userStateRepository.findById("A123")).thenReturn(returned);

        assertEquals("testing state", userStateService.getUserState("A123"));

    }

    @Test
    public void whenGetuserDoesNotExistShouldThrowException() {

        Optional<UserState> returned = Optional.empty();
        when(userStateRepository.findById("A123")).thenReturn(returned);

        assertThrows(NoSuchElementException.class,
            () -> userStateService.getUserState("A123"));

    }

    @Test
    public void whenSetUserDoesNotExistShouldReturnException() {

        assertThrows(NoSuchElementException.class,
            () -> userStateService.setUserState("A123", "A123"));
    }

    @Test
    public void whenSetUserStateExistShouldSaveIt() {

        UserState mockState = new UserState();
        mockState.setState("the old state");
        Optional<UserState> returned = Optional.of(mockState);
        when(userStateRepository.findById(("A123"))).thenReturn(returned);

        ArgumentCaptor<UserState> argumentCaptor = ArgumentCaptor.forClass(UserState.class);
        userStateService.setUserState("A123", "the new state");
        verify(userStateRepository, times(1)).save(argumentCaptor.capture());
        assertEquals("the new state", argumentCaptor.getValue().getState());
    }

    @Test
    public void whenDeleteUserDoesNotExistShouldDoNothing() {

        userStateService.deleteUserData("A123");
    }

    @Test
    public void whenDeleteUserGivesErrorShouldDoNothing() {

        doThrow(new EmptyResultDataAccessException(1)).when(userStateRepository).deleteById("A123");
        userStateService.deleteUserData("A123");
    }

    @Test
    public void whenDeleteUserShouldCallRepositoryDelete() {

        userStateService.deleteUserData(("A123"));
        verify(userStateRepository, times(1)).deleteById("A123");
    }
}

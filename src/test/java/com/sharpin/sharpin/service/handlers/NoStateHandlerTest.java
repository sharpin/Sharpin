package com.sharpin.sharpin.service.handlers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.sharpin.sharpin.service.handlers.addcard.AddCardHandler;
import com.sharpin.sharpin.service.handlers.adddirectory.AddDirectoryHandler;
import com.sharpin.sharpin.service.handlers.deletecard.DeleteCardInitialHandler;
import com.sharpin.sharpin.service.handlers.editcard.EditCardHandler;
import com.sharpin.sharpin.service.handlers.helphandler.HelpHandler;
import com.sharpin.sharpin.service.handlers.listhandler.ListHandler;
import com.sharpin.sharpin.service.handlers.movecard.MoveCardHandler;
import com.sharpin.sharpin.service.handlers.sessionhandler.QuizHandler;
import com.sharpin.sharpin.service.handlers.sessionhandler.ReviewHandler;
import com.sharpin.sharpin.service.handlers.statisticshandler.StatisticsHandler;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class NoStateHandlerTest {

    @InjectMocks
    Handler handler = new NoStateHandler();

    @Mock
    MoveCardHandler moveCardHandler;

    @Mock
    QuizHandler quizHandler;

    @Mock
    ReviewHandler reviewHandler;

    @Mock
    AddCardHandler addCardHandler;

    @Mock
    EditCardHandler editCardHandler;

    @Mock
    AddDirectoryHandler addDirectoryHandler;

    @Mock
    VersionHandler versionHandler;

    @Mock
    StatisticsHandler statisticsHandler;

    @Mock
    ListHandler listHandler;

    @Mock
    DeleteCardInitialHandler deleteCardInitialHandler;

    @Mock
    HelpHandler helpHandler;

    /**
     * function to set-up test.
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(moveCardHandler.getName()).thenReturn("\\movecard");
        when(quizHandler.getName()).thenReturn("\\quiz");
        when(reviewHandler.getName()).thenReturn("\\review");
        when(addCardHandler.getName()).thenReturn("\\addcard");
        when(editCardHandler.getName()).thenReturn("\\editcard");
        when(addDirectoryHandler.getName()).thenReturn("\\adddirectory");
        when(statisticsHandler.getName()).thenReturn("\\statistics");
        when(versionHandler.getName()).thenReturn("\\version");
        when(listHandler.getName()).thenReturn("\\list");
        when(deleteCardInitialHandler.getName()).thenReturn("\\deletecard");
        when(helpHandler.getName()).thenReturn("\\help");
    }

    @Test
    public void testGetNameShouldReturnCorrectName() {

        assertEquals("No State Handler", handler.getName());
    }

    @Test
    public void testCommandNotFoundShouldReturnTheMessage() {

        assertEquals("Command not found, please enter \\help for the list of commands.",
            handler.handle("a123", "\\fakeCommand fakeinput").get(0));
    }

    @Test
    public void testVersionCommandShouldReturnVersion() {

        when(versionHandler.handle("a123", null)).thenReturn(Arrays.asList("Version 0.0.1"));
        assertEquals("Version 0.0.1",
            handler.handle("a123", "\\version").get(0));
    }

    @Test
    public void whenMoveCardShouldGoToCorrectMethod() {

        handler.handle("A123", "\\movecard");
        verify(moveCardHandler, times(1)).handle("A123", null);

    }

}

package com.sharpin.sharpin.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;





public class NavigationServiceTest {

    NavigationService navigationService;

    @Mock
    Directory rootDir;

    @Mock
    Directory child1;

    @Mock
    Directory child11;

    @Mock
    Directory child12;

    @Mock
    Card mockCard2;

    @Mock
    Card mockCard3;

    @Mock
    Card mockCard4;

    @Mock
    Card mockCard5;
    /**
     * Create a setup.
     */

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(rootDir.componentType()).thenReturn("Directory");
        when(child1.componentType()).thenReturn("Directory");
        when(child11.componentType()).thenReturn("Directory");
        when(child12.componentType()).thenReturn("Directory");
        when(mockCard2.componentType()).thenReturn("Card");
        when(mockCard3.componentType()).thenReturn("Card");
        when(mockCard4.componentType()).thenReturn("Card");
        when(mockCard5.componentType()).thenReturn("Card");


        when(rootDir.getChildrenSize()).thenReturn(1);
        when(rootDir.getChild(0)).thenReturn(child1);

        when(child1.getChildrenSize()).thenReturn(4);
        when(child1.getChild(0)).thenReturn(child11);
        when(child1.getChild(1)).thenReturn(mockCard2);
        when(child1.getChild(2)).thenReturn(mockCard3);
        when(child1.getChild(3)).thenReturn(child12);


        when(child11.getChildrenSize()).thenReturn(1);
        when(child11.getChild(0)).thenReturn(mockCard4);

        when(child12.getChildrenSize()).thenReturn(1);
        when(child12.getChild(0)).thenReturn(mockCard5);

        when(mockCard2.getFront()).thenReturn("Card 2");
        when(mockCard2.getSrsStage()).thenReturn(3);
        when(mockCard2.getRatioOfCorrectReviews()).thenReturn(0.6);

        when(mockCard3.getFront()).thenReturn("Card 3");
        when(mockCard3.getSrsStage()).thenReturn(5);
        when(mockCard3.getRatioOfCorrectReviews()).thenReturn(0.9);

        when(mockCard4.getFront()).thenReturn("Card 4");
        when(mockCard4.getSrsStage()).thenReturn(2);
        when(mockCard4.getRatioOfCorrectReviews()).thenReturn(0.2);

        when(mockCard5.getFront()).thenReturn("Card 5");
        when(mockCard5.getSrsStage()).thenReturn(5);
        when(mockCard5.getRatioOfCorrectReviews()).thenReturn(0.81);

        navigationService = new NavigationServiceImpl();
    }

    @Test
    public void testNoDirectoryShouldReturnEmptyList() {

        List<Directory> ret = navigationService.navigateDirectory(child11);
        assertEquals(0, ret.size());
    }

    @Test
    public void testChildDirectoryExistShouldReturnIt() {
        List<Directory> ret = navigationService.navigateDirectory(child1);
        assertEquals(2, ret.size());
    }

    @Test
    public void testNoCardsInDirectoryShouldReturnEmptyList() {

        List<Card> ret = navigationService.listCardInDirectory(rootDir);
        assertEquals(0, ret.size());
    }

    @Test
    public void testCardsExistInDirectoryShouldReturnIt() {

        List<Card> ret = navigationService.listCardInDirectory(child1);
        assertEquals(2, ret.size());
        assertEquals("Card 2", ret.get(0).getFront());
        assertEquals("Card 3", ret.get(1).getFront());
    }

    @Test
    public void testAllCardsFoundInHierarchy() {
        List<Card> ret = navigationService.listCardInHierarchy(rootDir);
        assertEquals(4, ret.size());
    }
}

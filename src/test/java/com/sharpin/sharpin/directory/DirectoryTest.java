package com.sharpin.sharpin.directory;

import static org.junit.jupiter.api.Assertions.*;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DirectoryTest {

    private Directory sampleDirectory;
    private Card sampleData;

    @BeforeEach
    public void setUp() {
        sampleDirectory = new Directory("directory", "123456");
        sampleData = new NormalCard("front", "back", "notes");
    }

    @Test
    public void testAddAndGetChild() {
        sampleDirectory.addChild(sampleData);
        assertEquals(sampleData, sampleDirectory.getChild(0));
    }

    @Test
    public void testRemoveChild() {
        sampleDirectory.addChild(sampleData);
        assertEquals(sampleData, sampleDirectory.getChild(0));
        sampleDirectory.removeChild(sampleData);
        assertTrue(sampleDirectory.isEmpty());
    }

    @Test
    public void testRemoveNotExistChild() {
        sampleDirectory.addChild(sampleData);
        sampleDirectory.removeChild(new NormalCard("front", "back", "notes"));
        assertEquals(1, sampleDirectory.getChildrenSize());
    }

    @Test
    public void directoryCanInheritAnotherDirectory() {
        String userId = "123456";

        Directory directory = new Directory("directory", userId);

        Directory childDirectory = new Directory("child directory", userId);
        directory.addChild(childDirectory);

        Card card0 = new NormalCard("front", "back", "card 0");
        Card card1 = new NormalCard("front", "back", "card 1");

        directory.addChild(card0);
        childDirectory.addChild(card1);

        assertEquals(2, directory.getChildrenSize());
        assertEquals(1, childDirectory.getChildrenSize());

        Directory sample = (Directory)directory.getChild(0);
        Card sampleCard = (Card)sample.getChild(0);

        assertEquals("front", sampleCard.getFront());
        assertEquals("back", sampleCard.getBack());
        assertEquals("card 1", sampleCard.getNotes());
    }

    @Test
    public void testComponentType() {

        assertEquals("Directory", sampleDirectory.componentType());
    }

    @Test
    public void getUserIdShouldBeCorrect() {
        assertEquals("123456", sampleDirectory.getUserId());
    }

    @Test
    public void getDirectoryNameShouldBeCorrect() {
        assertEquals("directory", sampleDirectory.getDirectoryName());
    }

    @Test
    public void toStringShouldReturnUserIdAndName() {
        assertEquals("directory 123456", sampleDirectory.toString());
    }

    @Test
    public void testSearchingForCardFront() {

        Directory d = new Directory("testdir", "A123");
        sampleDirectory.addChild(d);

        DataComponent tobeFound = new NormalCard("existcard1", "back", "notes");
        sampleDirectory.addChild(tobeFound);

        DataComponent randomCard = new NormalCard("existcard2", "back", "notes");
        sampleDirectory.addChild(randomCard);

        assertTrue(sampleDirectory.isCardFrontExistAsDirectChild("existcard1"));
        assertFalse(sampleDirectory.isCardFrontExistAsDirectChild("testdir"));
        assertFalse(sampleDirectory.isCardFrontExistAsDirectChild("bad directory"));


    }

    @Test
    public void testDeletingForCardFront() {

        Directory d = new Directory("testdir", "A123");
        sampleDirectory.addChild(d);

        DataComponent tobeFound = new NormalCard("existcard1", "back", "notes");
        sampleDirectory.addChild(tobeFound);

        DataComponent randomCard = new NormalCard("existcard2", "back", "notes");
        sampleDirectory.addChild(randomCard);

        sampleDirectory.removeChildByCardFront("existcard1");
        sampleDirectory.removeChildByCardFront("testdir");
        sampleDirectory.removeChildByCardFront("nonexistdir");
        assertEquals(2, sampleDirectory.getChildrenSize());

        Directory child1 = (Directory) sampleDirectory.getChild(0);
        assertEquals("testdir", child1.getDirectoryName());

        Card child2 = (Card) sampleDirectory.getChild(1);
        assertEquals("existcard2", child2.getFront());
    }

    @Test
    public void testFindCardByName() {

        sampleDirectory.addChild(sampleData);
        assertEquals(sampleData, sampleDirectory.findCardByName("front"));
        assertNull(sampleDirectory.findCardByName("not card front"));
    }
}

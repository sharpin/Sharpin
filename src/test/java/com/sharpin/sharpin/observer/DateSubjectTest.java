package com.sharpin.sharpin.observer;

import static org.mockito.Mockito.when;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class DateSubjectTest {

    @Mock
    Card mockCard;

    @Mock
    Card mockCard2;

    @Mock
    DataComponentRepository dataComponentRepository;

    @InjectMocks
    private DateSubject dateSubject = new DateSubject();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testnotifyObserver() {
        List<Card> listCard = new ArrayList<>();
        listCard.add(mockCard);
        listCard.add(mockCard2);

        NormalCard card1 = new NormalCard("x", "y", "z");
        card1.setNextReview(new Date());
        listCard.add(card1);

        NormalCard card2 = new NormalCard("x", "y", "z");
        card2.setNextReview(new Date());
        listCard.add(card2);

        NormalCard card3 = new NormalCard("x", "y", "z");
        card3.setNextReview(new Date());
        listCard.add(card3);
        when(dataComponentRepository.listAllCards()).thenReturn(listCard);

        Date time = new Date();
        dateSubject.asyncNotification();
        Mockito.verify(mockCard).update(time);
        Mockito.verify(mockCard2).update(time);
    }

}

package com.sharpin.sharpin.observer;

import java.util.Date;

public interface DateObserver {
    public void update(Date date);
}

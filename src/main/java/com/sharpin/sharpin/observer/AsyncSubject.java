package com.sharpin.sharpin.observer;

import java.util.Date;

public interface AsyncSubject {
    void asyncNotification();

}


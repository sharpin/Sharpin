package com.sharpin.sharpin.observer;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class DateSubject implements AsyncSubject {

    @Autowired
    DataComponentRepository dataComponentRepository;

    public DateSubject() {

    }

    private void notifyObserver(Date date) {
        List<Card> cardList = dataComponentRepository.listAllCards();
        for (Card card : cardList) {
            CompletableFuture<Card> completableFuture = CompletableFuture.supplyAsync(() -> {
                card.update(date);
                dataComponentRepository.save(card);
                return card;
            });
        }
    }

    private Date getTime() {

        return new Date();
    }


    @Override
    @Scheduled(cron = "0 0 * * * ?")
    public void asyncNotification() {
        Date newDate = getTime();
        System.out.println("notifying all cards at :" + newDate);
        notifyObserver(newDate);
    }
}


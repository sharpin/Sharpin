package com.sharpin.sharpin.service;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;

public interface NavigationService {

    List<Directory> navigateDirectory(Directory directory);

    List<Card> listCardInDirectory(Directory directory);

    List<Card> listCardInHierarchy(DataComponent directory);
}

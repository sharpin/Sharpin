package com.sharpin.sharpin.service;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.springframework.stereotype.Service;


@Service
public class StatisticsServiceImpl implements StatisticsService {

    /**
     * List all of the cards.
     */
    public List<CardAnswerData> listAllCards(Directory directory) {
        List<CardAnswerData> allCards = new ArrayList<>();

        for (int i = 0; i < directory.getChildrenSize(); i++) {

            if (directory.getChild(i).componentType().equals("Card")) {
                Card cardEntries = (Card)directory.getChild(i);
                double cardCorrectRatio = cardEntries.getRatioOfCorrectReviews();

                CardAnswerData data = new CardAnswerData(cardEntries.getFront(), cardCorrectRatio);
                allCards.add(data);

            } else if (directory.getChild(i).componentType().equals("Directory")) {
                List<CardAnswerData> wrongList = listAllCards((Directory) directory.getChild(i));

                for (int j = 0; j < wrongList.size(); j++) {
                    CardAnswerData childData = wrongList.get(j);
                    allCards.add(childData);
                }
            }
        }
        return allCards;
    }

    @Override
    public List<CardAnswerData> getMostWrongAnsweredCard(Directory directory, int displayedCards) {
        List<CardAnswerData> correctRatios = listAllCards(directory);
        List<CardAnswerData> mostWrong = new ArrayList<>();

        Collections.sort(correctRatios);

        if (displayedCards > correctRatios.size()) {
            for (int i = 0; i < correctRatios.size(); i++) {
                mostWrong.add(correctRatios.get(i));
            }
        } else {
            for (int i = 0; i < displayedCards; i++) {
                mostWrong.add(correctRatios.get(i));
            }
        }
        return mostWrong;
    }


    @Override
    public List<LevelData> getNumberOfCardsInEachLevel(Directory directory) {
        int[] levels = new int[9];
        List<LevelData> cardsInLevel = new ArrayList<>();

        for (int i = 0; i < directory.getChildrenSize(); i++) {
            if (directory.getChild(i).componentType().equals("Card")) {
                Card cardEntries = (Card) directory.getChild(i);
                int cardLevel = cardEntries.getSrsStage();

                levels[cardLevel]++;

            } else if (directory.getChild(i).componentType().equals("Directory")) {
                Directory recentDir = (Directory)directory.getChild(i);
                List<LevelData> dirCards = getNumberOfCardsInEachLevel(recentDir);

                for (int j = 0; j < dirCards.size(); j++) {
                    int cardFromChildDirLevel = dirCards.get(j).srsStage;
                    levels[cardFromChildDirLevel] += dirCards.get(j).numberOfCards;
                }
            }
        }

        for (int i = 0; i < levels.length; i++) {
            LevelData newLevel = new LevelData(i, levels[i]);
            cardsInLevel.add(newLevel);
        }

        return cardsInLevel;
    }
}

package com.sharpin.sharpin.service.handlers.movecard;

public interface MoveCardService {

    String getState(String userId);

    String getOldDirectory(String userId);

    public String getCardFront(String userId);

    public String getNewDirectory(String userId);

    void setState(String userId, String nextState);

    boolean setOldDirectory(String userId, String oldDirectory);

    boolean setCardFront(String userId, String cardFront);

    boolean setNewDirectory(String userId, String nextDirectory);

    void deleteUserData(String userId);

    void createUserData(String userId);

    String handleChanges(String userId);

}

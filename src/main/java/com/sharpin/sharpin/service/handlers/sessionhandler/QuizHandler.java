package com.sharpin.sharpin.service.handlers.sessionhandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.UserCardList;
import com.sharpin.sharpin.service.DataService;
import com.sharpin.sharpin.service.JsonProcessingHelper;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class QuizHandler implements Handler {

    @Autowired
    private DataService dataService;

    @Autowired
    private UserStateService userStateService;

    @Autowired
    private JsonProcessingHelper jsonHelper;

    @Autowired
    private Environment environment;

    @Override
    public String getName() {
        return "\\quiz";
    }

    @Override
    public List<String> handle(String userId, String message) {
        List<String> res = new ArrayList<>();

        if (message == null) {
            List<String> responses = new ArrayList<>();
            responses.add("Please enter a topic!");
            return responses;
        }
        String topic = message;
        List<Card> questionedCards = new ArrayList<>();
        try {
            questionedCards = dataService.getCardsFrom(userId, topic, 5);
        } catch (NoSuchElementException e) {
            List<String> responses = new ArrayList<>();
            responses.add(e.getMessage());
            return responses;
        }

        if (questionedCards.size() == 0) {
            res.add("No cards to quiz on this directory");
            return res;
        }

        userStateService.createUserData(userId);
        userStateService.setUserState(userId, "session");
        return postQuiz(userId, questionedCards);
    }

    /**
     * Function to post the quiz.
     */
    public List<String> postQuiz(String userId, List<Card> cards) {

        String baseApiUrl = environment.getProperty("SESSION_URL");
        String apiUrl = baseApiUrl + "/startquiz";
        UserCardList userCardList = new UserCardList(userId, cards);
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            String serializedCards = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(userCardList);

            String response = jsonHelper.postRequest(apiUrl, serializedCards);
            List<String> responses = jsonHelper
                .getListFromJson(response, "returned_data", "responses");
            return responses;
        } catch (Exception e) {
            List<String> response = new ArrayList<>();
            response.add("Error when parsing, please try again!");
            return response;
        }

    }
}

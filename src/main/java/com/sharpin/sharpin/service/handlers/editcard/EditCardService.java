package com.sharpin.sharpin.service.handlers.editcard;

public interface EditCardService {

    String getState(String userId);

    String getDirectoryName(String userId);

    String getCardFront(String userId);

    String getOldFront(String userId);

    String getOldBack(String userId);

    String getOldNote(String userId);

    String getNewFront(String userId);

    String getNewBack(String userId);

    String getNewNote(String userId);

    void setState(String userId, String nextState);

    boolean setDirectoryName(String userId, String directoryName);

    boolean setCardFront(String userId, String cardFront);

    boolean setNewFront(String userId, String newFront);

    void setNewBack(String userId, String newBack);

    void setNewNote(String userId, String newNote);

    void deleteUserData(String userId);

    void createUserData(String userId);

    String handleChanges(String userId);

}

package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectoryNameEcHandler implements Handler {

    @Autowired
    EditCardService editCardService;

    @Override
    public String getName() {
        return "Directory Name Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Directory name can't be empty!");
        } else if (editCardService.setDirectoryName(userId, message)) {
            editCardService.setState(userId, "Card Front Handler");
            res.add("Changing card, type keepsame to not change");
            res.add("Card front?");
        } else {
            editCardService.setState(userId, "Directory Name Handler");
            res.add("ERROR: Directory with name " + message + " does not exist");
            res.add("Directory name?");
        }

        return res;

    }
}

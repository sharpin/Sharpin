package com.sharpin.sharpin.service.handlers;

import com.sharpin.sharpin.service.handlers.addcard.AddCardHandler;
import com.sharpin.sharpin.service.handlers.adddirectory.AddDirectoryHandler;
import com.sharpin.sharpin.service.handlers.deletecard.DeleteCardInitialHandler;
import com.sharpin.sharpin.service.handlers.editcard.EditCardHandler;
import com.sharpin.sharpin.service.handlers.helphandler.HelpHandler;
import com.sharpin.sharpin.service.handlers.listhandler.ListHandler;
import com.sharpin.sharpin.service.handlers.movecard.MoveCardHandler;
import com.sharpin.sharpin.service.handlers.sessionhandler.QuizHandler;
import com.sharpin.sharpin.service.handlers.sessionhandler.ReviewHandler;
import com.sharpin.sharpin.service.handlers.statisticshandler.StatisticsHandler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoStateHandler implements Handler {

    @Autowired
    MoveCardHandler moveCardHandler;

    @Autowired
    AddCardHandler addCardHandler;

    @Autowired
    AddDirectoryHandler addDirectoryHandler;

    @Autowired
    EditCardHandler editCardHandler;

    @Autowired
    ReviewHandler reviewHandler;

    @Autowired
    QuizHandler quizHandler;

    @Autowired
    StatisticsHandler statisticsHandler;

    @Autowired
    ListHandler listHandler;

    @Autowired
    VersionHandler versionHandler;

    @Autowired
    DeleteCardInitialHandler deleteCardHandler;

    @Autowired
    HelpHandler helpHandler;

    @Override
    public String getName() {
        return "No State Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {
        List<Handler> nextHandlers = new ArrayList<>();
        nextHandlers.add(moveCardHandler);
        nextHandlers.add(versionHandler);
        nextHandlers.add(addCardHandler);
        nextHandlers.add(addDirectoryHandler);
        nextHandlers.add(editCardHandler);
        nextHandlers.add(reviewHandler);
        nextHandlers.add(quizHandler);
        nextHandlers.add(statisticsHandler);
        nextHandlers.add(listHandler);
        nextHandlers.add(deleteCardHandler);
        nextHandlers.add(helpHandler);

        String[] tokenized = message.split(" ", 2);
        String command = tokenized[0];
        String body = (tokenized.length > 1) ? tokenized[1] : null;

        for (Handler handler : nextHandlers) {
            if (command.equals(handler.getName())) {
                return handler.handle(userId, body);
            }
        }

        List<String> res = new ArrayList<>();
        res.add("Command not found, please enter \\help for the list of commands.");
        return res;

    }
}

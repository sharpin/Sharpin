package com.sharpin.sharpin.service.handlers;

import com.sharpin.sharpin.repository.temporarydata.UserState;
import com.sharpin.sharpin.repository.temporarydata.UserStateRepository;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class UserStateServiceImpl implements UserStateService {

    @Autowired
    UserStateRepository userStateRepository;

    @Override
    public String getUserState(String userId) {

        UserState returned = userStateRepository.findById(userId).get();
        return returned.getState();
    }

    @Override
    public void setUserState(String userId, String newState) {
        UserState returned = userStateRepository.findById(userId).get();
        returned.setState(newState);
        userStateRepository.save(returned);
    }

    @Override
    public void deleteUserData(String userId) {

        try {
            userStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String donothing = "do nothing";
        }
    }

    @Override
    public void createUserData(String userId) {

        UserState userState = new UserState();
        userState.setUserId(userId);
        userStateRepository.save(userState);
    }
}

package com.sharpin.sharpin.service.handlers.adddirectory;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddDirectoryHandler implements Handler {

    @Autowired
    AddDirectoryService addDirectoryService;

    @Autowired
    UserStateService userStateService;

    @Autowired
    DirectoryNameHandler directoryNameHandler;

    @Autowired
    ParentDirectoryHandler parentDirectoryHandler;

    @Autowired
    InitHandler initHandler;

    @Override
    public String getName() {
        return "\\adddirectory";
    }

    @Override
    public List<String> handle(String userId, String message) {
        if (message != null && message.equals("\\exit")) {
            addDirectoryService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            List<String> res = new ArrayList<>();
            res.add("Process canceled");
            return res;
        }

        List<Handler> stages = new ArrayList<>();
        stages.add(directoryNameHandler);
        stages.add(parentDirectoryHandler);

        String addDirectoryState = null;
        try {
            addDirectoryState = addDirectoryService.getState(userId);

        } catch (NoSuchElementException e) {
            return initHandler.handle(userId, message);
        }

        for (Handler nextStage : stages) {
            if (addDirectoryState.equals(nextStage.getName())) {
                return nextStage.handle(userId, message);
            }
        }

        addDirectoryService.deleteUserData(userId);
        userStateService.deleteUserData(userId);

        List<String> res = new ArrayList<>();
        res.add("Invalid stage reached!");
        return res;
    }

}

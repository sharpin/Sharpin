package com.sharpin.sharpin.service.handlers.adddirectory;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParentDirectoryHandler implements Handler {

    @Autowired
    AddDirectoryService addDirectoryService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "Parent Directory Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Directory name can't be empty!");
        } else if (addDirectoryService.setParentDirectory(userId, message)) {
            String responseToUser = addDirectoryService.handleChanges(userId);
            addDirectoryService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            res.add(responseToUser);
        } else {
            addDirectoryService.setState(userId, "Parent Directory Handler");
            res.add("ERROR: Directory doesn't exist");
            res.add("Parent directory?");
        }
        return res;
    }
}

package com.sharpin.sharpin.service.handlers;

import java.util.List;

public interface Handler {

    String getName();

    List<String> handle(String userId, String message);

}

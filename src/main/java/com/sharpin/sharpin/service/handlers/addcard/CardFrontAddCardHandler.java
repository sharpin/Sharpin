package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardFrontAddCardHandler implements Handler {

    @Autowired
    AddCardService addCardService;

    @Override
    public String getName() {
        return "Card Front Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Card front can't be empty!");
            return res;
        }

        addCardService.setCardFront(userId,message);
        addCardService.setState(userId,"Card Back Handler");
        res.add("Card back?");
        return res;
    }
}

package com.sharpin.sharpin.service.handlers.movecard;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.MoveCardState;
import com.sharpin.sharpin.repository.temporarydata.MoveCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class MoveCardServiceImpl implements MoveCardService {

    @Autowired
    MoveCardStateRepository moveCardStateRepository;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Override
    public String getState(String userId) {
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        return returned.getState();
    }

    @Override
    public String getOldDirectory(String userId) {
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        return returned.getOldDirectory();
    }

    @Override
    public String getCardFront(String userId) {
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        return returned.getCardFront();
    }

    @Override
    public String getNewDirectory(String userId) {
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        return returned.getNewDirectory();
    }

    @Override
    public void setState(String userId, String nextState) {
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        returned.setState(nextState);
        moveCardStateRepository.save(returned);
    }

    @Override
    public boolean setOldDirectory(String userId, String oldDirectory) {

        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, oldDirectory);
        if (data.isPresent()) {
            returned.setOldDirectory(oldDirectory);
            moveCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean setCardFront(String userId, String cardFront) {

        NormalCard card = null;

        // the directory will always exist because is already checked on setOldDirectory
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        Optional<Directory> dir = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, returned.getOldDirectory());

        Directory oldDir = dir.get();
        if (oldDir.isCardFrontExistAsDirectChild(cardFront)) {
            returned.setCardFront(cardFront);
            moveCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean setNewDirectory(String userId, String newDirectory) {
        MoveCardState returned = moveCardStateRepository.findById(userId).get();
        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, newDirectory);
        if (data.isPresent()) {
            returned.setNewDirectory(newDirectory);
            moveCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void deleteUserData(String userId) {

        try {
            moveCardStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String noting = "donothing";
        }
    }

    @Override
    public void createUserData(String userId) {
        MoveCardState newState = new MoveCardState();
        newState.setUserId(userId);
        moveCardStateRepository.save(newState);
    }

    @Override
    public String handleChanges(String userId) {
        MoveCardState cur = moveCardStateRepository.findById(userId).get();
        if (modify(userId, cur.getOldDirectory(), cur.getNewDirectory(), cur.getCardFront())) {
            return "Card " + cur.getCardFront() + " moved from "
                + cur.getOldDirectory() + " to " + cur.getNewDirectory() + "!";
        } else {
            return "The card " + cur.getCardFront() + " already exist in "
                + cur.getNewDirectory() + ". Canceling movecard process, please try again";
        }

    }

    private boolean modify(String userId, String oldDir, String newDir, String cardFront) {

        Directory newDirPlace = dataComponentRepository.findByUserIdAndDirectoryName(
            userId, newDir).get();

        // should let the caller know if the card already exist on the targeted directory
        if (newDirPlace.isCardFrontExistAsDirectChild(cardFront)) {
            return false;
        }

        Directory oldDirPlace = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, oldDir).get();

        final Card card = oldDirPlace.findCardByName(cardFront);
        oldDirPlace.removeChildByCardFront(cardFront);
        dataComponentRepository.save(oldDirPlace);

        // there is a possibility that newDir is an ancestor of oldDir
        // searching for new dir again will handle this
        newDirPlace = dataComponentRepository.findByUserIdAndDirectoryName(
            userId, newDir).get();
        newDirPlace.addChild(card);
        dataComponentRepository.save(newDirPlace);

        return true;


    }
}

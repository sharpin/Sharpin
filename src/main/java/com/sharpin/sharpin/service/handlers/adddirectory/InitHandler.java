package com.sharpin.sharpin.service.handlers.adddirectory;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitHandler implements Handler {

    @Autowired
    AddDirectoryService addDirectoryService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "Initial Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {
        userStateService.createUserData(userId);
        addDirectoryService.createUserData(userId);
        userStateService.setUserState(userId, "\\adddirectory");
        addDirectoryService.setState(userId, "Directory Name Handler");
        List<String> res = new ArrayList<>();
        res.add("Directory name?");
        return res;
    }
}

package com.sharpin.sharpin.service.handlers.adddirectory;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectoryNameHandler implements Handler {

    @Autowired
    AddDirectoryService addDirectoryService;

    @Override
    public String getName() {
        return "Directory Name Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Directory name can't be empty!");
        } else if (addDirectoryService.setDirectoryName(userId, message)) {
            addDirectoryService.setState(userId, "Parent Directory Handler");
            res.add("Parent directory?");
        } else {
            addDirectoryService.setState(userId, "Directory Name Handler");
            res.add("ERROR: Directory with name " + message + " already exists");
            res.add("Directory name?");
        }

        return res;

    }
}

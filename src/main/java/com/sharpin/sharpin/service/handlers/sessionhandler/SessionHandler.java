package com.sharpin.sharpin.service.handlers.sessionhandler;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.JsonBuilder;
import com.sharpin.sharpin.service.JsonProcessingHelper;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class SessionHandler implements Handler {

    @Autowired
    UserStateService userStateService;

    @Autowired
    JsonProcessingHelper jsonProcessingHelper;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    Environment environment;

    @Override
    public String getName() {
        return "session";
    }


    @Override
    public List<String> handle(String userId, String message) {
        if (message != null && message.equals("\\exit")) {
            userStateService.deleteUserData(userId);
            List<String> res = new ArrayList<>();
            res.add("Process canceled");
            return res;
        }

        String baseApiUrl = environment.getProperty("SESSION_URL");
        String apiUrl = baseApiUrl + "/answer";

        JsonBuilder jsonBuilder = new JsonBuilder();
        jsonBuilder.addEntry("answer", message);
        jsonBuilder.addEntry("userId", userId);
        String answerJson = jsonBuilder.createJson("answer_data");
        String returned = jsonProcessingHelper.postRequest(apiUrl, answerJson);

        // if the microservice gives error, should reset the session
        if (jsonProcessingHelper.getIntFromJson(returned, "returned_data", "exit_status") == 1) {
            userStateService.deleteUserData(userId);
            List<String> responses = new ArrayList<>();
            responses.add("An error occurred, please try again!");
            return responses;
        }

        List<String> responses = jsonProcessingHelper
                .getListFromJson(returned, "returned_data", "responses");

        // if there are srs updates, will update the corresponding cards
        // if there are unexpected errors (directory not found), session will terminate
        try {
            if (jsonProcessingHelper.isFieldInJson(returned, "returned_data", "srsUpdate")) {
                long cardId = jsonProcessingHelper
                        .getLongFromJson(returned, "returned_data", "answeredCardId");
                int srsUpdateValue = jsonProcessingHelper
                        .getIntFromJson(returned, "returned_data", "srsUpdate");
                String additionalResponse = updateCardSrs(cardId, srsUpdateValue);
                /*
                function may give response if srs card is added
                the response should be put before the last in responses list
                because the last one should be the next question or session terminator
                 */
                if (additionalResponse != null) {
                    if (responses.size() > 0) {
                        responses.add(responses.size() - 1, additionalResponse);
                    } else {
                        responses.add(additionalResponse);
                    }
                }
            }
        } catch (NoSuchElementException e) {
            userStateService.deleteUserData(userId);
            return Arrays.asList(e.getMessage());
        }

        // if the session ends, delete all data from this
        boolean hasSessionEnded = jsonProcessingHelper
                .getBooleanFromJson(returned, "returned_data", "session_ended");
        if (hasSessionEnded) {
            userStateService.deleteUserData(userId);
        }
        return responses;

    }

    // update card SRS and gives a notification to the user
    private String updateCardSrs(long cardId, int srsUpdateValue) throws NoSuchElementException {

        Card card = (Card) dataComponentRepository.findById(cardId).get();
        String returned = null;
        if (srsUpdateValue == 1) {
            card.increaseSrsStage();
            returned = "Card with front \"" + card.getFront()
                    + "\" SRS has been increased to " + card.getSrsStage();
        } else if (srsUpdateValue == -1) {
            card.decreaseSrsStage();
            returned = "Card with front \"" + card.getFront()
                    + "\" SRS has been decreased to " + card.getSrsStage();
        }
        dataComponentRepository.save(card);
        return returned;

    }
}

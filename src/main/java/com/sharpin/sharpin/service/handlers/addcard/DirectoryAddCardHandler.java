package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectoryAddCardHandler implements Handler {

    @Autowired
    AddCardService addCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "Directory Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Directory name can't be empty!");
            return res;
        }

        boolean directoryExisted = addCardService.setDirectory(userId, message);
        if (directoryExisted) {
            String responseToUser = addCardService.handleChanges(userId);
            addCardService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            res.add(responseToUser);
            return res;
        } else {
            addCardService.setState(userId, "Directory Handler");
            res.add("ERROR: Directory doesn't exist\n"
                    + "Directory ?");
            return res;
        }
    }
}

package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditCardInitHandler implements Handler {

    @Autowired
    EditCardService editCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "Initial Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {
        userStateService.createUserData(userId);
        editCardService.createUserData(userId);
        userStateService.setUserState(userId, "\\editcard");
        editCardService.setState(userId, "Directory Name Handler");
        List<String> res = new ArrayList<>();
        res.add("Directory name?");
        return res;
    }

}

package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.repository.temporarydata.AddCardState;

public interface AddCardService {

    String getState(String userID);

    public String getDirectory(String userId);

    public String getCardFront(String userId);

    public String getCardBack(String userId);

    public String getCardNotes(String userId);

    void setState(String userId, String nextState);

    void setCardFront(String userId, String cardFront);

    void setCardBack(String userId, String dardBack);

    void setCardNotes(String userId, String dardNotes);

    boolean setDirectory(String userId, String directory);

    void deleteUserData(String userId);

    void createUserData(String userId);

    String handleChanges(String userId);

}

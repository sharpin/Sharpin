package com.sharpin.sharpin.service.handlers;


import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.handlers.addcard.AddCardHandler;
import com.sharpin.sharpin.service.handlers.adddirectory.AddDirectoryHandler;
import com.sharpin.sharpin.service.handlers.deletecard.DeleteCardHandler;
import com.sharpin.sharpin.service.handlers.editcard.EditCardHandler;
import com.sharpin.sharpin.service.handlers.movecard.MoveCardHandler;
import com.sharpin.sharpin.service.handlers.sessionhandler.SessionHandler;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SharpinHandler implements Handler {

    @Autowired
    UserStateService userStateService;

    @Autowired
    MoveCardHandler moveCardHandler;

    @Autowired
    AddCardHandler addCardHandler;

    @Autowired
    AddDirectoryHandler addDirectoryHandler;

    @Autowired
    EditCardHandler editCardHandler;

    @Autowired
    NoStateHandler noStateHandler;

    @Autowired
    SessionHandler sessionHandler;

    @Autowired
    DataComponentRepository repository;

    @Autowired
    DeleteCardHandler deleteCardHandler;

    public String getName() {
        return "Sharpin Handler";
    }

    /**
     * Function to check if a user already has a root directory (and create it).
     * @param userId the users userId
     */
    public void addRootDirectory(String userId) {
        Optional<Directory> data = repository.findByUserIdAndDirectoryName(userId, "root");
        if (!data.isPresent()) {
            Directory rootDirectory = new Directory("root", userId);
            repository.save(rootDirectory);
        }
    }

    /**
     * Function to handle user's message.
     */
    public List<String> handle(String userId, String message) {

        /*
        each user has a "root" directory by default.
        this is created lazily when the user first uses the chatbot
         */
        addRootDirectory(userId);

        List<Handler> checkpointHandlers = new ArrayList<>();
        checkpointHandlers.add(moveCardHandler);
        checkpointHandlers.add(addCardHandler);
        checkpointHandlers.add(addDirectoryHandler);
        checkpointHandlers.add(editCardHandler);
        checkpointHandlers.add(sessionHandler);
        checkpointHandlers.add(deleteCardHandler);

        String userState = null;
        try {
            userState = userStateService.getUserState(userId);
        } catch (NoSuchElementException e) {
            return noStateHandler.handle(userId, message);
        }
        for (Handler handler : checkpointHandlers) {
            if (userState.equals(handler.getName())) {
                return handler.handle(userId, message);
            }
        }

        // no found state will get here, means user arrived at invalid state
        userStateService.deleteUserData(userId);
        List<String> res = new ArrayList<>();
        res.add("Invalid checkpoint reached");
        return res;

    }
}

package com.sharpin.sharpin.service.handlers.listhandler;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.NavigationService;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ListHandler implements Handler {

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    NavigationService navigationService;

    @Override
    public String getName() {
        return "\\list";
    }

    @Override
    public List<String> handle(String userId, String message) {
        List<String> res = new ArrayList<>();
        String[] splitted = message.split(" ");
        StringJoiner joiner = new StringJoiner("");
        for (int i = 1; i < splitted.length; i++) {
            joiner.add(splitted[i] + " ");
        }
        String supposed = joiner.toString().trim();
        if (supposed.equals("")) {
            res.add("ERROR: Please give a directory name.\n"
                + "For example: \\list card root");
        } else if (splitted[0].equals("dir")) {
            Optional<Directory> dir =
                    dataComponentRepository.findByUserIdAndDirectoryName(userId, supposed);
            if (dir.isPresent()) {
                Directory toBeListed = dir.get();
                List<Directory> directories = navigationService.navigateDirectory(toBeListed);
                String toBeListedDirName = toBeListed.getDirectoryName();
                if (directories.size() != 0) {
                    res.add("In directory " + toBeListedDirName + ". List of directories:\n");
                    for (int i = 0; i < directories.size(); i++) {
                        res.add("- " + directories.get(i).getDirectoryName() + "\n");
                    }
                } else {
                    res.add("In directory " + toBeListedDirName
                            + ". This directory does not contain any directory");
                }
            } else {
                res.add("ERROR: Directory doesn't exist\n"
                        + "Please try another directory");
            }
        } else if (splitted[0].equals("card")) {
            Optional<Directory> dir =
                    dataComponentRepository.findByUserIdAndDirectoryName(userId, supposed);
            if (dir.isPresent()) {
                Directory toBeListed = dir.get();
                List<Card> cardsInCurrentDir = navigationService.listCardInDirectory(toBeListed);
                String toBeListedDirname = toBeListed.getDirectoryName();
                if (cardsInCurrentDir.size() != 0) {
                    res.add("In directory " + toBeListedDirname
                            + ". List of cards in this directory:\n");
                    for (int i = 0; i < cardsInCurrentDir.size(); i++) {
                        res.add("- " + cardsInCurrentDir.get(i).getFront() + "\n");
                    }
                } else {
                    res.add("In directory " + toBeListedDirname
                            + ". This directory does not contain any card");
                }
            } else {
                res.add("ERROR: Directory doesn't exist\n"
                        + "Please try another directory");
            }
        } else {
            res.add("ERROR: Invalid list command. Please enter card or dir as the second command.\n"
                + "For example: \\list card root");
        }
        List<String> returned = new ArrayList<>();
        returned.add(listToString(res));
        return returned;
    }

    private String listToString(List<String> list) {
        String answer = "";
        for (String s : list) {
            answer += s;
        }
        return answer;
    }
}

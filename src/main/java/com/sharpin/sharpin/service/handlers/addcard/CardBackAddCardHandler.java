package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardBackAddCardHandler implements Handler {

    @Autowired
    AddCardService addCardService;

    @Override
    public String getName() {
        return "Card Back Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Card back can't be empty!");
            return res;
        }

        addCardService.setCardBack(userId,message);
        addCardService.setState(userId, "Card Notes Handler");
        res.add("Card notes?");
        return res;
    }
}

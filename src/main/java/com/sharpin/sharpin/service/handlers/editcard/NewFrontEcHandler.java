package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewFrontEcHandler implements Handler {

    @Autowired
    EditCardService editCardService;

    @Override
    public String getName() {
        return "New Front Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("New card front name can't be empty!");
        } else if (editCardService.setNewFront(userId, message)) {
            editCardService.setState(userId, "New Back Handler");
            res.add("This card's current back is:");
            res.add(editCardService.getOldBack(userId));
            res.add("New back?");
        } else {
            editCardService.setState(userId, "New Front Handler");
            res.add("ERROR: Card with front " + message + " already exists.");
            res.add("New front?");
        }

        return res;

    }
}

package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.EditCardState;
import com.sharpin.sharpin.repository.temporarydata.EditCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class EditCardServiceImpl implements EditCardService {

    @Autowired
    EditCardStateRepository editCardStateRepository;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Override
    public String getState(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getState();
    }

    @Override
    public String getDirectoryName(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getDirectoryName();
    }

    @Override
    public String getCardFront(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getCardFront();
    }

    @Override
    public String getOldFront(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getOldFront();
    }

    @Override
    public String getOldBack(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getOldBack();
    }

    @Override
    public String getOldNote(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getOldNote();
    }

    @Override
    public String getNewFront(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getNewFront();
    }

    @Override
    public String getNewBack(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getNewBack();
    }

    @Override
    public String getNewNote(String userId) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        return returned.getNewNote();
    }

    @Override
    public void setState(String userId, String nextState) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        returned.setState(nextState);
        editCardStateRepository.save(returned);
    }

    @Override
    public boolean setDirectoryName(String userId, String directoryName) {
        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, directoryName);

        if (data.isPresent()) {
            EditCardState returned = editCardStateRepository.findById(userId).get();
            returned.setDirectoryName(directoryName);
            editCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean setCardFront(String userId, String cardFront) {

        EditCardState returned = editCardStateRepository.findById(userId).get();
        Optional<Directory> dir = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, returned.getDirectoryName());
        Directory directory = dir.get();
        Card card = directory.findCardByName(cardFront);
        if (card != null) {
            returned.setCardFront(cardFront);
            returned.setOldFront(card.getFront());
            returned.setOldBack(card.getBack());
            returned.setOldNote(card.getNotes());
            editCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setNewFront(String userId, String newFront) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        if (newFront.equals("keepsame")) {
            returned.setNewFront("keepsame");
            editCardStateRepository.save(returned);
            return true;
        }
        Optional<Directory> dir = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, returned.getDirectoryName());
        Directory directory = dir.get();
        if (!directory.isCardFrontExistAsDirectChild(newFront)) {
            returned.setNewFront(newFront);
            editCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setNewBack(String userId, String newBack) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        returned.setNewBack(newBack);
        editCardStateRepository.save(returned);
    }

    @Override
    public void setNewNote(String userId, String newNote) {
        EditCardState returned = editCardStateRepository.findById(userId).get();
        returned.setNewNote(newNote);
        editCardStateRepository.save(returned);
    }

    @Override
    public void deleteUserData(String userId) {
        try {
            editCardStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String don = "nothing";
        }
    }

    @Override
    public void createUserData(String userId) {
        EditCardState newState = new EditCardState();
        newState.setUserId(userId);
        editCardStateRepository.save(newState);
    }

    @Override
    public String handleChanges(String userId) {
        EditCardState edited = editCardStateRepository.findById(userId).get();
        modify(edited);
        return "Card edited!";
    }

    private void modify(EditCardState state) {
        Optional<Directory> dir = dataComponentRepository.findByUserIdAndDirectoryName(
                state.getUserId(), state.getDirectoryName());
        Directory directory = dir.get();
        Card card = directory.findCardByName(state.getCardFront());
        if (!state.getNewFront().equals("keepsame")) {
            card.updateFront(state.getNewFront());
        }
        if (!state.getNewBack().equals("keepsame")) {
            card.updateBack(state.getNewBack());
        }
        if (!state.getNewNote().equals("keepsame")) {
            card.updateNotes(state.getNewNote());
        }
        dataComponentRepository.save(directory);
    }
}

package com.sharpin.sharpin.service.handlers.deletecard;

public interface DeleteCardService {

    void createUserData(String userId);

    void deleteUserData(String userId);

    void setState(String userId, String state);

    String getState(String userId);

    boolean setDirectoryName(String userId, String directoryName);

    boolean setCardFront(String userId, String cardFront);

    String handleChanges(String userId);
}

package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewNoteEcHandler implements Handler {

    @Autowired
    EditCardService editCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "New Note Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("New card note can't be empty!");
            return res;
        }
        editCardService.setNewNote(userId, message);
        String responseToUser = editCardService.handleChanges(userId);
        editCardService.deleteUserData(userId);
        userStateService.deleteUserData(userId);
        res.add(responseToUser);
        return res;

    }
}

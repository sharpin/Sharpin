package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardNotesAddCardHandler implements Handler {

    @Autowired
    AddCardService addCardService;

    @Override
    public String getName() {
        return "Card Notes Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Card notes can't be empty!");
            return res;
        }

        addCardService.setCardNotes(userId, message);
        addCardService.setState(userId, "Directory Handler");
        res.add("Directory?");
        return res;
    }
}

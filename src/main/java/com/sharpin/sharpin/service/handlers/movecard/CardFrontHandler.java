package com.sharpin.sharpin.service.handlers.movecard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardFrontHandler implements Handler {

    @Autowired
    MoveCardService moveCardService;

    @Override
    public String getName() {
        return "Card Front Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Card front name can't be empty!");
            return res;
        } else if (moveCardService.setCardFront(userId, message)) {
            moveCardService.setState(userId, "New Directory Handler");
            res.add("New directory?");
        } else {
            res.add("ERROR: Card " + message + " does not exist.");
            res.add("Card front?");
        }

        return res;
    }
}

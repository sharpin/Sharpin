package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitialAddCardHandler implements Handler {

    @Autowired
    AddCardService addCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "Initial Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        userStateService.createUserData(userId);
        addCardService.createUserData(userId);

        userStateService.setUserState(userId, "\\addcard");

        addCardService.setState(userId,"Card Front Handler");

        List<String> res = new ArrayList<>();
        res.add("Card front?");
        return res;
    }
}

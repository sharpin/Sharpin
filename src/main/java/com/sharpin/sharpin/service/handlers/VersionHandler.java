package com.sharpin.sharpin.service.handlers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class VersionHandler implements Handler {

    @Override
    public String getName() {
        return "\\version";
    }

    @Override
    public List<String> handle(String userId, String message) {
        List<String> res = new ArrayList<>();
        res.add("Version 0.0.1");
        return res;
    }
}

package com.sharpin.sharpin.service.handlers.deletecard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DeleteCardDirectoryNameHandler implements Handler {

    @Autowired
    private DeleteCardService deleteCardService;

    @Override
    public String getName() {
        return "Directory Name Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        if (message == null) {
            List<String> response = new ArrayList<>();
            response.add("Directory name cant be empty, please give a directory name");
            return response;
        }

        if (!deleteCardService.setDirectoryName(userId, message)) {
            List<String> response = new ArrayList<>();
            response.add("Directory does not exist, please give a valid directory name");
            return response;
        } else {
            deleteCardService.setState(userId, "Card Name Handler");
            List<String> response = new ArrayList<>();
            response.add("Card front?");
            return response;
        }

    }
}

package com.sharpin.sharpin.service.handlers;

import com.sharpin.sharpin.repository.temporarydata.UserState;

public interface UserStateService {

    String getUserState(String userId);

    void setUserState(String userId, String newState);

    void deleteUserData(String userId);

    void createUserData(String userId);

}

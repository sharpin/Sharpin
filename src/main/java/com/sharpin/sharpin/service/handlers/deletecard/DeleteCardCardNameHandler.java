package com.sharpin.sharpin.service.handlers.deletecard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteCardCardNameHandler implements Handler {

    @Autowired
    DeleteCardService deleteCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {

        return "Card Name Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        if (message == null) {
            List<String> response = new ArrayList<>();
            response.add("Card front cant be empty, please give a card front");
            return response;
        }

        if (!deleteCardService.setCardFront(userId, message)) {
            List<String> response = new ArrayList<>();
            response.add("The card front does not exist, please give a valid card front");
            return response;
        } else {
            String response = deleteCardService.handleChanges(userId);
            List<String> responses = new ArrayList<>();
            responses.add(response);
            userStateService.deleteUserData(userId);
            deleteCardService.deleteUserData(userId);
            return responses;
        }
    }
}

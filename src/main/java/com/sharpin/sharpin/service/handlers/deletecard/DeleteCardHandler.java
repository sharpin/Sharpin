package com.sharpin.sharpin.service.handlers.deletecard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteCardHandler implements Handler {

    @Autowired
    DeleteCardService deleteCardService;

    @Autowired
    UserStateService userStateService;

    @Autowired
    DeleteCardCardNameHandler deleteCardCardNameHandler;

    @Autowired
    DeleteCardDirectoryNameHandler deleteCardDirectoryNameHandler;

    @Override
    public String getName() {
        return "Delete Card Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {
        if (message != null && message.equals("\\exit")) {
            deleteCardService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            List<String> res = new ArrayList<>();
            res.add("Process canceled");
            return res;
        }

        List<Handler> nextHandlers = new ArrayList<>();
        nextHandlers.add(deleteCardCardNameHandler);
        nextHandlers.add(deleteCardDirectoryNameHandler);

        String state = deleteCardService.getState(userId);
        for (Handler handler: nextHandlers) {
            if (state.equals(handler.getName())) {
                return handler.handle(userId, message);
            }
        }
        deleteCardService.deleteUserData(userId);
        userStateService.deleteUserData(userId);
        List<String> response = new ArrayList<>();
        response.add("Invalid state reached, please try again!");
        return response;
    }
}

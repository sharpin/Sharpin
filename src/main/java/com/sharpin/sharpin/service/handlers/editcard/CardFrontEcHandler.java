package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardFrontEcHandler implements Handler {

    @Autowired
    EditCardService editCardService;

    @Override
    public String getName() {
        return "Card Front Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Card front name can't be empty!");
        } else if (editCardService.setCardFront(userId, message)) {
            editCardService.setState(userId, "New Front Handler");
            res.add("This card's current front is:");
            res.add(editCardService.getOldFront(userId));
            res.add("New front?");
        } else {
            editCardService.setState(userId, "Card Front Handler");
            res.add("ERROR: Card with front " + message + " does not exist");
            res.add("Card front?");
        }

        return res;

    }
}

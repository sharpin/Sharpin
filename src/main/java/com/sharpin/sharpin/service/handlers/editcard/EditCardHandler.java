package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditCardHandler implements Handler {

    @Autowired
    EditCardService editCardService;

    @Autowired
    UserStateService userStateService;

    @Autowired
    DirectoryNameEcHandler directoryNameEcHandler;

    @Autowired
    CardFrontEcHandler cardFrontEcHandler;

    @Autowired
    NewFrontEcHandler newFrontEcHandler;

    @Autowired
    NewBackEcHandler newBackEcHandler;

    @Autowired
    NewNoteEcHandler newNoteEcHandler;

    @Autowired
    EditCardInitHandler editCardInitHandler;

    @Override
    public String getName() {
        return "\\editcard";
    }

    @Override
    public List<String> handle(String userId, String message) {
        if (message != null && message.equals("\\exit")) {
            editCardService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            List<String> res = new ArrayList<>();
            res.add("Process canceled");
            return res;
        }

        List<Handler> stages = new ArrayList<>();
        stages.add(directoryNameEcHandler);
        stages.add(cardFrontEcHandler);
        stages.add(newFrontEcHandler);
        stages.add(newBackEcHandler);
        stages.add(newNoteEcHandler);

        String editCardState = null;
        try {
            editCardState = editCardService.getState(userId);

        } catch (NoSuchElementException e) {
            return editCardInitHandler.handle(userId, message);
        }

        for (Handler nextStage : stages) {
            if (editCardState.equals(nextStage.getName())) {
                return nextStage.handle(userId, message);
            }
        }

        editCardService.deleteUserData(userId);
        userStateService.deleteUserData(userId);

        List<String> res = new ArrayList<>();
        res.add("Invalid stage reached!");
        return res;
    }
}

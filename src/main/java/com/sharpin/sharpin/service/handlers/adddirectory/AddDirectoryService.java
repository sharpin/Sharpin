package com.sharpin.sharpin.service.handlers.adddirectory;

public interface AddDirectoryService {

    String getState(String userId);

    String getDirectoryName(String userId);

    public String getParentDirectory(String userId);

    void setState(String userId, String nextState);

    boolean setDirectoryName(String userId, String directoryName);

    boolean setParentDirectory(String userId, String parentDirectory);

    void deleteUserData(String userId);

    void createUserData(String userId);

    String handleChanges(String userId);

}

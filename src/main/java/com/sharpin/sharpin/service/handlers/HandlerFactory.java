package com.sharpin.sharpin.service.handlers;

public interface HandlerFactory {

    Handler create(String handlerName);
}

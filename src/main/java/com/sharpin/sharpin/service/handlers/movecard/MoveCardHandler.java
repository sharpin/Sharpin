package com.sharpin.sharpin.service.handlers.movecard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MoveCardHandler implements Handler {

    @Autowired
    MoveCardService moveCardService;

    @Autowired
    UserStateService userStateService;

    @Autowired
    OldDirectoryHandler oldDirectoryHandler;

    @Autowired
    CardFrontHandler cardFrontHandler;

    @Autowired
    NewDirectoryHandler newDirectoryHandler;

    @Autowired
    InitialHandler initialHandler;

    @Override
    public String getName() {
        return "\\movecard";
    }

    @Override
    public List<String> handle(String userId, String message) {
        if (message != null && message.equals("\\exit")) {
            moveCardService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            List<String> res = new ArrayList<>();
            res.add("Process canceled");
            return res;
        }

        List<Handler> stages = new ArrayList<>();
        stages.add(oldDirectoryHandler);
        stages.add(newDirectoryHandler);
        stages.add(cardFrontHandler);

        String moveCardState = null;
        try {
            moveCardState = moveCardService.getState(userId);

        } catch (NoSuchElementException e) {
            return initialHandler.handle(userId, message);
        }

        for (Handler nextStage : stages) {
            if (moveCardState.equals(nextStage.getName())) {
                return nextStage.handle(userId, message);
            }
        }

        moveCardService.deleteUserData(userId);
        userStateService.deleteUserData(userId);
        List<String> res = new ArrayList<>();
        res.add("Invalid stage reached!");
        return res;

    }
}


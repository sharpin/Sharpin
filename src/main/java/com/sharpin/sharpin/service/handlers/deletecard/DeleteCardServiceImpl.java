package com.sharpin.sharpin.service.handlers.deletecard;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.DeleteCardState;
import com.sharpin.sharpin.repository.temporarydata.DeleteCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class DeleteCardServiceImpl implements DeleteCardService {

    @Autowired
    DeleteCardStateRepository deleteCardStateRepository;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Override
    public void createUserData(String userId) {
        DeleteCardState state = new DeleteCardState();
        state.setUserId(userId);
        deleteCardStateRepository.save(state);
    }

    @Override
    public void deleteUserData(String userId) {

        try {
            deleteCardStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String don = "nothing";
        }
    }

    @Override
    public void setState(String userId, String state) {

        DeleteCardState currentState = deleteCardStateRepository.findById(userId).get();
        currentState.setState(state);
        deleteCardStateRepository.save(currentState);
    }

    @Override
    public String getState(String userId) {
        DeleteCardState currentState = deleteCardStateRepository.findById(userId).get();
        return currentState.getState();
    }

    @Override
    public boolean setDirectoryName(String userId, String directoryName) {

        Optional<Directory> poten = dataComponentRepository
            .findByUserIdAndDirectoryName(userId, directoryName);

        if (!poten.isPresent()) {
            return false;
        } else {
            DeleteCardState currentState = deleteCardStateRepository.findById(userId).get();
            currentState.setDirectory(directoryName);
            deleteCardStateRepository.save(currentState);
            return true;
        }

    }

    @Override
    public boolean setCardFront(String userId, String cardFront) {

        DeleteCardState currentState = deleteCardStateRepository.findById(userId).get();
        String directoryName = currentState.getDirectory();
        Directory target = dataComponentRepository
            .findByUserIdAndDirectoryName(userId, directoryName).get();
        if (!target.isCardFrontExistAsDirectChild(cardFront)) {
            return false;
        } else {
            currentState.setCardFront(cardFront);
            deleteCardStateRepository.save(currentState);
            return true;
        }
    }

    @Override
    public String handleChanges(String userId) {
        DeleteCardState deleteCardState = deleteCardStateRepository.findById(userId).get();
        String directoryName = deleteCardState.getDirectory();
        String cardFront = deleteCardState.getCardFront();
        Directory targetDir = dataComponentRepository
            .findByUserIdAndDirectoryName(userId, directoryName).get();
        targetDir.removeChildByCardFront(cardFront);
        dataComponentRepository.save(targetDir);
        return "Card with front \"" + cardFront + "\" "
            + "has been deleted from directory " + directoryName;
    }
}

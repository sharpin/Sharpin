package com.sharpin.sharpin.service.handlers.movecard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewDirectoryHandler implements Handler {

    @Autowired
    MoveCardService moveCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "New Directory Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Directory name can't be empty!");
        } else if (moveCardService.setNewDirectory(userId, message)) {
            String responseToUser = moveCardService.handleChanges(userId);
            moveCardService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            res.add(responseToUser);
        } else {
            moveCardService.setState(userId, "New Directory Handler");
            res.add("ERROR: Directory " + message + " does not exist.");
            res.add("New directory?");
        }

        return res;
    }
}

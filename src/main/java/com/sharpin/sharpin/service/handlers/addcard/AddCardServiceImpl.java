package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.card.NormalCard;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.AddCardState;
import com.sharpin.sharpin.repository.temporarydata.AddCardStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class AddCardServiceImpl implements AddCardService {

    @Autowired
    AddCardStateRepository addCardStateRepository;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Override
    public String getState(String userID) {
        AddCardState returned = addCardStateRepository.findById(userID).get();
        return returned.getState();
    }

    @Override
    public String getDirectory(String userId) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        return returned.getDirectory();
    }

    @Override
    public String getCardFront(String userId) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        return returned.getCardFront();
    }

    @Override
    public String getCardBack(String userId) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        return returned.getCardBack();
    }

    @Override
    public String getCardNotes(String userId) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        return returned.getCardNotes();
    }

    @Override
    public void setState(String userId, String nextState) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        returned.setState(nextState);
        addCardStateRepository.save(returned);
    }

    @Override
    public void setCardFront(String userId, String cardFront) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        returned.setCardFront(cardFront);
        addCardStateRepository.save(returned);
    }

    @Override
    public void setCardBack(String userId, String cardBack) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        returned.setCardBack(cardBack);
        addCardStateRepository.save(returned);
    }

    @Override
    public void setCardNotes(String userId, String cardNotes) {
        AddCardState returned = addCardStateRepository.findById(userId).get();
        returned.setCardNotes(cardNotes);
        addCardStateRepository.save(returned);
    }

    @Override
    public boolean setDirectory(String userId, String directory) {
        AddCardState returned = addCardStateRepository.findById(userId).get();

        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                userId,directory);
        if (data.isPresent()) {
            returned.setDirectory(directory);
            addCardStateRepository.save(returned);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void deleteUserData(String userId) {

        try {
            addCardStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String donothing = "Do nothing";
        }
    }

    @Override
    public void createUserData(String userId) {
        AddCardState newState = new AddCardState();
        newState.setUserId(userId);
        addCardStateRepository.save(newState);

    }

    @Override
    public String handleChanges(String userId) {
        AddCardState moved = addCardStateRepository.findById(userId).get();
        boolean saved = save(moved);
        if (saved) {
            return "Card " + moved.getCardFront() + " added to "
                 + moved.getDirectory() + "!";
        } else {
            return "Card with front " + moved.getCardFront()
                    + " already exist in " + moved.getDirectory()
                    + ". Canceling addcard process, please try again";
        }
    }

    private boolean save(AddCardState addCardState) {

        // directory is guaranteed to exist because is checked in setDirectory()
        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                addCardState.getUserId(),addCardState.getDirectory());
        Directory existingDirectory = data.get();

        if (!existingDirectory.isCardFrontExistAsDirectChild(addCardState.getCardFront())) {
            NormalCard saveCard = new NormalCard(addCardState.getCardFront(),
                addCardState.getCardBack(), addCardState.getCardNotes());

            existingDirectory.addChild(saveCard);
            dataComponentRepository.save(existingDirectory);
            return true;
        } else {
            return false;
        }
    }
}

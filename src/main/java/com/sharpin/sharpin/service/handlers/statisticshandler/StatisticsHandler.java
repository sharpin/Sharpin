package com.sharpin.sharpin.service.handlers.statisticshandler;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.service.CardAnswerData;
import com.sharpin.sharpin.service.LevelData;
import com.sharpin.sharpin.service.StatisticsService;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsHandler implements Handler {

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Autowired
    StatisticsService statisticsService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "\\statistics";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();
        String[] splitted = message.split(" ");
        StringJoiner joiner = new StringJoiner("");
        for (int i = 1; i < splitted.length; i++) {
            joiner.add(splitted[i] + " ");
        }
        String supposedDir = joiner.toString().trim();
        if (supposedDir.equals("")) {
            res.add("ERROR: please give a directory name\n"
                + "For example: \\statistics statmiss root");
        } else if (splitted[0].equals("statlevel")) {
            Optional<Directory> dir =
                    dataComponentRepository.findByUserIdAndDirectoryName(userId, supposedDir);
            if (dir.isPresent()) {
                Directory toBeListed = dir.get();
                List<LevelData> datas = statisticsService.getNumberOfCardsInEachLevel(toBeListed);
                res.add("Number of cards in each level on directory "
                    + toBeListed.getDirectoryName() + ":\n");
                for (LevelData data : datas) {
                    res.add("- Level "
                        + data.getSrsStage() + ": " + data.getNumberOfCards() + "\n");

                }
            } else {
                res.add("ERROR: Directory doesn't exist\n"
                        + "Please try another directory");
            }

        } else if (splitted[0].equals("statmiss")) {
            Optional<Directory> dir =
                    dataComponentRepository.findByUserIdAndDirectoryName(userId, supposedDir);
            if (dir.isPresent()) {
                Directory toBeListed = dir.get();
                List<CardAnswerData> datas =
                        statisticsService.getMostWrongAnsweredCard(toBeListed, 5);
                res.add("Cards in this directory that has the lowest correct answer rate:\n");
                for (CardAnswerData data : datas) {
                    res.add("- " + data.getCardFront()
                        + " (" + data.getRatioOfAnsweredCorrectly() + "%)\n");
                }


            } else {
                res.add("ERROR: Directory doesn't exist\n"
                        + "Please try another directory");
            }
        } else {
            res.add("ERROR: invalid statistics command, "
                + "please enter statmiss or statlevel as the second command\n"
                + "For example: \\statistics statmiss root");
        }
        List<String> returned = new ArrayList<>();
        returned.add(listToString(res));
        return returned;
    }

    private String listToString(List<String> list) {
        String answer = "";
        for (String s : list) {
            answer += s;
        }
        return answer;
    }
}







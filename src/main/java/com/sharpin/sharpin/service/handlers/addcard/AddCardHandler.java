package com.sharpin.sharpin.service.handlers.addcard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddCardHandler implements Handler {

    @Autowired
    AddCardService addCardService;

    @Autowired
    UserStateService userStateService;

    @Autowired
    CardFrontAddCardHandler cardFrontAddCardHandler;

    @Autowired
    CardBackAddCardHandler cardBackAddCardHandler;

    @Autowired
    CardNotesAddCardHandler cardNotesAddCardHandler;

    @Autowired
    DirectoryAddCardHandler directoryAddCardHandler;

    @Autowired
    InitialAddCardHandler initialAddCardHandler;

    @Override
    public String getName() {
        return "\\addcard";
    }

    @Override
    public List<String> handle(String userId, String message) {
        if (message != null && message.equals("\\exit")) {
            addCardService.deleteUserData(userId);
            userStateService.deleteUserData(userId);
            List<String> res = new ArrayList<>();
            res.add("Process canceled");
            return res;
        }

        List<Handler> stages = new ArrayList<>();
        stages.add(cardFrontAddCardHandler);
        stages.add(cardBackAddCardHandler);
        stages.add(cardNotesAddCardHandler);
        stages.add(directoryAddCardHandler);

        String addCardState = null;
        try {
            addCardState = addCardService.getState(userId);

        } catch (NoSuchElementException e) {
            return initialAddCardHandler.handle(userId, message);
        }

        for (Handler nextStage : stages) {
            if (addCardState.equals(nextStage.getName())) {
                return nextStage.handle(userId, message);
            }
        }

        addCardService.deleteUserData(userId);
        userStateService.deleteUserData(userId);
        List<String> res = new ArrayList<>();
        res.add("Invalid stage reached!");
        return res;
        
    }

}

package com.sharpin.sharpin.service.handlers.deletecard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteCardInitialHandler implements Handler {

    @Autowired
    DeleteCardService deleteCardService;

    @Autowired
    UserStateService userStateService;

    @Override
    public String getName() {
        return "\\deletecard";
    }

    @Override
    public List<String> handle(String userId, String message) {
        userStateService.createUserData(userId);
        userStateService.setUserState(userId, "Delete Card Handler");

        deleteCardService.createUserData(userId);
        deleteCardService.setState(userId, "Directory Name Handler");

        List<String> returned = new ArrayList<>();
        returned.add("Directory name?");
        return returned;
    }
}

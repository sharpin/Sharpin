package com.sharpin.sharpin.service.handlers.editcard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewBackEcHandler implements Handler {

    @Autowired
    EditCardService editCardService;


    @Override
    public String getName() {
        return "New Back Handler";
    }

    @Override
    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("New card back name can't be empty!");
        } else {
            editCardService.setNewBack(userId, message);
            editCardService.setState(userId, "New Note Handler");
            res.add("This card's current note is:");
            res.add(editCardService.getOldNote(userId));
            res.add("New note?");
        }

        return res;

    }
}

package com.sharpin.sharpin.service.handlers.movecard;

import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class InitialHandler implements Handler {

    @Autowired
    MoveCardService moveCardService;

    @Autowired
    UserStateService userStateService;


    public String getName() {
        return "Initial Handler";
    }

    public List<String> handle(String userId, String message) {

        userStateService.createUserData(userId);
        moveCardService.createUserData(userId);

        userStateService.setUserState(userId, "\\movecard");

        moveCardService.setState(userId, "Old Directory Handler");

        List<String> res = new ArrayList<>();
        res.add("Directory?");
        return res;
    }

}

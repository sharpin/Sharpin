package com.sharpin.sharpin.service.handlers.sessionhandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.UserCardList;
import com.sharpin.sharpin.service.DataService;
import com.sharpin.sharpin.service.JsonProcessingHelper;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ReviewHandler implements Handler {

    @Autowired
    private DataService dataService;

    @Autowired
    private UserStateService userStateService;

    @Autowired
    private JsonProcessingHelper jsonHelper;

    @Autowired
    private Environment environment;

    @Override
    public String getName() {
        return "\\review";
    }

    @Override
    public List<String> handle(String userId, String message) {
        List<String> res = new ArrayList<>();

        int numberOfCards = 5;
        if (message != null) {
            try {
                numberOfCards = Integer.parseInt(message);
            } catch (NumberFormatException e) {
                numberOfCards = 5;
            }
        }
        String topic = message;

        List<Card> questionedCards = dataService.getReviewCards(userId, numberOfCards);
        if (questionedCards.size() == 0) {
            return Arrays.asList("No cards to review");
        }
        userStateService.createUserData(userId);
        userStateService.setUserState(userId, "session");
        return postReview(userId, questionedCards);
    }

    /**
     * Helper to post review.
     */
    public List<String> postReview(String userId, List<Card> cards) {

        String baseApiUrl = environment.getProperty("SESSION_URL");
        String apiUrl = baseApiUrl + "/startreview";
        UserCardList userCardList = new UserCardList(userId, cards);
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            String serializedCards = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(userCardList);

            String response = jsonHelper.postRequest(apiUrl, serializedCards);
            List<String> responses = jsonHelper
                .getListFromJson(response, "returned_data", "responses");
            return responses;
        } catch (Exception e) {
            List<String> response = new ArrayList<>();
            response.add("Error when parsing, please try again!");
            return response;
        }

    }
}

package com.sharpin.sharpin.service.handlers.movecard;

import com.sharpin.sharpin.service.handlers.Handler;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class OldDirectoryHandler implements Handler {

    @Autowired
    MoveCardService moveCardService;

    public String getName() {
        return "Old Directory Handler";
    }

    public List<String> handle(String userId, String message) {

        List<String> res = new ArrayList<>();

        if (message == null) {
            res.add("Directory name can't be empty!");
        } else if (moveCardService.setOldDirectory(userId, message)) {
            moveCardService.setState(userId, "Card Front Handler");
            res.add("Card front?");
        } else {
            res.add("ERROR: Directory " + message + " does not exist.");
            res.add("Directory?");
        }
        return res;
    }
}

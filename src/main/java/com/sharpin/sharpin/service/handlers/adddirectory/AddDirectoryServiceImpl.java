package com.sharpin.sharpin.service.handlers.adddirectory;

import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.repository.temporarydata.AddDirectoryState;
import com.sharpin.sharpin.repository.temporarydata.AddDirectoryStateRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class AddDirectoryServiceImpl implements AddDirectoryService {

    @Autowired
    AddDirectoryStateRepository addDirectoryStateRepository;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Override
    public String getState(String userId) {
        AddDirectoryState returned = addDirectoryStateRepository.findById(userId).get();
        return returned.getState();
    }

    @Override
    public String getDirectoryName(String userId) {
        AddDirectoryState returned = addDirectoryStateRepository.findById(userId).get();
        return returned.getDirectoryName();
    }

    @Override
    public String getParentDirectory(String userId) {
        AddDirectoryState returned = addDirectoryStateRepository.findById(userId).get();
        return returned.getParentDirectory();
    }

    @Override
    public void setState(String userId, String nextState) {
        AddDirectoryState returned = addDirectoryStateRepository.findById(userId).get();
        returned.setState(nextState);
        addDirectoryStateRepository.save(returned);
    }

    @Override
    public boolean setDirectoryName(String userId, String directoryName) {

        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, directoryName);

        if (!data.isPresent()) {
            AddDirectoryState returned = addDirectoryStateRepository.findById(userId).get();
            returned.setDirectoryName(directoryName);
            addDirectoryStateRepository.save(returned);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setParentDirectory(String userId, String parentDirectory) {


        Optional<Directory> data = dataComponentRepository.findByUserIdAndDirectoryName(
                userId, parentDirectory);

        if (data.isPresent()) {
            AddDirectoryState returned = addDirectoryStateRepository.findById(userId).get();
            returned.setParentDirectory(parentDirectory);
            addDirectoryStateRepository.save(returned);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void deleteUserData(String userId) {

        try {
            addDirectoryStateRepository.deleteById(userId);
        } catch (EmptyResultDataAccessException e) {
            String don = "nothing";
        }
    }

    @Override
    public void createUserData(String userId) {
        AddDirectoryState newState = new AddDirectoryState();
        newState.setUserId(userId);
        addDirectoryStateRepository.save(newState);
    }

    @Override
    public String handleChanges(String userId) {
        AddDirectoryState moved = addDirectoryStateRepository.findById(userId).get();
        save(moved);
        return "Directory added!";
    }

    private void save(AddDirectoryState state) {
        Directory newDirectory = new Directory(state.getDirectoryName(), state.getUserId());
        Optional<Directory> found = dataComponentRepository.findByUserIdAndDirectoryName(
                state.getUserId(), state.getParentDirectory());

        if (found.isPresent()) {
            Directory parentDirectory = found.get();
            parentDirectory.addChild(newDirectory);
            dataComponentRepository.save(parentDirectory);
        }
    }
}

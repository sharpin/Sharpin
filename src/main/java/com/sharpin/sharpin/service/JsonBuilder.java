package com.sharpin.sharpin.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;

public class JsonBuilder {

    private ObjectNode currentJson;
    private ObjectMapper objectMapper;

    /**
     * The constructor.
     */
    public JsonBuilder() {

        objectMapper = new ObjectMapper();
        currentJson = objectMapper.createObjectNode();
    }

    /**
     * Add an integer to JSON.
     */
    public JsonBuilder addEntry(String key, int value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * Add a String to JSON.
     */
    public JsonBuilder addEntry(String key, String value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * Add a boolean to JSON.
     */
    public JsonBuilder addEntry(String key, boolean value) {
        currentJson.put(key, value);
        return this;
    }

    /**
     * Add a List to JSON.
     */
    public JsonBuilder addList(String key, List<String> value) {
        ArrayNode responseNode = objectMapper.valueToTree(value);
        currentJson.putArray(key).addAll(responseNode);
        return this;
    }

    /**
     * Create JSON based on the string.
     */
    public String createJson(String heading)  {

        try {
            JsonNode result = objectMapper.createObjectNode().set(heading, currentJson);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
        } catch (JsonProcessingException e) {
            return "error_message:" + e.getMessage();
        }
    }

}

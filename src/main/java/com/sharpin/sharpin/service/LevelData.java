package com.sharpin.sharpin.service;

public class LevelData {

    int srsStage;
    int numberOfCards;

    public LevelData(int stage, int cardsInLevel) {
        srsStage = stage;
        numberOfCards = cardsInLevel;
    }

    public int getSrsStage() {
        return srsStage;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }
}

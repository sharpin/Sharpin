package com.sharpin.sharpin.service;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;


@Service
public class NavigationServiceImpl implements NavigationService {

    @Override
    public List<Card> listCardInDirectory(Directory directory) {
        List<Card> listOfCards = new ArrayList<Card>();
        for (int i = 0; i < directory.getChildrenSize(); i++) {
            DataComponent comp = directory.getChild(i);
            if (directory.getChild(i).componentType().equals("Card")) {
                listOfCards.add((Card) directory.getChild(i));
            }
        }
        return listOfCards;
    }


    @Override
    public List<Card> listCardInHierarchy(DataComponent cur) {

        List<Card> ret = new ArrayList<>();
        if (cur.componentType().equals("Card")) {
            ret.add((Card) cur);
            return ret;
        }
        Directory curd = (Directory) cur;
        for (int i = 0; i < curd.getChildrenSize(); i++) {
            List<Card> childCards = listCardInHierarchy(curd.getChild(i));
            ret.addAll(childCards);
        }
        return ret;

    }

    @Override
    public List<Directory> navigateDirectory(Directory directory) {
        List<Directory> listOfDirectory = new ArrayList<>();
        for (int i = 0; i < directory.getChildrenSize(); i++) {
            DataComponent comp = directory.getChild(i);
            if (comp.componentType().equals("Directory")) {
                listOfDirectory.add((Directory) comp);
            }
        }
        return listOfDirectory;
    }
}
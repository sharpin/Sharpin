package com.sharpin.sharpin.service;

import com.sharpin.sharpin.card.Card;
import java.util.List;
import java.util.NoSuchElementException;

public interface DataService {

    List<Card> getReviewCards(String userId, int numberOfCards);

    List<Card> getCardsFrom(String userId, String topic, int numberOfCards)
        throws NoSuchElementException;
}

package com.sharpin.sharpin.service;

import com.sharpin.sharpin.storage.Directory;
import java.util.List;

public interface StatisticsService {

    public List<CardAnswerData> getMostWrongAnsweredCard(Directory directory, int displayedCards);

    public List<LevelData> getNumberOfCardsInEachLevel(Directory directory);

}

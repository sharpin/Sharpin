package com.sharpin.sharpin.service;

public class CardAnswerData implements Comparable<CardAnswerData> {

    String cardFront;
    Double ratioOfAnsweredCorrectly;

    public CardAnswerData(String cardFrontName, double ratioOfCorrect) {
        cardFront = cardFrontName;
        ratioOfAnsweredCorrectly = ratioOfCorrect;
    }

    public String getCardFront() {
        return cardFront;
    }

    public double getRatioOfAnsweredCorrectly() {
        return ratioOfAnsweredCorrectly;
    }

    @Override
    public int compareTo(CardAnswerData o) {
        if (this.getRatioOfAnsweredCorrectly() <= o.getRatioOfAnsweredCorrectly()) {
            return -1;
        } else {
            return 1;
        }
    }
}

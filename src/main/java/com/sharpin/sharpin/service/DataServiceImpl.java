package com.sharpin.sharpin.service;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.repository.DataComponentRepository;
import com.sharpin.sharpin.storage.Directory;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DataServiceImpl implements DataService {

    @Autowired
    NavigationService navigationService;

    @Autowired
    DataComponentRepository dataComponentRepository;

    @Value("${keep_data_order}")
    private boolean keepDataOrder;

    @Override
    public List<Card> getReviewCards(String userId, int numberOfCards) {

        Directory userRootDirectory = dataComponentRepository
            .findByUserIdAndDirectoryName(userId, "root").get();

        List<Card> allUserCards = navigationService.listCardInHierarchy(userRootDirectory);
        List<Card> allReviewCards = new ArrayList<>();
        for (Card card : allUserCards) {
            if (card.isReadyForReview()) {
                allReviewCards.add(card);
            }
        }

        if (!keepDataOrder) {
            Collections.shuffle(allReviewCards);
        }

        List<Card> sampledReviewCards = new ArrayList<>();
        for (int i = 0; i < numberOfCards; i++) {
            if (i == allReviewCards.size()) {
                break;
            }
            sampledReviewCards.add(allReviewCards.get(i));
        }
        return sampledReviewCards;
    }

    @Override
    public List<Card> getCardsFrom(String userId, String topic, int numberOfCards)
        throws NoSuchElementException {

        Optional<Directory> optDir = dataComponentRepository
            .findByUserIdAndDirectoryName(userId, topic);

        if (!optDir.isPresent()) {
            throw new NoSuchElementException("Directory does not exist");
        }

        List<Card> allCard = navigationService.listCardInHierarchy(optDir.get());

        if (!keepDataOrder) {
            Collections.shuffle(allCard);
        }
        List<Card> sampledCards = new ArrayList<>();
        for (int i = 0; i < numberOfCards; i++) {
            if (i >= allCard.size()) {
                break;
            }
            sampledCards.add(allCard.get(i));
        }
        return sampledCards;
    }
}


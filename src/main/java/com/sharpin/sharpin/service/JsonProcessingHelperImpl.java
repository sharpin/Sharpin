package com.sharpin.sharpin.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class JsonProcessingHelperImpl implements JsonProcessingHelper {

    @Override
    public String postRequest(String url, String jsonString) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(jsonString, headers);
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.postForObject(url, request, String.class);
        return response;
    }

    @Override
    public List<String> getListFromJson(String jsonString, String header, String fieldName) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode returned = mapper.readTree(jsonString).get(header);
            JsonNode responsesJson = returned.get(fieldName);
            String responsesString = mapper.writeValueAsString(responsesJson);
            List<String> responses = mapper.readValue(responsesString, List.class);
            return responses;
        } catch (IOException e) {
            List<String> responses = new ArrayList<>();
            responses.add("An error has occured!");
            return responses;

        }
    }

    @Override
    public boolean getBooleanFromJson(String jsonString, String header, String fieldName) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode returned = mapper.readTree(jsonString).get(header);
            return returned.get(fieldName).asBoolean();
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public long getLongFromJson(String jsonString, String header, String fieldName) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode returned = mapper.readTree(jsonString).get(header);
            return returned.get(fieldName).asLong();
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public int getIntFromJson(String jsonString, String header, String fieldName) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode returned = mapper.readTree(jsonString).get(header);
            return returned.get(fieldName).asInt();
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public boolean isFieldInJson(String jsonString, String header, String fieldName) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode returned = mapper.readTree(jsonString).get(header);
            return returned.has(fieldName);
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public String getStringFromJson(String jsonString, String header, String fieldName) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode returned = mapper.readTree(jsonString).get(header);
            return returned.get(fieldName).asText();
        } catch (IOException e) {
            return null;
        }
    }
}

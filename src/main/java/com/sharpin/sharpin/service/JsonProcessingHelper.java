package com.sharpin.sharpin.service;

import java.util.List;

public interface JsonProcessingHelper {

    String postRequest(String url, String jsonString);

    List<String> getListFromJson(String jsonString, String header, String fieldName);

    boolean getBooleanFromJson(String jsonString, String header, String fieldName);

    long getLongFromJson(String jsonString, String header, String fieldName);

    int getIntFromJson(String jsonString, String header, String fieldName);

    String getStringFromJson(String jsonString, String header, String fieldName);

    boolean isFieldInJson(String jsonString, String header, String fieldName);
}

package com.sharpin.sharpin.storage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

@Entity
@Table(name = "data_component")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class DataComponent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long componentId;

    public abstract String componentType();

    @ManyToOne
    @JoinColumn(name = "parent_component_id", updatable = true, insertable = true)
    protected DataComponent parent;

    @JsonIgnore
    public Long getId() {
        return componentId;
    }

    public DataComponent getParent() {
        return parent;
    }

    public void setParent(DataComponent parent) {
        this.parent = parent;
    }


}


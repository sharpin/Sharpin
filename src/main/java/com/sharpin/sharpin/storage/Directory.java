package com.sharpin.sharpin.storage;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.card.NormalCard;
import java.util.*;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "directory",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"directoryName", "userId"})
        })
public class Directory extends DataComponent {

    public Directory(){}

    public Directory(String directoryName, String userId) {
        this.directoryName = directoryName;
        this.userId = userId;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,
        mappedBy = "parent", fetch = FetchType.LAZY)
    protected List<DataComponent> children = new ArrayList<>();

    @Column(nullable = false)
    protected String directoryName;

    @Column(nullable = false)
    protected String userId;

    public int getChildrenSize() {
        return children.size();
    }

    public DataComponent getChild(int index) {
        return children.get(index);
    }

    /**
     * Finding card by its name from list of children.
     * @param name string to be found
     */
    public Card findCardByName(String name) {
        Card result = null;
        for (DataComponent data : children) {
            if (data.componentType().equals("Card")) {
                if (((Card)data).getFront().equals(name)) {
                    result = (Card)data;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Adding child to this directory.
     * @param data the child
     */
    public void addChild(DataComponent data) {

        children.add(data);
        data.setParent(this);
    }

    /**
     * Removing a child from the directory.
     * @param data the child to remove
     */
    public void removeChild(DataComponent data) {

        if (children.remove(data)) {
            data.setParent(null);
        }

    }

    /**
     * Removes a card from the immediate child of this directory.
     * @param cardFront the cardfront of the card to remove
     */
    public void removeChildByCardFront(String cardFront) {

        DataComponent found = null;
        for (DataComponent dataComponent: children) {
            if (dataComponent.componentType().equals("Card")) {
                Card child = (Card) dataComponent;
                if (cardFront.equals(child.getFront())) {
                    found = child;
                    break;
                }
            }
        }
        if (found != null) {
            removeChild(found);
        }
    }

    public boolean isEmpty() {
        return children.isEmpty();
    }

    @Override
    public String componentType() {
        return "Directory";
    }

    @Override
    public String toString() {
        return directoryName + " " + userId;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public String getUserId() {
        return userId;
    }

    /**
     * A function to find if a card exists as an immediate child.
     * @param cardFront the card front to find
     * @return boolean indicating whether the card exists.
     */
    public boolean isCardFrontExistAsDirectChild(String cardFront) {

        for (DataComponent dataComponent: children) {
            if (dataComponent.componentType().equals("Card")) {
                Card child = (Card) dataComponent;
                if (cardFront.equals(child.getFront())) {
                    return true;
                }
            }
        }
        return false;
    }


}
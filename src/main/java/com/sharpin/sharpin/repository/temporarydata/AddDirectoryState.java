package com.sharpin.sharpin.repository.temporarydata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "add_directory_state")
public class AddDirectoryState {

    @Id
    private String userId;

    @Column(name = "directory_name")
    private String directoryName;

    @Column(name = "parent_directory")
    private String parentDirectory;

    @Column(name = "state")
    private String state;

    public String getUserId() {
        return userId;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public String getParentDirectory() {
        return parentDirectory;
    }

    public String getState() {
        return state;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public void setState(String state) {
        this.state = state;
    }
}

package com.sharpin.sharpin.repository.temporarydata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoveCardStateRepository extends JpaRepository<MoveCardState, String> {
}

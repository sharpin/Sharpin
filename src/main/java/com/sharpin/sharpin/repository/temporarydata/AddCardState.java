package com.sharpin.sharpin.repository.temporarydata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "add_card_state")
public class AddCardState {

    @Id
    private String userId;

    @Column(name = "card_front")
    private String cardFront;

    @Column(name = "card_back")
    private String cardBack;

    @Column(name = "card_notes")
    private String cardNotes;

    @Column(name = "directory")
    private String directory;

    @Column(name = "state")
    private String state;

    public String getUserId() {
        return userId;
    }

    public String getCardFront() {
        return cardFront;
    }

    public String getCardBack() {
        return cardBack;
    }

    public String getCardNotes() {
        return cardNotes;
    }

    public String getDirectory() {
        return directory;
    }

    public String getState() {
        return state;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCardFront(String cardFront) {
        this.cardFront = cardFront;
    }

    public void setCardBack(String cardBack) {
        this.cardBack = cardBack;
    }

    public void setCardNotes(String cardNotes) {
        this.cardNotes = cardNotes;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public void setState(String state) {
        this.state = state;
    }
}

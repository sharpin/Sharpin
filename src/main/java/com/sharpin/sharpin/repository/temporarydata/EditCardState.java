package com.sharpin.sharpin.repository.temporarydata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "edit_card_state")
public class EditCardState {

    @Id
    private String userId;

    @Column(name = "directory_name")
    private String directoryName;

    @Column(name = "card_front")
    private String cardFront;

    @Column(name = "old_front")
    private String oldFront;

    @Column(name = "old_back")
    private String oldBack;

    @Column(name = "old_note")
    private String oldNote;

    @Column(name = "new_front")
    private String newFront;

    @Column(name = "new_back")
    private String newBack;

    @Column(name = "new_note")
    private String newNote;

    @Column(name = "state")
    private String state;

    public EditCardState() {
    }

    public String getUserId() {
        return userId;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public String getCardFront() {
        return cardFront;
    }

    public String getOldFront() {
        return oldFront;
    }

    public String getOldBack() {
        return oldBack;
    }

    public String getOldNote() {
        return oldNote;
    }

    public String getNewFront() {
        return newFront;
    }

    public String getNewBack() {
        return newBack;
    }

    public String getNewNote() {
        return newNote;
    }

    public String getState() {
        return state;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public void setCardFront(String cardFront) {
        this.cardFront = cardFront;
    }

    public void setOldFront(String oldFront) {
        this.oldFront = oldFront;
    }

    public void setOldBack(String oldBack) {
        this.oldBack = oldBack;
    }

    public void setOldNote(String oldNote) {
        this.oldNote = oldNote;
    }

    public void setNewFront(String newFront) {
        this.newFront = newFront;
    }

    public void setNewBack(String newBack) {
        this.newBack = newBack;
    }

    public void setNewNote(String newNote) {
        this.newNote = newNote;
    }

    public void setState(String state) {
        this.state = state;
    }
}

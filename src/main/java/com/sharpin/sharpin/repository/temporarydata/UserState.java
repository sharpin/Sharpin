package com.sharpin.sharpin.repository.temporarydata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_state")
public class UserState {

    @Column(name = "state")
    private String state;

    @Id
    private String userId;

    public String getState() {
        return state;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setState(String state) {
        this.state = state;
    }
}

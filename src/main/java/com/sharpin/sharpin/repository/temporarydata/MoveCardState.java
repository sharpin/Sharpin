package com.sharpin.sharpin.repository.temporarydata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "move_card_state")
public class MoveCardState {

    @Id
    private String userId;

    @Column(name = "old_directory")
    private String oldDirectory;

    @Column(name = "card_front")
    private String cardFront;

    @Column(name = "new_directory")
    private String newDirectory;

    @Column(name = "state")
    private String state;

    public String getUserId() {
        return userId;
    }

    public String getOldDirectory() {
        return oldDirectory;
    }

    public String getCardFront() {
        return cardFront;
    }

    public String getNewDirectory() {
        return newDirectory;
    }

    public String getState() {
        return state;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCardFront(String cardFront) {
        this.cardFront = cardFront;
    }

    public void setOldDirectory(String oldDirectory) {
        this.oldDirectory = oldDirectory;
    }

    public void setNewDirectory(String newDirectory) {
        this.newDirectory = newDirectory;
    }

    public void setState(String state) {
        this.state = state;
    }

}

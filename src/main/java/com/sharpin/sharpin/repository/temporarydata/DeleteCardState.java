package com.sharpin.sharpin.repository.temporarydata;

import javax.persistence.*;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "add_card_state")
@NoArgsConstructor
public class DeleteCardState {

    @Id
    private String userId;

    @Column(name = "card_front")
    private String cardFront;

    @Column(name = "directory")
    private String directory;

    @Column(name = "state")
    private String state;

    public String getUserId() {
        return userId;
    }

    public String getCardFront() {
        return cardFront;
    }

    public String getDirectory() {
        return directory;
    }

    public String getState() {
        return state;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCardFront(String cardFront) {
        this.cardFront = cardFront;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public void setState(String state) {
        this.state = state;
    }
}

package com.sharpin.sharpin.repository;

import com.sharpin.sharpin.card.Card;
import com.sharpin.sharpin.storage.DataComponent;
import com.sharpin.sharpin.storage.Directory;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DataComponentRepository extends JpaRepository<DataComponent, Long> {

    @Query(value = "SELECT D FROM Directory D\n"
            + "WHERE D.userId = :userId AND D.directoryName = :directoryName\n")
    Optional<Directory> findByUserIdAndDirectoryName(@Param("userId") String userId,
                                                     @Param("directoryName") String directoryName);

    @Query(value = "SELECT C FROM Card C")
    List<Card> listAllCards();
}

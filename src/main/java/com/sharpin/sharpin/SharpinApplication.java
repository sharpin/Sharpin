package com.sharpin.sharpin;


import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.sharpin.sharpin.service.handlers.Handler;
import com.sharpin.sharpin.service.handlers.SharpinHandler;
import com.sharpin.sharpin.service.handlers.UserStateService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;

@SpringBootApplication
@Controller
@LineMessageHandler
public class SharpinApplication extends SpringBootServletInitializer {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private SharpinHandler handler;

    @Autowired
    private UserStateService userStateService;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SharpinApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SharpinApplication.class, args);
    }

    /**
     * handle the text event.
     */
    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        String replyToken = messageEvent.getReplyToken();
        String message = messageEvent.getMessage().getText();
        try {

            if (message.equals("backdoor312")) {
                List<String> response = Arrays.asList(
                    "You entered the back door!",
                    "3...",
                    "2...",
                    "1...",
                    "You exited the back door!"
                );
                replyMessage(replyToken, response);
                return;
            }
            List<String> response = handler.handle(messageEvent.getSource().getUserId(),message);
            replyMessage(replyToken, response);

        } catch (Exception e) {
            String errorMessage = "Sorry, an error has occured, please try again";
            if (e.getMessage() != null) {
                errorMessage = e.getMessage();
            }
            userStateService.deleteUserData(messageEvent.getSource().getUserId());
            replyMessage(replyToken, stringToList(errorMessage));
        }
    }

    private void replyMessage(String replyToken, List<String> responses) {
        List<Message> wrappedResponses = stringToMessage(responses);
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, wrappedResponses)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error when replying message");
        }
    }

    private List<String> stringToList(String response) {
        List<String> messages = new ArrayList<>();
        messages.add(response);
        return messages;
    }

    private List<Message> stringToMessage(List<String> responses) {
        List<Message> messages = new ArrayList<>();
        for (String response: responses) {
            TextMessage wrappedResponse = new TextMessage(response);
            messages.add(wrappedResponse);
        }
        return messages;
    }

}

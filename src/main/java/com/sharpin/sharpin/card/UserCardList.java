package com.sharpin.sharpin.card;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class UserCardList extends ArrayList<Card> {

    @JsonProperty("userId")
    private String userId;

    @JsonIgnore
    private boolean empty;

    public UserCardList(String userId) {
        super();
        this.userId = userId;
    }

    @JsonCreator
    public UserCardList(@JsonProperty("userId") String userId,
                        @JsonProperty("cardList") List<Card> items) {
        super(items);
        this.userId = userId;
    }

    public UserCardList() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<Card> getCardList() {
        return new ArrayList<>(this);
    }

}

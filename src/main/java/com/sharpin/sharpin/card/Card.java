package com.sharpin.sharpin.card;

import com.fasterxml.jackson.annotation.*;
import com.sharpin.sharpin.observer.DateObserver;
import com.sharpin.sharpin.storage.DataComponent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "card")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = NormalCard.class, name = "NormalCard"),
})
public abstract class Card extends DataComponent implements DateObserver {

    @Column(name = "front")
    protected String front;

    @Column(name = "back")
    protected String back;

    @Column(name = "notes")
    protected String notes;

    @Column(name = "stage")
    @JsonIgnore
    protected int srsStage;

    @Column(name = "nextReview")
    @JsonIgnore
    protected Date nextReview;

    @Column(name = "readyForReview")
    @JsonIgnore
    protected boolean readyForReview;

    @Column(name = "correct_answer", columnDefinition = "integer default 0")
    @JsonIgnore
    protected int numberOfCorrectAnswer;

    @Column(name = "incorrect_answer", columnDefinition = "integer default 0")
    @JsonIgnore
    protected int numberOfIncorrectAnswer;

    /**
     * Inisialisasi kartu.
     *
     * @param front depan kartu (yang ditanyakan)
     * @param back  belakang kartu (yang di check)
     * @param notes notes dari kartu
     */
    public Card(String front, String back, String notes) {
        this();
        this.front = front;
        this.back = back;
        this.notes = notes;
    }

    /**
     * Setting up the default things in cards.
     * A card should be reviewable when it is created.
     */
    public Card() {
        this.srsStage = 0;
        this.readyForReview = true;
        setNextReview(new Date());
    }

    @Transient
    private SrsWaitingTimeUpdate srsWaitingTimeUpdate = new SrsWaitingTimeUpdate();

    private void setNextReviewFromNow(int level) {
        Date now = new Date();
        Date nextReviewDate = srsWaitingTimeUpdate.getNewReviewDate(now, level);
        setNextReview(nextReviewDate);
    }

    /**
     * When srs is increased should call this.
     */
    public void increaseSrsStage() {
        if (srsStage != 8) {
            srsStage++;
            readyForReview = false;
            setNextReviewFromNow(srsStage);
        }
        numberOfCorrectAnswer++;
    }

    /**
     * When srs is decreased should call this.
     */
    public void decreaseSrsStage() {
        if (srsStage != 0) {
            srsStage--;
        }
        numberOfIncorrectAnswer++;
        readyForReview = false;
        setNextReviewFromNow(srsStage);
    }

    public abstract boolean validateAnswer(String answer);

    public abstract void updateFront(String front);

    public abstract void updateBack(String back);

    public abstract void updateNotes(String notes);

    /**
     * Get ratio of answered correctly.
     */
    public double getRatioOfCorrectReviews() {
        if (numberOfCorrectAnswer == 0 && numberOfIncorrectAnswer == 0) {
            return 100;
        }

        Double ratio = (double)numberOfCorrectAnswer * 100
                / (numberOfCorrectAnswer + numberOfIncorrectAnswer);


        return Math.round(ratio * 100.0) / 100.0;
    }

    public String getFront() {
        return front;
    }

    public String getBack() {
        return back;
    }

    public String getNotes() {
        return notes;
    }

    public int getSrsStage() {
        return srsStage;
    }

    // for testing purposes only
    public void setSrsStage(int stage) {
        this.srsStage = stage;
    }

    public Date getNextReview() {
        return nextReview;
    }

    public void setNextReview(Date nextReview) {
        this.nextReview = nextReview;
    }

    public boolean isReadyForReview() {
        return readyForReview;
    }

    @Override
    public String componentType() {
        return "Card";
    }

    private String roundToNearestHour(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH");
        return formatter.format(date);
    }

    @Override
    public void update(Date date) {

        if (roundToNearestHour(nextReview).compareTo(roundToNearestHour(date)) <= 0) {
            readyForReview = true;
        } else  {
            readyForReview = false;
        }
    }

    @JsonGetter(value = "cardId")
    public Long getCardId() {

        return componentId;
    }

    public void setCardId(long componentId) {
        this.componentId = componentId;
    }
}

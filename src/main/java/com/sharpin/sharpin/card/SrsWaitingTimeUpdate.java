package com.sharpin.sharpin.card;

import java.util.Date;

public class SrsWaitingTimeUpdate {

    final long oneMinuteInMillis = 60000;
    final long oneHourInMillis = 60 * oneMinuteInMillis;
    final long oneDayInMillis = 24 * oneHourInMillis;
    final long oneWeekInMillis = 7 * oneDayInMillis;
    final long oneMonthInMillis = 30 * oneDayInMillis;

    /*
     *  Convenience method to add a specified number of minutes to a Date object
     *  From: http://stackoverflow.com/questions/9043981/how-to-add-minutes-to-my-date
     *  @param  minutes  The number of minutes to add
     *  @param  beforeTime  The time that will have minutes added to it
     *  @return  A date object with the specified number of minutes added to it
     */
    private Date addMillisToDate(long millis, Date beforeTime) {


        long curTimeInMs = beforeTime.getTime();
        Date afterAdding = new Date(curTimeInMs + millis);
        return afterAdding;
    }

    /**
     * Getting the milliseconds that must be waited for the level.
     * @param level srs level
     * @return required wait time (in milliseconds)
     */
    public long getSrsNewWaitingTime(int level) {

        switch (level) {
            case 0: return 4 * oneHourInMillis;
            case 1: return 8 * oneHourInMillis;
            case 2: return oneDayInMillis;
            case 3: return 2 * oneDayInMillis;
            case 4: return oneWeekInMillis;
            case 5: return 2 * oneWeekInMillis;
            case 6: return oneMonthInMillis;
            case 7: return 4 * oneMonthInMillis;
            case 8: return 1000 * oneMonthInMillis; // forever
            default: throw new IllegalArgumentException("Level must be between [0, 8]");
        }
    }

    /**
     * Getting new review date given the current date and srs level.
     * @param oldDate the current date
     * @param newLevel the srs level
     * @return a date object denoting when to review
     */
    public Date getNewReviewDate(Date oldDate, int newLevel) {

        long waitTimeInMillis = getSrsNewWaitingTime(newLevel);
        Date newDate = addMillisToDate(waitTimeInMillis, oldDate);
        return newDate;
    }

}

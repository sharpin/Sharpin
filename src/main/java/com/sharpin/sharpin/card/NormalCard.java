package com.sharpin.sharpin.card;

import javax.persistence.Entity;

@Entity
public class NormalCard extends  Card {

    /**
     * Inisialisasi kartu.
     *
     * @param front depan kartu (yang ditanyakan)
     * @param back  belakang kartu (yang di check)
     * @param notes notes dari kartu
     */
    public NormalCard(String front, String back, String notes) {
        super(front, back, notes);
    }

    public NormalCard() {
        super();
    }

    @Override
    public boolean validateAnswer(String answer) {
        return back.equals(answer);
    }

    @Override
    public void updateFront(String front) {
        this.front = front;
    }

    @Override
    public void updateBack(String back) {
        this.back = back;
    }

    @Override
    public void updateNotes(String notes) {
        this.notes = notes;
    }
}
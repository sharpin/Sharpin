# Coverage and Pipeline statuses:

- master: [![pipeline status](https://gitlab.com/thegalang/Sharpin/badges/master/pipeline.svg)](https://gitlab.com/thegalang/Sharpin/-/commits/master)
[![coverage report](https://gitlab.com/thegalang/Sharpin/badges/master/coverage.svg)](https://gitlab.com/thegalang/Sharpin/-/commits/master)
- session microservice: [![pipeline status](https://gitlab.com/thegalang/sharpin-session/badges/master/pipeline.svg)](https://gitlab.com/thegalang/sharpin-session/-/commits/master) [![coverage report](https://gitlab.com/thegalang/sharpin-session/badges/master/coverage.svg)](https://gitlab.com/thegalang/sharpin-session/-/commits/master)

A LINE chatbot for automatic SRS flashcards.

This is the main repository for the application. Microservice for the Session feature is here: https://gitlab.com/sharpin/sharpin-session

# Adding the bot

line id: @282xnplk

![](assets/sharpinQR.png)
# Features

## Help

### General help
type \\help
![](assets/helpGeneral.jpg)

### Specific help
type \\help [COMMAND_NAME]
![](assets/helpSpecific.jpg)

## Add card

Adding a card to your card collection. Type \\addcard
For each directory, the card front must be unique.
![](assets/add_card.jpg)

## Add directory

Creating a new directory to your system. Type \\adddirectory
The directory names on your system must be unique.
![](assets/adddirectory.jpg)

## Delete card

Deleting a card with a certain front from your directory. Type \\deletecard
![](assets/deletecard.jpg)

## Edit card

Editing the contents of a card. Type \\editcard
![](assets/editcard.jpg)

## Move card

Moving a card to a different directory. Type \\movecard
![](assets/movecard.jpg)

## Listing

Listing contents, either list all cards or directory on a directory.

### List card

![](assets/list_card.jpg)

### List directory

![](assets/list_dir.jpg)

## Q & A Session

The main feature of this app is that you can start a Q & A session. There are two types of session: review and quiz

### Review

![](assets/review.jpg)

Each card has an SRS level attached to it. Details on SRS and SRS level can be read by typing \\help SRS
You can specify the number of cards to review with \\review [NUMBER OF CARDS]. Reviews will only ask cards that are ready for review.

### Quiz

![](assets/quiz.jpg)

Unlike reviews, quiz can ask for any card regardless of its SRS level. You can specify the directory to quiz by the command \quiz [DIRECTORY NAME]. All cards in this directory and its ancestor will be asked. You cannot specify the number of cards to ask, the number of cards that will be asked is 5.

## Statistics

These are statistics utilities of the application.

### Number of cards in each level on a directory

Using the \\statistics statlevel [DIRECTORY NAME] command, you will get the amount of cards for each level on the directory and all of its descendants

![](assets/statlevel.jpg)

### Cards that are frequently got wrong

Using the \\statistics statmiss [DIRECTORY NAME] command, you will know the top 5 cards which you get wrong often in this directory along with all of its descendants. The correct/incorrect count is only counted from reviews.

![](assets/statmiss.jpg)

## Exit from a command

You can use the reserved \\exit keyword to exit in the middle of an interaction and aborting whatever action you did.

![](assets/exit.jpg)

# The team

By Group 3 Advanced Programming Class A Fasilkom UI
- Galangkangin Gotera
- Nethania Sonya
- Stephen Handiar
- Naura Azka
